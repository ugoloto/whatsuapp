<?php

/* desktop/poster/index.tpl */
class __TwigTemplate_0f49f9be63a0206c418611c0fe0ea869 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/root.tpl"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_css($context, array $blocks = array())
    {
        // line 3
        echo "<link href=\"/js/lib/crope/cropper.css\" rel=\"stylesheet\">";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "<section class=\"page-block user-account double-space-bottom\">
    <div class=\"container\">
        <div class=\"row\">";
        // line 12
        $template = $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/profile/menu.tpl"));
        $template->display($context);
        // line 13
        echo "

            <div class=\"col-md-10\">
                <div class=\"tab-content\">

                    <div class=\"tab-pane fade in active\" id=\"notify\">

                        <h3 class=\"heading center\">Афіша <a class=\"btn btn-success pull-right\" href=\"/poster/add\">Нова
                                подія</a></h3>
                        <div class=\"row\">
                            <div class=\"col-xs-12 col-md-12 gray-bg\">
                                <table class=\"table\">
                                    <thead>
                                    <tr>

                                        <th>Дата </br>публікації</th>
                                        <th>Дата </br>події</th>
                                        <th>Час </br>події</th>
                                        <th>Юзер</th>
                                        <th>Тема</th>
                                        <th width=\"5%\">Дії</th>
                                        <th>Статус</th>

                                    </tr>
                                    </thead>";
        // line 38
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 39
            echo "                                    <tr>
                                        <td>";
            // line 40
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "poster_date_reg"), "d.m.Y");
            echo "</td>
                                        <td>";
            // line 41
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "poster_date"), "d.m.Y");
            echo "</td>
                                        <td>";
            // line 42
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "poster_time"), "h.i");
            echo "</td>
                                        <th>";
            // line 43
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "user_nick");
            echo "</th>
                                        <td><h3>";
            // line 44
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "poster_text");
            echo "</h3></td>
                                        <td>
                                            <a href=\"/poster/edit/";
            // line 46
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "poster_id");
            echo "\"><i class=\"fa fa-pencil\"></i></a>
                                            <a href=\"#\" class=\"del\" data-id=\"";
            // line 47
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "poster_id");
            echo "\"><i
                                                        class=\"fa fa-times\"></i></a>
                                        <td>";
            // line 49
            if (($this->getAttribute((isset($context["r"]) ? $context["r"] : null), "poster_status") == 0)) {
                echo "Не опубліковано";
            } else {
                echo " Опубліковано";
            }
            echo "</td>
                                        </td>
                                    </tr>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 55
        echo "                                </table>
                            </div>


                        </div>";
        // line 61
        if ($this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "previous")) {
            echo " <a href=\"";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "?p=";
            echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "previous");
            echo "\"
                                                      style=\"margin: 4px 0;\"><i
                                    class=\"fa fa-arrow-left\"></i></a>";
        }
        // line 64
        echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "info");
        // line 65
        if ($this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "next")) {
            echo "<a href=\"";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "?p=";
            echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "next");
            echo "\" style=\"margin: 6px 0 0 0;\"><i
                                    class=\"fa fa-arrow-right\"></i></a>";
        }
        // line 67
        echo "
                    </div>


                </div>
            </div>
        </div>


    </div>
</section>";
    }

    // line 81
    public function block_js($context, array $blocks = array())
    {
        // line 82
        echo "

<script src=\"/js/lib/plupload-2.1.9/plupload.full.min.js\"></script>
<script src=\"/js/lib/bootbox.min.js\"></script>
<script src=\"/js/lib/crope/cropper.js\"></script>
<script src=\"/js/poster.js\"></script>
<script src=\"/js/profile.js\"></script>";
    }

    public function getTemplateName()
    {
        return "desktop/poster/index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 82,  160 => 81,  146 => 67,  137 => 65,  135 => 64,  125 => 61,  119 => 55,  106 => 49,  101 => 47,  97 => 46,  92 => 44,  88 => 43,  84 => 42,  80 => 41,  76 => 40,  73 => 39,  69 => 38,  43 => 13,  40 => 12,  36 => 8,  33 => 7,  29 => 3,  26 => 2,);
    }
}
