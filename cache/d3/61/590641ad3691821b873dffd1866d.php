<?php

/* desktop/profile/index.tpl */
class __TwigTemplate_d361590641ad3691821b873dffd1866d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/root.tpl"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_css($context, array $blocks = array())
    {
        // line 3
        echo "<link href=\"/js/lib/crope/cropper.css\" rel=\"stylesheet\">";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "<section class=\"page-block user-account double-space-bottom\">
    <div class=\"container\">
        <div class=\"row\">";
        // line 12
        $template = $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/profile/menu.tpl"));
        $template->display($context);
        // line 13
        echo "

            <!--Content-->
            <div class=\"col-md-9 gray-bg\">
                <div class=\"tab-content\">

                    <!--Account Settings-->
                    <div class=\"tab-pane fade in active\" id=\"account\">
                        <h3 class=\"heading center\">Налаштування аккаунта<span></span></h3>
                        <form class=\"account-settings\" id=\"fprofile\" enctype=\"multipart/form-data\">
                            <input type=\"hidden\" id=\"user_id\" name=\"user_id\" value=\"";
        // line 23
        echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id");
        echo "\">
                            <div class=\"row\">
                                <div class=\"col-md-7 col-sm-7 space-bottom\">
                                    <fieldset>
                                        <div class=\"form-group\">
                                            <label for=\"af-first-name\">Ім'я </label>
                                            <input class=\"form-control\" type=\"text\" value=\"";
        // line 29
        echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_fname");
        echo "\"
                                                   name=\"user_fname\" id=\"user_fname\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"af-last-name\">Прізвище </label>
                                            <input class=\"form-control\" type=\"text\" value=\"";
        // line 34
        echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_lname");
        echo "\"
                                                   name=\"user_lname\" id=\"af-last-name\">
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"af-screen-name\">Нік <span class=\"red\">*</span></label>
                                            <input class=\"form-control\" type=\"text\" name=\"user_nick\"
                                                   value=\"";
        // line 40
        echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_nick");
        echo "\" id=\"user_nick\" required>
                                        </div>
                                        <div class=\"form-group\">
                                            <label for=\"af-email\">Email <span class=\"red\">*</span></label>
                                            <input class=\"form-control\" type=\"email\" name=\"user_email\"
                                                   value=\"";
        // line 45
        echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_email");
        echo "\" id=\"user_email\" required>
                                        </div>
                                    </fieldset>

                                </div>
                                <div class=\"col-md-5 col-sm-5\">
                                    <input type=\"hidden\" name=\"img\" id=\"img-file\">
                                    <div class=\"user-avatar\" id=\"containerpic\">
   <span";
        // line 53
        if (($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_pic") == "nopic.jpg")) {
            echo "style=\"display:none;\"";
        }
        echo " id=\"delAva\" class=\"delete\"></span>

    <div id=\"avatar-box\">";
        // line 56
        if (($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_pic") == "nopic.jpg")) {
            // line 57
            echo "    <img class=\"avatar\" id=\"preview\" src=\"/img/nopic.jpg\" alt=\"Аватар\"/>";
        } else {
            // line 59
            echo "    <img class=\"img-responsive center-block\" src=\"/userfiles/";
            echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id");
            echo "/avatar/";
            echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_pic");
            echo "\" alt=\"Користувач\"/>";
        }
        // line 61
        echo "</div>
    <a class=\"upload\" id=\"pickfiles\"><i class=\"fa fa-upload\"></i><span>Завантажити фото</span></a>
</div>

<!--<div class=\"form-group\">
<label >Додати Фото</label>
<input type=\"hidden\" name=\"img\" id=\"img-file\">
<div class=\"form-group\">
<div id=\"containerpic\">
<img class=\"img_responsive\" id=\"preview\" src=\"/img/nopic.jpg\" style=\"display:none;\">
<button id=\"pickfiles\" class=\"btn btn-primary\">Upload Image</button>
</div>
</div>
</div>-->
</div>
</div>
<div class=\"row space-top\">
<div class=\"col-md-7 col-sm-7\">
<h3 class=\"heading center\">Атрибути<span></span></h3>
<div class=\"form-group\">
<div class=\"row\">
<div class=\"col-md-12\"><label>Дата народження    </label></div>
</div>
<div class=\"row\">
<div class=\"col-md-3 form-group\">
<div class=\"select-style\">
<select name=\"db_day\" id=\"db_day\">";
        // line 88
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 31));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 89
            echo "<option";
            if ((twig_date_format_filter($this->env, $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_birth"), "j") == (isset($context["i"]) ? $context["i"] : null))) {
                echo "selected";
            }
            echo " value=\"";
            echo (isset($context["i"]) ? $context["i"] : null);
            echo "\">";
            echo (isset($context["i"]) ? $context["i"] : null);
            echo "</option>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 91
        echo "</select>
</div>
</div>
<div class=\"col-md-5 form-group\">
<div class=\"select-style\">
<select name=\"db_month\" id=\"db_month\">";
        // line 97
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["months"]) ? $context["months"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
            // line 98
            echo "<option";
            if ((twig_date_format_filter($this->env, $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_birth"), "n") == $this->getAttribute((isset($context["m"]) ? $context["m"] : null), "m"))) {
                echo "selected";
            }
            echo " value=\"";
            echo $this->getAttribute((isset($context["m"]) ? $context["m"] : null), "m");
            echo "\">";
            echo $this->getAttribute((isset($context["m"]) ? $context["m"] : null), "name");
            echo "</option>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 101
        echo "
</select>
</div>
</div>
<div class=\"col-md-4 form-group\">
<div class=\"select-style\">
<select name=\"db_year\" id=\"db_year\">";
        // line 108
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1920, 2010));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 109
            echo "<option";
            if ((twig_date_format_filter($this->env, $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_birth"), "Y") == (isset($context["i"]) ? $context["i"] : null))) {
                echo "selected";
            }
            echo " value=\"";
            echo (isset($context["i"]) ? $context["i"] : null);
            echo "\">";
            echo (isset($context["i"]) ? $context["i"] : null);
            echo "</option>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 111
        echo "</select>
</div>
</div>
</div>
<label>Стать</label>
<div class=\"form-group\">
<div class=\"radio\">
<label><input type=\"radio\" name=\"user_gender\" id=\"user_gender\"";
        // line 118
        if (($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_gender") == "m")) {
            echo "checked";
        }
        echo " value=\"m\" > Чоловік</label>
</div>
<div class=\"radio\">
<label><input type=\"radio\" name=\"user_gender\" id=\"user_gender\"";
        // line 121
        if (($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_gender") == "f")) {
            echo "checked";
        }
        echo " value=\"f\"> Жінка</label>
</div>
</div>

</div>
</div>
<div class=\"col-md-5 col-sm-5\">
<h3 class=\"heading center\">Місцезнаходження<span></span></h3>
<div class=\"form-group\">
<label for=\"af-country\">Країна </label>
<div class=\"select-style\">
<select name=\"country\" id=\"country\">";
        // line 133
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clist"]) ? $context["clist"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 134
            echo "<option";
            if (($this->getAttribute((isset($context["c"]) ? $context["c"] : null), "country_iso") == $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_country"))) {
                echo "selected";
            }
            echo " value=\"";
            echo $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "country_iso");
            echo "\">";
            echo $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "country_printable_name");
            echo "</option>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 136
        echo "</select>
</div>
</div>
<div class=\"form-group\">
<label for=\"af-state\">Земля / Провінція</label>
<input class=\"form-control\" type=\"text\" value=\"";
        // line 141
        echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_state");
        echo "\" name=\"user_state\" >
\t\t\t\t\t\t</div>
                          <div class=\"form-group\">
                            <label for=\"af-city\">Місто</label>
                            <input class=\"form-control\" type=\"text\" value=\"";
        // line 145
        echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_city");
        echo "\"name=\"user_city\" >
                          </div>
                          <div class=\"form-group\">
                            <label for=\"af-city\">Адреса</label>
                            <input class=\"form-control\" type=\"text\" value=\"";
        // line 149
        echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_street");
        echo "\" name=\"user_street\" >
                          </div>

                          <div class=\"form-group\">
                            <label for=\"af-zip\">Почтовий Індекс</label>
                            <input class=\"form-control\" type=\"text\" value=\"";
        // line 154
        echo $this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_zip");
        echo "\" name=\"user_zip\" >
                          </div>
                        </div>
                       </div>
                       <div class=\"row space-top\">
                       \t<div class=\"col-md-3 col-sm-3 space-bottom\">
                        \t
                         
                        </div>
                        <div class=\"col-md-6 col-sm-6\">
                        \t<div class=\"text-center\">
                        
                              
                              <button class=\"btn btn-md btn-default2\" type=\"reset\" >Скасувати</button>
                              <button class=\"btn btn-md btn-primary\" id=\"doSave\" type=\"submit\" >Зберегти</button>
                          </div>
                        </div>
                       </div>
                      </form>
                    </div>
                    

                    
                    
                  </div>
                </div>
              </div>
          \t</div>
          </section>";
    }

    // line 185
    public function block_js($context, array $blocks = array())
    {
        // line 186
        echo "

<script src=\"/js/lib/plupload-2.1.9/plupload.full.min.js\"></script>
<script src=\"/js/lib/bootbox.min.js\"></script>
<script src=\"/js/lib/crope/cropper.js\"></script>

<script src=\"/js/profile.js\"></script>";
    }

    public function getTemplateName()
    {
        return "desktop/profile/index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  332 => 186,  329 => 185,  296 => 154,  288 => 149,  281 => 145,  274 => 141,  267 => 136,  253 => 134,  249 => 133,  233 => 121,  225 => 118,  216 => 111,  202 => 109,  198 => 108,  190 => 101,  176 => 98,  172 => 97,  165 => 91,  151 => 89,  147 => 88,  119 => 61,  112 => 59,  109 => 57,  107 => 56,  100 => 53,  89 => 45,  81 => 40,  72 => 34,  64 => 29,  55 => 23,  43 => 13,  40 => 12,  36 => 8,  33 => 7,  29 => 3,  26 => 2,);
    }
}
