<?php

/* desktop/events/index.tpl */
class __TwigTemplate_fe21e799a60b22c6ac8eaf514692140b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/root.tpl"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "
<!--Breadcrumbs-->
<div class=\"container space-top\">
    <ol class=\"breadcrumb\">
        <li><a href=\"index.tpl\">Головна</a></li>
        <li class=\"active\">Афіша</li>
    </ol>
</div><!--Breadcrumbs Close-->

<div class=\"container double-padding-bottom\">
    <div class=\"row\">

        <!--Blog Posts-->
        <section class=\"col-md-8 grid-view\">
            <!--Row-->
            <div class=\"row\">
                <div class=\"col-md-6 col-sm-6\">";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 21
            echo "                    <div class=\"post\">
                        <div class=\"featured-img\">
                            <a href=\"/events/view/";
            // line 23
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "poster_id");
            echo "\"><img class=\"img-responsive\"
                                                                        src=\"/userfiles/";
            // line 24
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "user_id");
            echo "/poster/";
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "poster_pic");
            echo "\"
                                                                        alt=\"Post01\"/></a>

                            <h3><a href=\"/events/view/";
            // line 27
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "poster_id");
            echo "\">";
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "poster_text");
            echo "</a></h3>
                            <span class=\"devider\"></span>
                            <div class=\"taxonomy\">
                                <a href=\"#\">";
            // line 30
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "user_nick");
            echo "</a></br>
                                Дата Події
                                <a href=\"#\">";
            // line 32
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "poster_date"), "d.m.Y");
            echo "</a>
                            </div>
                        </div>
                    </div>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 37
        echo "
                    <!--Pagination-->";
        // line 39
        if ($this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "previous")) {
            echo " <a href=\"";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "?p=";
            echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "previous");
            echo "\" style=\"margin: 4px 0;\"><i
                                class=\"fa fa-arrow-left\"></i></a>";
        }
        // line 41
        echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "info");
        // line 42
        if ($this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "next")) {
            echo "<a href=\"";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "?p=";
            echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "next");
            echo "\" style=\"margin: 6px 0 0 0;\"><i
                                class=\"fa fa-arrow-right\"></i></a>";
        }
        // line 44
        echo "                </div>
                    </div>
        </section><!--Blog Posts Close-->

        <!--Sidebar-->
        <div class=\"col-md-4\" id=\"sidebar\">
            <aside class=\"sidebar right\">

                <!--Recent/Featured posts widget-->
                <div class=\"widget rposts\">
                    <h3>Популярні події</h3>";
        // line 56
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["popul"]) ? $context["popul"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["pop"]) {
            // line 57
            echo "                    <div class=\"rpost\">
                        <div class=\"rpost-img\">
                            <img src=\"/userfiles/";
            // line 59
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "user_id");
            echo "/poster/";
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "poster_pic");
            echo "\" alt=\"\">
                        </div>
                        <div class=\"rpost-text\">
                            <h4><a href=\"/pub/view/";
            // line 62
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "poster_id");
            echo "\">";
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "poster_text");
            echo "</a></h4>
                            <p>Автор:";
            // line 63
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "user_nick");
            echo "</p>
                            <p>Дата:";
            // line 64
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "poster_date"), "d.m.Y");
            echo "</p>
                            <p>Час:";
            // line 65
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "poster_time");
            echo "</p>
                        </div>
                    </div>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pop'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 69
        echo "
                    <!-- Links Widget-->";
        // line 80
        echo "
            </aside>
        </div><!--Sidebar Close-->

    </div>
</div>";
    }

    // line 88
    public function block_js($context, array $blocks = array())
    {
        // line 89
        echo "<script src=\"/js/events.js\"></script>";
    }

    public function getTemplateName()
    {
        return "desktop/events/index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 89,  175 => 88,  166 => 80,  163 => 69,  154 => 65,  150 => 64,  146 => 63,  140 => 62,  132 => 59,  128 => 57,  124 => 56,  112 => 44,  103 => 42,  101 => 41,  92 => 39,  89 => 37,  79 => 32,  74 => 30,  66 => 27,  58 => 24,  54 => 23,  50 => 21,  46 => 20,  28 => 3,  25 => 2,);
    }
}
