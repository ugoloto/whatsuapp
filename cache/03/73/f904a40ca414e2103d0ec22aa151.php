<?php

/* desktop/posts/add.tpl */
class __TwigTemplate_0373f904a40ca414e2103d0ec22aa151 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/root.tpl"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_css($context, array $blocks = array())
    {
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "<section class=\"page-block user-account double-space-bottom\">
    <div class=\"container\">
        <div class=\"row\">";
        // line 12
        $template = $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/profile/menu.tpl"));
        $template->display($context);
        // line 13
        echo "

            <!--Content-->
            <div class=\"col-md-9 gray-bg\">
                <div class=\"tab-content\">


                    <!--Notifications-->

                    <div class=\"tab-pane fade in active\" id=\"notify\">
                        <h3 class=\"heading center\">Публікації</h3>
                        <div class=\"row\">
                            <div class=\"col-md-10 col-md-offset-1\">


                                <form id=\"posts\" enctype=\"multipart/form-data\">
                                    <input type=\"hidden\" name=\"fname\" id=\"img-file\">
                                    <div class=\"form-group\">
                                        <div id=\"containerpic\">
                                            <img class=\"img_responsive\" id=\"preview\" src=\"/img/nopic.jpg\"
                                                 style=\"display:none;\">
                                            <button id=\"pickfiles\" class=\"btn btn-primary\">Завантажити фото</button>
                                        </div>
                                    </div>
                                    <div class=\"form-group\">
                                        <label for=\"af-country\">Категорія</label>
                                        <div class=\"select-style\">
                                            <select name=\"category\" id=\"category\">";
        // line 41
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clist"]) ? $context["clist"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 42
            echo "                                                <option  value=\"";
            echo $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "cat_id");
            echo "\">";
            echo $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "cat_name");
            echo "</option>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 44
        echo "                                            </select>
                                        </div>
                                    </div>

                                    <div class=\"form-group\">
                                        <label for=\"af-first-name\">Коротко </label>
                                        <textarea class=\"form-control\" name=\"subj\" cols=\"5\"></textarea>

                                    </div>
                                    <div class=\"form-group\">
                                        <label>Пост</label>
                                        <textarea class=\"form-control\" name=\"post\" cols=\"50\"></textarea>
                                    </div>

                                    <a href=\"/posts\" class=\"btn btn-md btn-default2\" type=\"reset\">Скасувати</a>
                                    <button class=\"btn btn-md btn-primary pull-right\" id=\"doSave\" type=\"submit\">Додати
                                    </button>
                                </form>

                            </div>


                        </div>


                    </div>


                </div>
            </div>
        </div>
    </div>
</section>";
    }

    // line 79
    public function block_js($context, array $blocks = array())
    {
        // line 80
        echo "

<script src=\"/js/lib/plupload-2.1.9/plupload.full.min.js\"></script>
<script src=\"/js/lib/bootbox.min.js\"></script>

<script src=\"/js/lib/jquery-form.js\"></script>
<script src=\"/js/post.js\"></script>";
    }

    public function getTemplateName()
    {
        return "desktop/posts/add.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 80,  120 => 79,  84 => 44,  74 => 42,  70 => 41,  41 => 13,  38 => 12,  34 => 8,  31 => 7,  26 => 2,);
    }
}
