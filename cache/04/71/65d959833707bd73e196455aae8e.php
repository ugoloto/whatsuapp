<?php

/* desktop/common/contact.tpl */
class __TwigTemplate_047165d959833707bd73e196455aae8e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"sticky-btns\">
    <form class=\"quick-contact\" method=\"post\" name=\"quick-contact\">
        <h3>Зворотній зв'язок</h3>
        <div class=\"form-group\">
            <label for=\"qc-name\">Ім'я</label>
            <input class=\"form-control\" type=\"text\" name=\"qc-name\" id=\"qc-name\" placeholder=\"Введіть свое ім'я\"
                   required>
        </div>
        <div class=\"form-group\">
            <label for=\"qc-email\">Email</label>
            <input class=\"form-control\" type=\"email\" name=\"qc-email\" id=\"qc-email\" placeholder=\"Введіть email\" required>
        </div>
        <div class=\"form-group\">
            <label for=\"qc-message\">Повідомлення</label>
            <textarea class=\"form-control\" name=\"qc-message\" id=\"qc-message\" placeholder=\"Напишіть Ваше повідомлення\"
                      required></textarea>
        </div>
        <button class=\"btn btn-primary btn-block\" id=\"doSendContact\" type=\"submit\" >Надіслати</button>
    </form>
    <span id=\"qcf-btn\"><i class=\"fa fa-envelope\"></i></span>
    <span id=\"scrollTop-btn\"><i class=\"fa fa-chevron-up\"></i></span>
</div><!--Sticky Buttons Close-->
";
    }

    public function getTemplateName()
    {
        return "desktop/common/contact.tpl";
    }

    public function getDebugInfo()
    {
        return array (  93 => 61,  84 => 57,  76 => 54,  73 => 53,  69 => 52,  94 => 165,  87 => 158,  77 => 151,  68 => 36,  61 => 31,  47 => 23,  42 => 21,  37 => 19,  64 => 34,  60 => 31,  55 => 28,  40 => 22,  17 => 1,  303 => 239,  298 => 204,  293 => 37,  287 => 242,  285 => 239,  265 => 219,  262 => 218,  254 => 210,  251 => 209,  248 => 206,  246 => 204,  243 => 202,  239 => 200,  236 => 199,  234 => 198,  231 => 197,  131 => 96,  126 => 93,  123 => 91,  118 => 89,  109 => 84,  104 => 80,  99 => 77,  82 => 155,  75 => 54,  70 => 51,  59 => 40,  57 => 29,  20 => 1,  178 => 89,  175 => 88,  166 => 80,  163 => 69,  154 => 65,  150 => 64,  146 => 63,  140 => 62,  132 => 59,  128 => 94,  124 => 56,  112 => 87,  103 => 42,  101 => 78,  92 => 39,  89 => 37,  79 => 32,  74 => 30,  66 => 35,  58 => 30,  54 => 29,  50 => 26,  46 => 23,  28 => 3,  25 => 2,);
    }
}
