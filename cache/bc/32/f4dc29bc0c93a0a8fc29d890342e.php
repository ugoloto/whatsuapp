<?php

/* desktop/pub/index.tpl */
class __TwigTemplate_bc32f4dc29bc0c93a0a8fc29d890342e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/root.tpl"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<!--Breadcrumbs-->
<div class=\"container space-top\">
    <ol class=\"breadcrumb\">
        <li><a href=\"index.tpl\">Головна</a></li>
        <li class=\"active\">Публікації</li>";
        // line 10
        echo "    </ol>
</div><!--Breadcrumbs Close-->

<div class=\"container double-padding-bottom\">
    <div class=\"row\">
        <section class=\"col-md-8 list-view\">";
        // line 17
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 18
            echo "            <div class=\"row post\">
                <div class=\"col-md-4 col-sm-4\">
                    <input type=\"hidden\" name=\"cat\" id=\"cat\" value=\"";
            // line 20
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "cat_id");
            echo "\">
                    <a class=\"featured-img\" href=\"/pub/view/";
            // line 21
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "post_id");
            echo "\">
                        <img class=\"img-responsive\" src=\"/userfiles/";
            // line 22
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "user_id");
            echo "/posts/";
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "post_pic");
            echo "\" alt=\"Post01\"/></a>
                </div>
                <div class=\"col-md-8 col-sm-8\">
                    <h3><a href=\"/pub/view/";
            // line 25
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "post_id");
            echo "\">";
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "post_subj");
            echo "</a></h3>";
            // line 27
            echo "                    <span class=\"devider\"></span>
                    <div class=\"meta group\">
                        <div class=\"left\">
                            <a href=\"#\">";
            // line 30
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "user_nick");
            echo "</a>
                        </div>
                        <div class=\"right\"><span class=\"text-muted\">";
            // line 32
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "post_date"), "d.m.Y");
            echo "</span></div>
                    </div>
                </div>
            </div>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 38
        echo "







            <ul class=\"pagination\">";
        // line 48
        if ($this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "previous")) {
            echo " <a href=\"";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "?p=";
            echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "previous");
            if ((isset($context["pcat"]) ? $context["pcat"] : null)) {
                echo "&cat=";
                echo (isset($context["pcat"]) ? $context["pcat"] : null);
            }
            echo "\" style=\"margin: 4px 0;\"><i
                            class=\"fa fa-arrow-left\"></i></a>";
        }
        // line 50
        echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "info");
        // line 51
        if ($this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "next")) {
            echo "<a href=\"";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "?p=";
            echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "next");
            if ((isset($context["pcat"]) ? $context["pcat"] : null)) {
                echo "&cat=";
                echo (isset($context["pcat"]) ? $context["pcat"] : null);
            }
            echo "\" style=\"margin: 6px 0 0 0;\"><i
                            class=\"fa fa-arrow-right\"></i></a>";
        }
        // line 53
        echo "            </ul>




        </section>

        <!--Sidebar-->
        <div class=\"col-md-4\" id=\"sidebar\">
            <aside class=\"sidebar right\">

                <!--Recent/Featured posts widget-->
                <div class=\"widget rposts\">
                    <h3>Популярні</h3>";
        // line 67
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["popular"]) ? $context["popular"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["pop"]) {
            // line 68
            echo "                    <div class=\"rpost\">
                        <div class=\"rpost-img\">
                            <img src=\"/userfiles/";
            // line 70
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "user_id");
            echo "/posts/";
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "post_pic");
            echo "\" alt=\"\">
                        </div>
                        <div class=\"rpost-text\">
                            <h4><a href=\"/pub/view/";
            // line 73
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "post_id");
            echo "\">";
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "post_subj");
            echo "</a></h4>
                            <p>Автор публікації:";
            // line 74
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "user_nick");
            echo "</p>
                            <p>Дата публікації:";
            // line 75
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "post_date"), "d.m.Y");
            echo "</p>
                        </div>
                    </div>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pop'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 79
        echo "                </div>


                <!-- Links Widget-->
                <div class=\"widget links\">
                    <h3>Категорії</h3>

                    <ul class=\"list-unstyled\">";
        // line 87
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["category"]) ? $context["category"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 88
            echo "                        <li>
                          <a name=\"cat\" id=\"cat\" href=\"/pub?cat=";
            // line 89
            echo $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "cat_id");
            echo "\">";
            echo $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "cat_name");
            echo "</a>
                        </li>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 92
        echo "                    </ul>
                </div>
            </aside>
        </div><!--Sidebar Close-->

    </div>
</div>";
    }

    // line 102
    public function block_js($context, array $blocks = array())
    {
        // line 103
        echo "<script src=\"/js/pub.js\"></script>";
    }

    public function getTemplateName()
    {
        return "desktop/pub/index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 103,  214 => 102,  204 => 92,  194 => 89,  191 => 88,  187 => 87,  178 => 79,  169 => 75,  165 => 74,  159 => 73,  151 => 70,  147 => 68,  143 => 67,  128 => 53,  115 => 51,  113 => 50,  100 => 48,  90 => 38,  80 => 32,  75 => 30,  70 => 27,  65 => 25,  57 => 22,  53 => 21,  49 => 20,  45 => 18,  41 => 17,  34 => 10,  28 => 4,  25 => 3,);
    }
}
