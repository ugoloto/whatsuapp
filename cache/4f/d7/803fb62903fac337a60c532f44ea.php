<?php

/* desktop/favorites/index.tpl */
class __TwigTemplate_4fd7803fb62903fac337a60c532f44ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/root.tpl"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_css($context, array $blocks = array())
    {
        // line 3
        echo "<link href=\"/js/lib/crope/cropper.css\" rel=\"stylesheet\">";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "<section class=\"page-block user-account double-space-bottom\">
    <div class=\"container\">
        <div class=\"row\">";
        // line 12
        $template = $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/profile/menu.tpl"));
        $template->display($context);
        // line 13
        echo "

            <div class=\"col-md-10\">
                <div class=\"tab-content\">

                    <div class=\"tab-pane fade in active\" id=\"notify\">

                        <h3 class=\"heading center\">Обрані публікації</h3>
                        <div class=\"row\">
                            <div class=\"col-xs-12 col-md-12 gray-bg\">
                                <table class=\"table\">
                                    <thead>
                                    <tr>
                                        <th>Дата</th>
                                        <th>Тема</th>
                                        <th width=\"5%\"></th>
                                    </tr>
                                    </thead>";
        // line 31
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 32
            echo "                                    <tr>
                                        <td>";
            // line 33
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "fp_date"), "d.m.Y");
            echo "</td>
                                        <td><a href=\"/pub/view/";
            // line 34
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "fp_post_id");
            echo "\"><h3>";
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "post_subj");
            echo "</h3></td>
                                        <td>
                                            <a href=\"#\" class=\"del\" data-id=\"";
            // line 36
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "fp_id");
            echo "\"><i
                                                        class=\"fa fa-times\"></i></a>
                                        </td>
                                    </tr>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 41
        echo "                                </table>
                            </div>


                        </div>";
        // line 47
        if ($this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "previous")) {
            echo " <a href=\"";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "?p=";
            echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "previous");
            echo "\"
                                                      style=\"margin: 4px 0;\"><i
                                    class=\"fa fa-arrow-left\"></i></a>";
        }
        // line 50
        echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "info");
        // line 51
        if ($this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "next")) {
            echo "<a href=\"";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "?p=";
            echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "next");
            echo "\" style=\"margin: 6px 0 0 0;\"><i
                                    class=\"fa fa-arrow-right\"></i></a>";
        }
        // line 53
        echo "
                    </div>


                </div>
            </div>
        </div>


    </div>
</section>";
    }

    // line 67
    public function block_js($context, array $blocks = array())
    {
        // line 68
        echo "

<script src=\"/js/lib/plupload-2.1.9/plupload.full.min.js\"></script>
<script src=\"/js/lib/bootbox.min.js\"></script>
<script src=\"/js/lib/crope/cropper.js\"></script>
<script src=\"/js/favorites.js\"></script>";
    }

    public function getTemplateName()
    {
        return "desktop/favorites/index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 68,  131 => 67,  117 => 53,  108 => 51,  106 => 50,  96 => 47,  90 => 41,  80 => 36,  73 => 34,  69 => 33,  66 => 32,  62 => 31,  43 => 13,  40 => 12,  36 => 8,  33 => 7,  29 => 3,  26 => 2,);
    }
}
