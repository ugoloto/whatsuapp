<?php

/* desktop/signup/index.tpl */
class __TwigTemplate_088777b6cbb722fcc2d06d6f8bc0a1fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/root.tpl"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_css($context, array $blocks = array())
    {
        // line 3
        echo "<style>
    .error {
        color: red;
    }
</style>";
    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        // line 11
        echo "<section class=\"specialty-page\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-8 col-md-offset-2 col-sm-12\">
                <div class=\"page-heading\">
                    <h2>РЕЄСТРАЦІЯ</h2>";
        // line 19
        echo "                </div>
                <div class=\"row\">
                    <div class=\"col-md-6 col-sm-6\">
                        <form class=\"signup-form space-bottom\" id=\"regForm\">
                            <div class=\"form-group\">
                                <label class=\"sr-only\" for=\"username\"></label>
                                <input type=\"email\" class=\"form-control input-lg\" name=\"username\" id=\"username\"
                                       placeholder=\"E-mail\" required>
                            </div>
                            <div class=\"form-group\">
                                <label class=\"sr-only\" for=\"password\"></label>
                                <input type=\"password\" class=\"form-control input-lg\" name=\"pass\" id=\"pass\"
                                       placeholder=\"Пароль\" required>
                            </div>
                            <div class=\"form-group\">
                                <label class=\"sr-only\" for=\"password2\"></label>
                                <input type=\"password\" class=\"form-control input-lg\" name=\"cpass\" id=\"cpass\"
                                       placeholder=\"Підтвердіть пароль\" required>
                            </div>
                            <button class=\"btn btn-md btn-primary btn-center\" type=\"submit\">Зареєструватись</button>
                        </form>
                    </div>
                    <div class=\"col-md-6 col-sm-6 social-sugnup\">
                        <a class=\"twitter\" href=\"#\">Twitter </a>
                        <a class=\"facebook\" href=\"#\">Facebook </a>
                        <a class=\"pinterest\" href=\"#\">Google Plus</a>
                    </div>
                </div>
                <p class=\"helper-text\">Реєструючись ви згодні з нашими <a href=\"#\"> умовами та правилами </a>
                    користування сайтом</p>
                <p>Вже зареєстровані? <a href=\"/login\">Увійти</a></p>
            </div>
        </div>
    </div>
</section>";
    }

    // line 56
    public function block_js($context, array $blocks = array())
    {
        // line 57
        echo "<script src=\"/js/signup.js\"></script>";
    }

    public function getTemplateName()
    {
        return "desktop/signup/index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 57,  85 => 56,  47 => 19,  40 => 11,  37 => 10,  29 => 3,  26 => 2,);
    }
}
