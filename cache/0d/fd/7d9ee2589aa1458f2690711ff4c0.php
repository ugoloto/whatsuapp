<?php

/* desktop/common/root.tpl */
class __TwigTemplate_0dfd7d9ee2589aa1458f2690711ff4c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<!--Conditionals for IE8-9 Support-->
<!--[if IE]>
<html lang=\"en\" class=\"ie\"><![endif]-->
<html>
<head>
    <meta charset=\"utf-8\">

    <title>autoforum</title>

    <!--SEO Meta Tags-->
    <meta name=\"description\" content=\"News from europe for ukrainian\"/>
    <meta name=\"keywords\" content=\"germany europe news ukraine\"/>
    <meta name=\"author\" content=\"Julia\"/>
    <!--Mobile Specific Meta Tag-->
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\"/>
    <!--Favicon-->
    <link rel=\"shortcut icon\" href=\"favicon.ico\" type=\"image/x-icon\">
    <link rel=\"icon\" href=\"favicon.ico\" type=\"image/x-icon\">
    <!--Master Slider Styles-->
    <link href=\"/assets/masterslider/style/masterslider.css\" rel=\"stylesheet\" media=\"screen\">
    <!--Kedavra Stylesheet-->
    <link href=\"/assets/css/styles.css?v=1\" rel=\"stylesheet\" media=\"screen\">
    <link href=\"/css/app.css\" rel=\"stylesheet\" media=\"screen\">
    <!--Kedavra Color Scheme-->
    <link class=\"color-scheme\" href=\"/assets/css/colors/color-2f8cea.css?v=3\" rel=\"stylesheet\" media=\"screen\">

    <link href=\"/assets/js/plugins/sweetalert/sweetalert.min.css\" rel=\"stylesheet\" media=\"screen\">
    <link href=\"http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.css\" rel=\"stylesheet\"
          type=\"text/css\"/>
    <!--Modernizr-->
    <script src=\"/assets/js/libs/modernizr.custom.js\"></script>
    <!--Adding Media Queries Support for IE8-->
    <!--[if lt IE 9]>
    <script src=\"/assets/js/plugins/respond.js\"></script>
    <![endif]-->";
        // line 37
        $this->displayBlock('css', $context, $blocks);
        // line 40
        echo "
</head>
<body class=\"parallax animated fadeIn\">
<!--Off-Canvas-->
<div class=\"off-canvas-wrap\" data-offcanvas>
    <div class=\"inner-wrap\">

        <!--Off-Canvas Menu-->
         <aside class=\"left-off-canvas-menu\">
  <div class=\"mobile-navi\">";
        // line 51
        if ((((isset($context["topHome"]) ? $context["topHome"] : null) || (isset($context["smProfile"]) ? $context["smProfile"] : null)) || (isset($context["smFavorites"]) ? $context["smFavorites"] : null))) {
            echo " <div style=\"display: none\"></div>
      <!--Quick Search-->";
        } else {
            // line 54
            echo "                 <form method=\"get\" class=\"mobile-search\">
                     <label class=\"sr-only\" for=\"m-search\"></label>
                     <input class=\"form-control input-lg\" name=\"s\" id=\"m-search\" type=\"text\" placeholder=\"Пошук\">
                     <button type=\"submit\"><i class=\"flaticon-search100\"></i></button>
                 </form>";
        }
        // line 60
        echo "                 <ul class=\"buttons\">
                     <li class=\"lang\"><a href=\"#\">Мова<i></i></a>
                         <ul class=\"submenu\">
                             <li><a href=\"#\">Укр</a></li>
                             <li><a href=\"#\">Deu</a></li>

                         </ul>
                     </li>
                     <li class=\"country\"><a href=\"#\">Країна<i></i></a>
                         <ul class=\"submenu\">
                             <li><a href=\"#\"> Німеччина</a></li>
                             <li><a href=\"#\"> Швейцарія</a></li>
                         </ul>
                     </li>
                 </ul>
                 <ul class=\"tools\">";
        // line 77
        if ($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id")) {
            // line 78
            echo "                     <li><a href=\"/logout\" style=\"color: red;\">Вихід</a></li>";
        } else {
            // line 80
            echo "                     <li><a href=\"/signup\" style=\"color: red;\">Реєстрація</a></li>

                     <li><a href=\"/login\">Логін</a></li>";
        }
        // line 84
        echo "                 </ul>
                 <ul>";
        // line 87
        echo "                     <li class=\"";
        echo (isset($context["topHome"]) ? $context["topHome"] : null);
        echo "\"><a href=\"/\">Головна</a></li>

                     <li class=\"";
        // line 89
        echo (isset($context["topPub"]) ? $context["topPub"] : null);
        echo "\"><a href=\"/pub\">Публікації</a></li>

                     <li class=\"";
        // line 91
        echo (isset($context["topEvents"]) ? $context["topEvents"] : null);
        echo "\"><a href=\"/events\">Афіша</a></li>";
        // line 93
        if ($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id")) {
            // line 94
            echo "                       <div class=\"user\"><a href=\"/profile\"><i class=\"fa fa-user\"></i>Кабінет</a><span></span></div>";
        }
        // line 96
        echo "                     <!--

                     <li><a href=\"shop-grid-1-big-banner.html\">Shop</a><span></span>
                         <ul class=\"submenu\">
                             <li><a href=\"shop-grid-1-big-banner.html\">Grid Layout Type 1</a><span></span>
                                 <ul class=\"sub-submenu\">
                                     <li><a href=\"shop-grid-1-fw.html\">Grid Full Width</a></li>
                                     <li><a href=\"shop-grid-1-big-banner.html\">Grid Big Banner</a></li>
                                     <li><a href=\"shop-grid-1-sl.html\">Grid Sidebar Left</a></li>
                                     <li><a href=\"shop-grid-1-sr.html\">Grid Sidebar Right</a></li>
                                 </ul>
                             </li>
                             <li><a href=\"shop-grid-2-big-banner.html\">Grid Layout Type 2</a><span></span>
                                 <ul class=\"sub-submenu\">
                                     <li><a href=\"shop-grid-2-fw.html\">Grid Full Width</a></li>
                                     <li><a href=\"shop-grid-2-big-banner.html\">Grid Big Banner</a></li>
                                     <li><a href=\"shop-grid-2-sl.html\">Grid Sidebar Left</a></li>
                                     <li><a href=\"shop-grid-2-sr.html\">Grid Sidebar Right</a></li>
                                 </ul>
                             </li>
                             <li><a href=\"shop-list-big-banner.html\">List Layout</a><span></span>
                                 <ul class=\"sub-submenu\">
                                     <li><a href=\"shop-list-fw.html\">List Full Width</a></li>
                                     <li><a href=\"shop-list-big-banner.html\">List Big Banner</a></li>
                                     <li><a href=\"shop-list-sl.html\">List Sidebar Left</a></li>
                                     <li><a href=\"shop-list-sr.html\">List Sidebar Right</a></li>
                                 </ul>
                             </li>
                             <li><a href=\"shop-single-dr.html\">Single Product</a><span></span>
                                 <ul class=\"sub-submenu\">
                                     <li><a href=\"shop-single-dr.html\">Description Right</a></li>
                                     <li><a href=\"shop-single-dl.html\">Description Left</a></li>
                                 </ul>
                             </li>
                             <li><a href=\"shopping-cart.html\">Shopping Cart</a></li>
                             <li><a href=\"wishlist.html\">Wishlist</a></li>
                             <li><a href=\"checkout.html\">Checkout</a><span></span>
                                 <ul class=\"sub-submenu\">
                                     <li><a href=\"checkout.html\">Checkout Page</a></li>
                                     <li><a href=\"checkout-unregistered.html\">Unregistered User</a></li>
                                 </ul>
                             </li>
                             <li><a href=\"shop-delivery.html\">Delivery Info Page</a></li>
                             <li><a href=\"shop-tracking.html\">Delivery Tracking</a></li>
                             <li><a href=\"shop-history.html\">Purchase History</a></li>
                         </ul>
                     </li>
                     <li><a href=\"pages.html\">Pages</a><span></span>
                         <ul class=\"submenu\">
                             <li><a href=\"login.html\">Login Page</a></li>
                             <li><a href=\"signup.html\">Sign Up Page</a></li>
                             <li><a href=\"user-account.html\">User Account Page</a></li>
                             <li><a href=\"resume.html\">Resume / Portfolio</a></li>
                             <li><a href=\"portfolio-single.html\">Single Project Page</a></li>
                             <li><a href=\"events-schedule.html\">Events Schedule</a></li>'
                             <li><a href=\"about-corporate.html\">About Us</a><span></span>
                                 <ul class=\"sub-submenu\">
                                     <li><a href=\"about-corporate.html\">About Corporate</a></li>
                                     <li><a href=\"about-personal.html\">About Personal</a></li>
                                 </ul>
                             </li>
                             <li><a href=\"contacts.html\">Contacts</a></li>
                             <li><a href=\"team.html\">Our Team</a></li>
                             <li><a href=\"team-profile.html\">Team Member Profile</a></li>
                             <li><a href=\"faq-tabs.html\">FAQ/Support Pages</a><span></span>
                                 <ul class=\"sub-submenu\">
                                     <li><a href=\"faq-tabs.html\">Tabs Version</a></li>
                                     <li><a href=\"faq-standard.html\">Standard Version</a></li>
                                 </ul>
                             </li>
                             <li><a href=\"under-construction.html\">Under Construction</a></li>
                             <li><a href=\"search.html\">Search Page</a></li>
                             <li><a href=\"search-results.html\">Search Results</a></li>
                             <li><a href=\"404.html\">Error Pages</a><span></span>
                                 <ul class=\"sub-submenu\">
                                     <li><a href=\"404.html\">404 Page</a></li>
                                     <li><a href=\"500.html\">500 Page</a></li>
                                 </ul>
                             </li>
                             <li><a href=\"sitemap.html\">Sitemap</a></li>
                         </ul>
                     </li>
                     <li><a href=\"components-and-styles.html\">Features</a><span></span>
                         <ul class=\"submenu\">
                             <li><a href=\"components-and-styles.html\">Components &amp; Styles</a></li>
                             <li><a href=\"sliders-and-carousels.html\">Sliders &amp; Carousels</a></li>
                             <li><a href=\"grid-and-list.html\">Grid &amp; List Layouts</a></li>
                         </ul>
                     </li>
                     -->
                 </ul>
         </div>
         </aside><!--Off-Canvas Menu Close-->



        <!--Layout-->
        <div class=\"site-layout\">
            <!--If you add class \"boxed\" to .site-layout it wraps the whole document in a box, than you can simply add pattern background to body or leave it white. Please note in a \"boxed\" mode header doesn't stick to the top.-->";
        // line 197
        $template = $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/headtoolbar.tpl"));
        $template->display($context);
        // line 198
        echo "            <!--Header-->";
        // line 199
        $template = $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/nav.tpl"));
        $template->display($context);
        // line 200
        echo "
            <!--Page Content--><!-- InstanceBeginEditable name=\"content\" -->
            <div class=\"page";
        // line 202
        echo (isset($context["page_blog"]) ? $context["page_blog"] : null);
        echo "\">";
        // line 204
        $this->displayBlock('content', $context, $blocks);
        // line 206
        echo "            </div>
            <!--Page Content Close-->";
        // line 209
        $template = $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/footer.tpl"));
        $template->display($context);
        // line 210
        echo "
        </div><!--Layout Close-->

        <!-- close the off-canvas menu -->
        <a class=\"exit-off-canvas\"></a>
    </div>
</div><!--Off-Canvas Close-->";
        // line 218
        $template = $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/contact.tpl"));
        $template->display($context);
        // line 219
        echo "
<!--Javascript (jQuery) Libraries and Plugins-->
<script src=\"/assets/js/libs/jquery-1.11.1.min.js\"></script>
<script src=\"/assets/js/libs/jquery.easing.min.js\"></script>
<script src=\"/assets/js/plugins/bootstrap.min.js\"></script>
<script src=\"/assets/js/plugins/smoothscroll.js\"></script>
<script src=\"/assets/js/plugins/utilities.js\"></script>
<script src=\"/assets/js/plugins/foundation.min.js\"></script>
<script src=\"/assets/js/plugins/jquery.placeholder.js\"></script>
<script src=\"/assets/js/plugins/icheck.min.js\"></script>
<script src=\"/assets/js/plugins/jquery.validate.min.js\"></script>
<script src=\"/assets/js/plugins/waypoints.min.js\"></script>

<script src=\"/js/lang.js\"></script>
<script src=\"/js/contact.js\"></script>
<script src=\"/assets/js/plugins/card.min.js\"></script>
<script src=\"/assets/js/scripts.js\"></script>
<script src=\"/assets/js/plugins/sweetalert/sweetalert.min.js\"></script>
<script src=\"/js/footer.js\"></script>";
        // line 239
        $this->displayBlock('js', $context, $blocks);
        // line 242
        echo "
</body><!--Body Close-->
</html>";
    }

    // line 37
    public function block_css($context, array $blocks = array())
    {
    }

    // line 204
    public function block_content($context, array $blocks = array())
    {
    }

    // line 239
    public function block_js($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "desktop/common/root.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  303 => 239,  298 => 204,  293 => 37,  287 => 242,  285 => 239,  265 => 219,  262 => 218,  254 => 210,  251 => 209,  248 => 206,  246 => 204,  243 => 202,  239 => 200,  236 => 199,  234 => 198,  231 => 197,  131 => 96,  126 => 93,  123 => 91,  118 => 89,  109 => 84,  104 => 80,  99 => 77,  82 => 60,  75 => 54,  70 => 51,  59 => 40,  57 => 37,  20 => 1,  178 => 89,  175 => 88,  166 => 80,  163 => 69,  154 => 65,  150 => 64,  146 => 63,  140 => 62,  132 => 59,  128 => 94,  124 => 56,  112 => 87,  103 => 42,  101 => 78,  92 => 39,  89 => 37,  79 => 32,  74 => 30,  66 => 27,  58 => 24,  54 => 23,  50 => 21,  46 => 20,  28 => 3,  25 => 2,);
    }
}
