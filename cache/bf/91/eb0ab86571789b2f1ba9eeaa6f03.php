<?php

/* desktop/posts/index.tpl */
class __TwigTemplate_bf91eb0ab86571789b2f1ba9eeaa6f03 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/root.tpl"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_css($context, array $blocks = array())
    {
        // line 3
        echo "<link href=\"/js/lib/crope/cropper.css\" rel=\"stylesheet\">";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "<section class=\"page-block user-account double-space-bottom\">
    <div class=\"container\">
        <div class=\"row\">";
        // line 12
        $template = $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/profile/menu.tpl"));
        $template->display($context);
        // line 13
        echo "

            <div class=\"col-md-10\">
                <div class=\"tab-content\">

                    <div class=\"tab-pane fade in active\" id=\"notify\">

                        <h3 class=\"heading center\">Публікації <a class=\"btn btn-success pull-right\" href=\"/posts/add\">Новий
                                пост</a></h3>
                        <div class=\"row\">
                            <div class=\"col-xs-12 col-md-12 gray-bg\">
                                <table class=\"table\">
                                    <thead>
                                    <tr>

                                        <th>Дата</th>";
        // line 29
        if (($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_role") != "user")) {
            // line 30
            echo "                                        <th>Юзер</th>";
        }
        // line 31
        echo "                                        <th>Тема</th>
                                        <th>Категорія</th>
                                        <th width=\"5%\">Дії</th>
                                        <th>Статус</th>
                                        <th>Кількість</br> переглядів</th>

                                    </tr>
                                    </thead>";
        // line 39
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["records"]) ? $context["records"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 40
            echo "                                    <tr>
                                        <td>";
            // line 41
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "post_date"), "d.m.Y");
            echo "</td>";
            // line 42
            if (($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_role") != "user")) {
                // line 43
                echo "                                        <th>";
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "user_nick");
                echo "</th>";
            }
            // line 44
            echo "                                        <td><h3>";
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "post_subj");
            echo "</h3></td>
                                        <td>";
            // line 45
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "cat_name");
            echo "</td>
                                        <td>
                                            <a href=\"/posts/edit/";
            // line 47
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "post_id");
            echo "\"><i class=\"fa fa-pencil\"></i></a>
                                            <a href=\"#\" class=\"del\" data-id=\"";
            // line 48
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "post_id");
            echo "\"><i class=\"fa fa-times\"></i></a>
                                        <td>";
            // line 49
            if (($this->getAttribute((isset($context["r"]) ? $context["r"] : null), "post_status") == 0)) {
                echo "Не опубліковано";
            } else {
                echo " Опубліковано";
            }
            echo "</td>
                                        </td>
                                        <td>";
            // line 51
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "post_view");
            echo "</td>
                                    </tr>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 56
        echo "                                </table>
                            </div>


                        </div>";
        // line 62
        if ($this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "previous")) {
            echo " <a href=\"";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "?p=";
            echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "previous");
            echo "\"
                                                      style=\"margin: 4px 0;\"><i
                                    class=\"fa fa-arrow-left\"></i></a>";
        }
        // line 65
        echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "info");
        // line 66
        if ($this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "next")) {
            echo "<a href=\"";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "?p=";
            echo $this->getAttribute((isset($context["pagination"]) ? $context["pagination"] : null), "next");
            echo "\" style=\"margin: 6px 0 0 0;\"><i
                                    class=\"fa fa-arrow-right\"></i></a>";
        }
        // line 68
        echo "
                    </div>


                </div>
            </div>
        </div>


    </div>
</section>";
    }

    // line 82
    public function block_js($context, array $blocks = array())
    {
        // line 83
        echo "

<script src=\"/js/lib/plupload-2.1.9/plupload.full.min.js\"></script>
<script src=\"/js/lib/bootbox.min.js\"></script>
<script src=\"/js/lib/crope/cropper.js\"></script>
<script src=\"/js/post.js\"></script>
<script src=\"/js/profile.js\"></script>";
    }

    public function getTemplateName()
    {
        return "desktop/posts/index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 83,  167 => 82,  153 => 68,  144 => 66,  142 => 65,  132 => 62,  126 => 56,  118 => 51,  109 => 49,  105 => 48,  101 => 47,  96 => 45,  91 => 44,  86 => 43,  84 => 42,  81 => 41,  78 => 40,  74 => 39,  65 => 31,  62 => 30,  60 => 29,  43 => 13,  40 => 12,  36 => 8,  33 => 7,  29 => 3,  26 => 2,);
    }
}
