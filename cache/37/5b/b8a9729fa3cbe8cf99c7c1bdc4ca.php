<?php

/* desktop/pub/view.tpl */
class __TwigTemplate_375bb8a9729fa3cbe8cf99c7c1bdc4ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate(((isset($context["skin"]) ? $context["skin"] : null) . "/common/root.tpl"));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<section class=\"container space-top double-space-bottom\">
    <div class=\"row\">

        <!--Content-->
        <section class=\"col-md-8\">

            <h2 class=\"post-title\">Фото події</h2>
            <input type=\"hidden\" name=\"post_id\" value=\"";
        // line 10
        echo $this->getAttribute((isset($context["post"]) ? $context["post"] : null), "post_id");
        echo "\">

            <!--Slider-->


            <img src=\"/userfiles/";
        // line 15
        echo $this->getAttribute((isset($context["rec"]) ? $context["rec"] : null), "user_id");
        echo "/posts/";
        echo $this->getAttribute((isset($context["rec"]) ? $context["rec"] : null), "post_pic");
        echo "\"/>
            <!-- slide caption -->


            <h3>";
        // line 19
        echo $this->getAttribute((isset($context["rec"]) ? $context["rec"] : null), "post_subj");
        echo "</h3>

            <p>";
        // line 21
        echo $this->getAttribute((isset($context["rec"]) ? $context["rec"] : null), "post_text");
        echo "</p>

            <!--Post Toolbox-->
            <div class=\"post-toolbox \">
                <a class=\"btn btn-md btn-outlined btn-primary\" data-id=\"";
        // line 25
        echo $this->getAttribute((isset($context["rec"]) ? $context["rec"] : null), "post_id");
        echo "\" id=\"doFav\">Додати в обране<i
                            class=\"fa fa-heart-o right\"></i></a>
                <div class=\"buttons\">
                    <a href=\"#\"><i class=\"fa fa-plus\"></i></a>
                    <a href=\"#\" data-toggle=\"modal\" data-target=\"#shareModal\"><i class=\"fa fa-share\"></i></a>
                    <a onclick=\"Share.facebook('URL','TITLE','DESC')\"><i class=\"fa fa-facebook\"></i></a>
                    <a onclick=\"Share.twitter('URL','TITLE')\"><i class=\"fa fa-twitter\"></i></a>
                </div>
            </div>

            <!--Post Meta-->
            <div class=\"post-meta\">
                <div class=\"taxonomy\">
                    <a href=\"#\">";
        // line 38
        echo $this->getAttribute((isset($context["rec"]) ? $context["rec"] : null), "user_nick");
        echo "</a>
                </div>
                <div class=\"date\">";
        // line 40
        echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["rec"]) ? $context["rec"] : null), "post_date"), "d.m.Y");
        echo "</div>
            </div>

            <!--Disqus Comments-->";
        // line 44
        if ($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id")) {
            // line 45
            echo "            <div class=\"double-space-top\">
                <div class=\"page-heading center\">
                    <h3>Залиште коментар</h3>
                    <p>Будь-ласка, переконайтеся що Ви увійшли у систему, щоб залишити коментар</p>
                </div>
                <form id = \"doAddComments\">
                    <input type=\"hidden\" name=\"post_id\" value=\"";
            // line 51
            echo $this->getAttribute((isset($context["rec"]) ? $context["rec"] : null), "post_id");
            echo "\">
                    <div class=\"form-group\">
                    <textarea class=\"form-control\" name=\"text\" cols=\"50\" placeholder=\"Введіть коментар\"></textarea>
                    </div>
                    <a href=\"#\" class=\"btn btn-md btn-default2\" type=\"reset\">Скасувати</a>
                    <button class=\"btn btn-md btn-primary pull-right\" id=\"doSave\"  type=\"submit\">Додати
                    </button>
                </form>

            </div>";
        }
        // line 64
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["comments"]) ? $context["comments"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 65
            echo "
            <input type=\"hidden\" id=\"post_id\"  name=\"post_id\" value=\"";
            // line 66
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "post_id");
            echo "\">
            <div class=\" panel static panel-default space-top\">
                <div class=\" panel-heading\">
                    <div class=\"col-md-2\">";
            // line 71
            echo "           <img class=\"img-responsive center-block\" src=\"/userfiles/";
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "user_id");
            echo "/avatar/";
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "user_pic");
            echo "\" style=\"margin-top: 10px;\"/>

                    </div>
                    <a href=\"#\">";
            // line 74
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "user_nick");
            echo "</a>
                    <div class=\"col-md-10 panel-body\">
                           <p><h4>";
            // line 76
            echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "comments_text");
            echo "</h4></p>
                        </div>
                        <div class=\"right\"><span class=\"text-muted\">";
            // line 78
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "comments_date"), "d.m.Y");
            echo "</span></div>

                </div>";
            // line 81
            if (($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_role") == "admin")) {
                // line 82
                echo "
                <a href=\"#\" class=\"D\" data-id=\"";
                // line 83
                echo $this->getAttribute((isset($context["r"]) ? $context["r"] : null), "comments_id");
                echo "\"><i class=\"fa fa-times\"></i></a>";
            }
            // line 85
            echo "            </div>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 90
        echo "

        </section><!--Content Close-->

        <!--Sidebar-->
        <div class=\"col-md-4 space-bottom\" id=\"sidebar\">
            <aside class=\"sidebar right\">

                <!--Search Widget-->

                <!--Recent/Featured posts widget-->
                <div class=\"widget rposts\">
                    <h3>Популярні</h3>";
        // line 103
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["popular"]) ? $context["popular"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["pop"]) {
            // line 104
            echo "                    <div class=\"rpost\">
                        <div class=\"rpost-img\">
                            <img src=\"/userfiles/";
            // line 106
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "user_id");
            echo "/posts/";
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "post_pic");
            echo "\" alt=\"\">
                        </div>
                        <div class=\"rpost-text\">
                            <h4><a href=\"/pub/view/";
            // line 109
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "post_id");
            echo "\">";
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "post_subj");
            echo "</a></h4>
                            <p>Автор публікації:";
            // line 110
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "user_nick");
            echo "</p>
                            <p>Дата публікації:";
            // line 111
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "post_date"), "d.m.Y");
            echo "</p>
                        </div>
                    </div>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pop'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 115
        echo "                </div>


                <!-- Links Widget-->
                <div class=\"widget links\">
                    <h3>Категорії</h3>

                    <ul class=\"list-unstyled\">";
        // line 123
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["category"]) ? $context["category"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 124
            echo "                        <li>
                            <a name=\"cat\" id=\"cat\" href=\"/pub?cat=";
            // line 125
            echo $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "cat_id");
            echo "\">";
            echo $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "cat_name");
            echo "</a>
                        </li>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 128
        echo "                    </ul>
                </div>
            </aside>
        </div><!--Sidebar Close-->
    </div>
</section><!--Single Post Content Close-->";
    }

    // line 136
    public function block_js($context, array $blocks = array())
    {
        // line 137
        echo "<script src=\"/js/comments.js\"></script>
<script src=\"/js/favorites.js\"></script>
<script src=\"/js/Share.js\"></script>
<script src=\"/js/pub.js\"></script>";
    }

    public function getTemplateName()
    {
        return "desktop/pub/view.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  255 => 137,  252 => 136,  243 => 128,  233 => 125,  230 => 124,  226 => 123,  217 => 115,  208 => 111,  204 => 110,  198 => 109,  190 => 106,  186 => 104,  182 => 103,  168 => 90,  162 => 85,  158 => 83,  155 => 82,  153 => 81,  148 => 78,  143 => 76,  138 => 74,  129 => 71,  123 => 66,  120 => 65,  116 => 64,  103 => 51,  95 => 45,  93 => 44,  87 => 40,  82 => 38,  66 => 25,  59 => 21,  54 => 19,  45 => 15,  37 => 10,  28 => 3,  25 => 2,);
    }
}
