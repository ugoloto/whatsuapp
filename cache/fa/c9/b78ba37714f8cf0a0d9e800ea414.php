<?php

/* desktop/profile/menu.tpl */
class __TwigTemplate_fac9b78ba37714f8cf0a0d9e800ea414 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"col-md-2 navigation\">
    <h3 class=\"heading center\">Налаштування<span></span></h3>
    <ul class=\"nav-tabs vertical\" role=\"tablist\">
        <li class=\"";
        // line 4
        echo (isset($context["smProfile"]) ? $context["smProfile"] : null);
        echo "\"><a href=\"/profile\">Аккаунт<span class=\"line\"></span></a></li>
        <li class=\"";
        // line 5
        echo (isset($context["smPosts"]) ? $context["smPosts"] : null);
        echo "\"><a href=\"/posts\">Публікації<span class=\"line\"></span></a></li>";
        // line 6
        if (($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_role") != "user")) {
            // line 7
            echo "        <li class=\"";
            echo (isset($context["smPosters"]) ? $context["smPosters"] : null);
            echo "\"><a href=\"/poster\">Афіша<span class=\"line\"></span></a></li>";
        }
        // line 8
        echo "        <li class=\"";
        echo (isset($context["smFavorites"]) ? $context["smFavorites"] : null);
        echo "\"><a href=\"/favorites\">Обране<span class=\"line\"></span></a></li>
    </ul>
</div>
";
    }

    public function getTemplateName()
    {
        return "desktop/profile/menu.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 7,  22 => 4,  17 => 1,  332 => 186,  329 => 185,  296 => 154,  288 => 149,  281 => 145,  274 => 141,  267 => 136,  253 => 134,  249 => 133,  233 => 121,  225 => 118,  216 => 111,  202 => 109,  198 => 108,  190 => 101,  176 => 98,  172 => 97,  165 => 91,  151 => 89,  147 => 88,  119 => 61,  112 => 59,  109 => 57,  107 => 56,  100 => 53,  89 => 45,  81 => 40,  72 => 34,  64 => 29,  55 => 23,  43 => 13,  40 => 12,  36 => 8,  33 => 7,  29 => 6,  26 => 5,);
    }
}
