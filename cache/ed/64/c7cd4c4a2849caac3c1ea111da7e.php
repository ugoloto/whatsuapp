<?php

/* desktop/common/footer.tpl */
class __TwigTemplate_ed64c7cd4c4a2849caac3c1ea111da7e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--Footer-->
<footer class=\"footer\">

    <!-- Footer Subscribe -->
    <div class=\"footer-subscribe-widget light-version\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-7\">
                    <p>Підписатися на наші новини. Не хвилюйтеся, ми не спамимо Вас!</p>
                </div>
                <div class=\"col-md-5\">
                    <form class=\"validate\" id=\"subs\">
                        <div class=\"row\">
                            <div class=\"col-sm-8\">
                                <input type=\"email\" class=\"form-control\" name=\"EMAIL\" id=\"subscr\"
                                       placeholder=\"Email\" required>
                            </div>

                          <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style=\"position: absolute; left: -5000px;\"><input type=\"text\"
                                                                                   name=\"b_168a366a98d3248fbc35c0b67_06b7eb4e02\"
                                                                                   tabindex=\"-1\" value=\"\"></div>
                            <div class=\"col-sm-4\">
                                <button type=\"submit\" class=\"btn btn-transparent btn-md btn-primary\">Підписатися
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class=\"container space-top\">
        <div class=\"row\">

            <!--Featured Posts-->
            <div class=\"col-md-3 col-sm-3 col-xs-6\">
                <h3>autoforum?</h3>
                <div class=\"featured-post\">
                    <h5><a href=\"#\">Про нас</a></h5>
                </div>
                <div class=\"featured-post\">
                    <h5><a href=\"#\">Контакти</a></h5>
                </div>

            </div>

            <!--Featured Posts-->
            <div class=\"col-md-3 col-sm-3 col-xs-6\">
                <h4>Популярні публікації</h4>";
        // line 52
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["popular"]) ? $context["popular"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["pop"]) {
            // line 53
            echo "                <div class=\"featured-post\">
                    <h5><a href=\"/pub/view/";
            // line 54
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "post_id");
            echo "\">";
            echo $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "post_subj");
            echo " </a></h5>
                    <div class=\"meta group\">
                        <div class=\"right\"><span
                                    class=\"text-muted\">Дата публікації:";
            // line 57
            echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["pop"]) ? $context["pop"] : null), "post_date"), "d.m.Y");
            echo "</span></div>
                    </div>
                </div>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pop'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 61
        echo "            </div>

            <!-- Flickr wiget -->
            <div class=\"col-md-3 col-sm-3 col-xs-6\">
                <div class=\"flickr-wiget\">
                    <h4>ФОТО</h4>


                </div>
            </div>

            <!--Contact Info-->
            <div class=\"col-md-3 col-sm-3 col-xs-6\">
                <div class=\"contacts\">
                    <h4>Контакти</h4>
                    <h5>Адреса:</h5>
                    <p>105448 Casa Blanca BLVD New York</p>
                    <h5>Телефони:</h5>
                    <p>+9 876 543 00 21<br/>+0 123 400 56 78</p>
                    <h5>E-mail:</h5>
                    <p><a href=\"mailto:Yuliya-240@mail.ru\">Yuliya-240@mail.ru</a></p>
                </div>
            </div>
        </div>
    </div>

    <!--Copyright-->
    <div class=\"copyright text-center\">
        <div class=\"container\">
            <p class=\"pull-left\"><a href=\"/\" target=\"_blank\">autoforum</a> - Усі права захищені</p>

            <div class=\"social-bar\">
                <a href=\"http://facebook.com\" target=\"_blank\"><i class=\"fa fa-facebook\"></i></a>
                <a href=\"https://twitter.com\" target=\"_blank\"><i class=\"fa fa-twitter\"></i></a>
                <a href=\"https://www.pinterest.com\" target=\"_blank\"><i class=\"fa fa-pinterest\"></i></a>

                <a href=\"https://plus.google.com/collections/featured?hl=de\" target=\"_blank\"><i
                            class=\"fa fa-google-plus\"></i></a>
            </div>
        </div>
    </div>
</footer><!--Footer Close

";
    }

    public function getTemplateName()
    {
        return "desktop/common/footer.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 61,  84 => 57,  76 => 54,  73 => 53,  69 => 52,  94 => 165,  87 => 158,  77 => 151,  68 => 36,  61 => 31,  47 => 23,  42 => 21,  37 => 19,  64 => 34,  60 => 31,  55 => 28,  40 => 22,  17 => 1,  303 => 239,  298 => 204,  293 => 37,  287 => 242,  285 => 239,  265 => 219,  262 => 218,  254 => 210,  251 => 209,  248 => 206,  246 => 204,  243 => 202,  239 => 200,  236 => 199,  234 => 198,  231 => 197,  131 => 96,  126 => 93,  123 => 91,  118 => 89,  109 => 84,  104 => 80,  99 => 77,  82 => 155,  75 => 54,  70 => 51,  59 => 40,  57 => 29,  20 => 1,  178 => 89,  175 => 88,  166 => 80,  163 => 69,  154 => 65,  150 => 64,  146 => 63,  140 => 62,  132 => 59,  128 => 94,  124 => 56,  112 => 87,  103 => 42,  101 => 78,  92 => 39,  89 => 37,  79 => 32,  74 => 30,  66 => 35,  58 => 30,  54 => 29,  50 => 26,  46 => 23,  28 => 3,  25 => 2,);
    }
}
