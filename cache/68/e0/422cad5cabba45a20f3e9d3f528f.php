<?php

/* desktop/common/nav.tpl */
class __TwigTemplate_68e0422cad5cabba45a20f3e9d3f528f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"header sticky\">
    <!--Adding class \"sticky\" to header enables its pinning to top on scroll. Please note in a \"boxed\" mode header doesn't stick to the top.-->
    <div class=\"inner\">
        <div class=\"container group\">

            <!--Logo-->
            <a class=\"logo\" href=\"index.html\"><img src=\"/img/logo.png\" alt=\"autoforum.eu\"/></a>

            <!--Navigation Toggle-->
            <div class=\"left-off-canvas-toggle\" id=\"nav-toggle\">
                <span></span>
            </div>

            <!--Site Navigation-->
            <div class=\"navigation\">
                <!--Menu-->
                <nav class=\"menu\">
                    <ul>
                        <li class=\"";
        // line 19
        echo (isset($context["topHome"]) ? $context["topHome"] : null);
        echo "\"><a href=\"/\">Головна</a></li>

                        <li class=\"";
        // line 21
        echo (isset($context["topPub"]) ? $context["topPub"] : null);
        echo "\"><a href=\"/pub\">Публікації</a></li>

                        <li class=\"";
        // line 23
        echo (isset($context["topEvents"]) ? $context["topEvents"] : null);
        echo "\"><a href=\"/events\">Афіша</a></li>";
        // line 26
        echo "                    </ul>
                </nav>
                <!--Search-->";
        // line 29
        if ((((isset($context["topHome"]) ? $context["topHome"] : null) || (isset($context["smProfile"]) ? $context["smProfile"] : null)) || (isset($context["smFavorites"]) ? $context["smFavorites"] : null))) {
            echo "<div style=\"display: none\"></div>";
        } else {
            // line 30
            echo "                <div class=\"search\"><i class=\"flaticon-search100\"></i></div>";
        }
        // line 31
        echo "            </div>

            <!--Tools-->
            <div class=\"tools group\">";
        // line 35
        if ($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id")) {
            // line 36
            echo "                <div class=\"user\"><a href=\"/profile\"><i class=\"fa fa-user\"></i>Кабінет</a>

                    <div class=\"user-dropdown\">
                        <a href=\"/profile\">Профіль</a>
                        <a href=\"/logout\">Вихід</a>
                    </div>
                </div>";
        }
        // line 151
        echo "            </div><!--Tools Close-->

        </div>
    </div>";
        // line 155
        if ((((isset($context["topHome"]) ? $context["topHome"] : null) || (isset($context["smProfile"]) ? $context["smProfile"] : null)) || (isset($context["smFavorites"]) ? $context["smFavorites"] : null))) {
            echo " <div style=\"display: none\"></div>
    <!--Quick Search-->";
        } else {
            // line 158
            echo "    <form class=\"quick-search\" autocomplete=\"off\">
        <div class=\"overlay\"></div>
        <input class=\"search-field\" name=\"s\" type=\"text\" placeholder=\"Пошук\">
        <span>Натисніть enter для пошуку</span>
    </form>";
        }
        // line 165
        echo "</header><!--Header Close-->";
    }

    public function getTemplateName()
    {
        return "desktop/common/nav.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 165,  87 => 158,  77 => 151,  68 => 36,  61 => 31,  47 => 23,  42 => 21,  37 => 19,  64 => 34,  60 => 31,  55 => 28,  40 => 22,  17 => 1,  303 => 239,  298 => 204,  293 => 37,  287 => 242,  285 => 239,  265 => 219,  262 => 218,  254 => 210,  251 => 209,  248 => 206,  246 => 204,  243 => 202,  239 => 200,  236 => 199,  234 => 198,  231 => 197,  131 => 96,  126 => 93,  123 => 91,  118 => 89,  109 => 84,  104 => 80,  99 => 77,  82 => 155,  75 => 54,  70 => 51,  59 => 40,  57 => 29,  20 => 1,  178 => 89,  175 => 88,  166 => 80,  163 => 69,  154 => 65,  150 => 64,  146 => 63,  140 => 62,  132 => 59,  128 => 94,  124 => 56,  112 => 87,  103 => 42,  101 => 78,  92 => 39,  89 => 37,  79 => 32,  74 => 30,  66 => 35,  58 => 30,  54 => 29,  50 => 26,  46 => 23,  28 => 3,  25 => 2,);
    }
}
