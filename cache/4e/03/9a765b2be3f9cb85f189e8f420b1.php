<?php

/* desktop/common/headtoolbar.tpl */
class __TwigTemplate_4e039a765b2be3f9cb85f189e8f420b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--Header Toolbar-->
<div class=\"header-toolbar\">
    <div class=\"container\">
        <div class=\"cont-info\">
            <div><i class=\"fa fa-envelope\"></i><a href=\"mailto:Yuliya-240@mail.ru\" target=\"_blank\">Yuliya-240@mail.ru</a>
            </div>
        </div>
        <div class=\"social-bar\">
            <a href=\"http://facebook.com\" target=\"_blank\"><i class=\"fa fa-facebook\"></i></a>
            <a href=\"https://twitter.com\" target=\"_blank\"><i class=\"fa fa-twitter\"></i></a>
            <a href=\"https://www.pinterest.com\" target=\"_blank\"><i class=\"fa fa-pinterest\"></i></a>

            <a href=\"https://plus.google.com/collections/featured?hl=de\" target=\"_blank\"><i
                        class=\"fa fa-google-plus\"></i></a>
        </div>

        <ul class=\"tools\">
        <li>
            <select class=\"form-control\" name=\"lang\" style=\"border-color: #ffffff;\">

                <option class=\"lang\" value=\"ua\">Укр</option>
                <option class=\"lang\"";
        // line 22
        if (((isset($context["lan"]) ? $context["lan"] : null) == "ru")) {
            echo "selected";
        }
        echo " value=\"ru\">Рус</option>
                <option class=\"lang\"";
        // line 23
        if (((isset($context["lan"]) ? $context["lan"] : null) == "de")) {
            echo "selected";
        }
        echo " value=\"de\">Deu</option>

            </select>
        </li>
            <li><a href=\"#\">&nbsp;|&nbsp;</a></li>";
        // line 28
        if ($this->getAttribute((isset($context["acs"]) ? $context["acs"] : null), "user_id")) {
            // line 29
            echo "            <li><a href=\"/logout\" style=\"color: red;\">Вихід</a></li>";
        } else {
            // line 31
            echo "            <li><a href=\"/signup\" style=\"color: red;\">Реєстрація</a></li>
            <li><a href=\"/login\">Логін</a></li>";
        }
        // line 34
        echo "        </ul>
    </div>
</div><!--Header Toolbar Close-->
          
";
    }

    public function getTemplateName()
    {
        return "desktop/common/headtoolbar.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 34,  60 => 31,  55 => 28,  40 => 22,  17 => 1,  303 => 239,  298 => 204,  293 => 37,  287 => 242,  285 => 239,  265 => 219,  262 => 218,  254 => 210,  251 => 209,  248 => 206,  246 => 204,  243 => 202,  239 => 200,  236 => 199,  234 => 198,  231 => 197,  131 => 96,  126 => 93,  123 => 91,  118 => 89,  109 => 84,  104 => 80,  99 => 77,  82 => 60,  75 => 54,  70 => 51,  59 => 40,  57 => 29,  20 => 1,  178 => 89,  175 => 88,  166 => 80,  163 => 69,  154 => 65,  150 => 64,  146 => 63,  140 => 62,  132 => 59,  128 => 94,  124 => 56,  112 => 87,  103 => 42,  101 => 78,  92 => 39,  89 => 37,  79 => 32,  74 => 30,  66 => 27,  58 => 24,  54 => 23,  50 => 21,  46 => 23,  28 => 3,  25 => 2,);
    }
}
