-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Время создания: Янв 06 2017 г., 17:12
-- Версия сервера: 5.5.42
-- Версия PHP: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- База данных: `autoforum`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_nick` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_status` int(11) NOT NULL DEFAULT '1',
  `user_role` enum('user','editor','admin') NOT NULL DEFAULT 'user',
  `user_fb_id` int(11) DEFAULT NULL,
  `user_facebook` varchar(255) DEFAULT NULL,
  `user_birth` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `user_nick`, `user_email`, `user_password`, `user_reg_date`, `user_status`, `user_role`, `user_fb_id`, `user_facebook`, `user_birth`) VALUES
(1, 'u586f90b44ca22', 'a@b.com', '202cb962ac59075b964b07152d234b70', '2017-01-06 12:42:28', 1, 'user', NULL, NULL, '2017-01-06'),
(2, 'u586f9248a4fc2', 'aa@bb.com', '202cb962ac59075b964b07152d234b70', '2017-01-06 12:49:12', 1, 'user', NULL, NULL, '2017-01-06');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;