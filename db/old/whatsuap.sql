-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Время создания: Янв 15 2017 г., 14:50
-- Версия сервера: 5.5.49-log
-- Версия PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `autoforum`
--

-- --------------------------------------------------------

--
-- Структура таблицы `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `country_id` int(11) NOT NULL,
  `country_iso` char(2) CHARACTER SET utf8 NOT NULL,
  `country_name` varchar(80) CHARACTER SET utf8 NOT NULL,
  `country_printable_name` varchar(80) CHARACTER SET utf8 NOT NULL,
  `country_iso3` char(3) CHARACTER SET utf8 DEFAULT NULL,
  `country_numcode` smallint(6) DEFAULT NULL,
  `country_phone_code` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Countries List';

--
-- Дамп данных таблицы `countries`
--

INSERT INTO `countries` (`country_id`, `country_iso`, `country_name`, `country_printable_name`, `country_iso3`, `country_numcode`, `country_phone_code`) VALUES
(25, 'BY', 'BELARUS', 'Беларусь', 'BLR', 112, 0),
(61, 'CZ', 'CZECH REPUBLIC', 'Чеська Республіка', 'CZE', 203, 0),
(77, 'FR', 'FRANCE', 'Франція', 'FRA', 250, 0),
(84, 'DE', 'GERMANY', 'Німеччина', 'DEV', 276, 0),
(180, 'RU', 'RUSSIAN FEDERATION', 'Російська Федерація', 'RUS', 643, 0),
(209, 'CH', 'SWITZERLAND', 'Швейцарія', 'CHE', 756, 0),
(226, 'UA', 'UKRAINE', 'Україна', 'UKR', 804, 38);

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` int(11) NOT NULL,
  `post_user_id` int(11) NOT NULL,
  `post_subj` text NOT NULL,
  `post_text` text NOT NULL,
  `post_status` int(11) NOT NULL DEFAULT '0',
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL,
  `user_nick` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_status` int(11) NOT NULL DEFAULT '1',
  `user_role` enum('user','editor','admin') NOT NULL DEFAULT 'user',
  `user_birth` date DEFAULT NULL,
  `user_fname` varchar(255) DEFAULT NULL,
  `user_lname` varchar(255) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  `user_gender` varchar(1) NOT NULL DEFAULT 'm',
  `user_country` varchar(3) NOT NULL DEFAULT 'DE',
  `user_state` varchar(255) DEFAULT NULL,
  `user_city` varchar(255) DEFAULT NULL,
  `user_street` varchar(255) DEFAULT NULL,
  `user_zip` varchar(50) DEFAULT NULL,
  `user_pic` varchar(255) NOT NULL DEFAULT 'nopic.jpg'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `user_nick`, `user_email`, `user_password`, `user_reg_date`, `user_status`, `user_role`, `user_birth`, `user_fname`, `user_lname`, `user_date`, `user_gender`, `user_country`, `user_state`, `user_city`, `user_street`, `user_zip`, `user_pic`) VALUES
(1, 'booper1', 'a@b.com', '202cb962ac59075b964b07152d234b70', '2017-01-06 10:42:28', 1, 'user', '1976-07-08', 'asda', 'asdas', NULL, 'm', 'DE', 'Саксонія', 'Хренабург', 'Гітлер штрассе 13', '666', '//autoforum/userfiles/1/avatar/5877915f22820.png'),
(4, 'Юлія', 'yuliya-240@mail.ru', '4a7d1ed414474e4033ac29ccb8653d9b', '2017-01-12 15:50:28', 1, 'user', '1990-08-24', 'Юлія', 'Голото', NULL, 'f', 'UA', 'Сумська Область', 'Суми', 'У лісі', '40000', 'nopic.jpg');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=227;
--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
