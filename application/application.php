<?php
namespace autoforum;

use \jet\config as config;
use \jet\language as lang;
use \autoforum\models\logic\jetsession as Session;

class application extends \jet\application {
   
    static public $config;     
    static public $lang;    
    static public $clang;
    static public $vars = null;
    static public $db = null;
    static public $device;
    static public $role;
    static public $proto = 'http://';
    static public $https;
    static public $ip;
    
    protected function __construct($settings = array()) {  
        
     parent::__construct($settings);
        
        self::$config = new config(self::$settings->config_path._.'config.cfg');
        if (!isset( $_COOKIE['lang'])){

            \setcookie("lang","ua",time()+60*60*24*30 ,"/");
            self::$clang="ua";

        }else{

            self::$clang = strtolower($_COOKIE['lang']);
        }


        self::$lang = new lang(self::$settings->lang_path._.self::$clang.'.lng');
        self::$vars = array();
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off'){
        	self::$https = true; 
			self::$proto = 'https://'; 
        }
        
    }           
    
    public function settings() {
        $root_path =  \realpath(__DIR__._.".."._);
        return array(
            'path'          => __DIR__, 
            'root_path'     => $root_path, 
            'cache_path'    => $root_path._.'cache',
            'config_path'   => $root_path._.'config',
            'lang_path'   => $root_path._.'lang',
            'locale'        => 'en_US',
            'session_name'  => 'autoforum'
        );
        
    }
    
    public function beforeController() {
	    
    	$this->session = new Session();
    	
    	self::$device = $this->session->mobDetect();
        
        $this->session->refresh();
        
        // Setup the local settings
        if ($this->session->isLogged() ) { 
	        	$this->session->setTZ();
	    }else{
	        
	        $this->session->setTZUTC();
	        
        }

        self::$ip = self::getUserIP();
        
    }
    
    public function afterController() {
        
    }
    
    static public function Log($msg){
		$logDir = $_SERVER['DOCUMENT_ROOT'].'/logs/' ;

		if (!file_exists($logDir)) {  
    			mkdir( $logDir, 0777);  
		};

	    $logFile = $logDir."system.log";
		$lfh = fopen($logFile, 'a');
		if($lfh){
			$strLog="[ ".date("M d, Y H:i:s")." ] ".$msg."\r\n"; 
			fwrite($lfh, $strLog);
			fclose($lfh);
			
		}

	   
    }
	
	
    static public function WriteProgress($msg){
		$progressDir = $_SERVER['DOCUMENT_ROOT'].'/userfiles/'.$_SESSION['account']['co_id'].'/' ;

		if (!file_exists($progressDir)) {  
    			mkdir( $progressDir, 0777);  
		};

	    $progressFile = $progressDir."progress.log";
		$lfh = fopen($progressFile, 'w');
		if($lfh){
			 
			fwrite($lfh, $msg);
			fclose($lfh);
			
		}

	   
    }

    static public function ReadProgress(){
		$progressDir = $_SERVER['DOCUMENT_ROOT'].'/userfiles/'.$_SESSION['account']['co_id'].'/' ;

	    $progressFile = $progressDir."progress.log";
		$lfh = fopen($progressFile, 'r');
		if($lfh){
			$content = fread($lfh, filesize($progressFile));
			fclose($lfh);
		}
		
		return $content;
    }
    
    
 /*      static function isActive(){
    		if (isset($_SESSION['LAST_ACTIVITY']) ){
			if ((time() - $_SESSION['LAST_ACTIVITY']) > 7200) {
   				 // last request was more than 120 minates ago
   				session_destroy();   // destroy session data in storage
    			session_unset();     // unset $_SESSION variable for the runtime
    			
				return false;
    			
			}else{
		
				$_SESSION['LAST_ACTIVITY'] = time();
				return true;
			}
       }else{
       
       		return false;
       }
    
    }
    

	}*/

	static function clean_num( $num ){
		$pos = strpos($num, '.');
		if($pos === false) { // it is integer number
			return $num;
		}else{ // it is decimal number
			return rtrim(rtrim($num, '0'), '.');
		}
	}
	
	static public function strings_clear($string){
    	$string = \trim($string);
    	$string = \strip_tags(\stripslashes($string));
    	return $string;
    }
    
    static public function dateFormatToDatePicker($dateFormat) { 

    $chars = array( 
        // Day
        'd' => 'dd', 'j' => 'd', 'l' => 'DD', 'D' => 'D',
        // Month 
        'm' => 'mm', 'n' => 'm', 'F' => 'MM', 'M' => 'M', 
        // Year 
        'Y' => 'yyyy', 'y' => 'yy', 
    ); 

    return \strtr((string)$dateFormat, $chars); 
   } 

    static public function dateFormatToMoment($dateFormat) { 

    $chars = array( 
        // Day
        'd' => 'DD', 'j' => 'D', 'l' => 'DD', 'D' => 'D',
        // Month 
        'm' => 'M', 'n' => 'M', 'F' => 'MM', 'M' => 'MMM', 
        // Year 
        'Y' => 'YYYY', 'y' => 'YY', 
    ); 

    return \strtr((string)$dateFormat, $chars); 
   } 

    static public function dateFormatToFullCall($dateFormat) { 

    $chars = array( 
        // Day
        'd' => 'dd', 'j' => 'd', 
        // Month 
        'm' => 'MM', 'n' => 'M', 'F' => 'MMMM', 'M' => 'MMM', 
        // Year 
        'Y' => 'yyyy', 'y' => 'yy', 
    ); 

    return \strtr((string)$dateFormat, $chars); 
   } 


    static public function recursiveGetObjectVars($obj) {
		$arr = array();
		$_arr = is_object($obj) ? get_object_vars($obj) : $obj;
		
		foreach ($_arr as $key => $val) {
			$val = (is_array($val) || is_object($val)) ? self::recursiveGetObjectVars($val) : $val;
			
			// Transform boolean into 1 or 0 to make it safe across all Opauth HTTP transports
			if (is_bool($val)) $val = ($val) ? 1 : 0;
			
			$arr[$key] = $val;
		}
		
		return $arr;
	}

    public static function http404() {
        header("HTTP/1.0 404 Not Found");             
        
        $view = new \jet\twig(); 
        
        $view->setTemplate('404.tpl');
        
        self::getInstance()->view($view);
        
        die();
    }
    
    static public function trace($var,$exit= true){
    	echo('<pre>');
    	print_r($var);
    	echo('</pre>');
    	if($exit)exit;
    }
 
    static public function tracevar($var,$exit= true){
    	
    	$r = print_r($var,TRUE);
    	return $r;
    	
    }
     
    static public function expDays(){
	    
	   $now = \time();
	   $exp = \strtotime($_SESSION['account']['co_date_exp']);
	   $diff = abs($exp - $now);
	   $years = floor($diff / (365*60*60*24));
	   $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
	   $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
	   
	   return $days;
    }
    
    static public function extractquotes  ($str){
	    
	    if (preg_match('/"([^"]+)"/', $str, $m)) {
			return $m[1];   
		} else {
			return false;
		}

    }

    static public function randomPassword() {
	    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = array(); //remember to declare $pass as an array
	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	    for ($i = 0; $i < 8; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); //turn the array into a string
   } 
    
    static public function emailValid($email){
	    
			if (!preg_match("/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/", $email)) return false; return true;	    
	    
    }
    
    static public function passValid($password){
	    
			if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{6,12}$/', $password)) return false; else return true;    
	    
    }
 
	static public function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
	   
	    $output = NULL;
	    if (\filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
	        $ip = $_SERVER["REMOTE_ADDR"];
	        if ($deep_detect) {
	            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
	                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
	                $ip = $_SERVER['HTTP_CLIENT_IP'];
	        }
	    }
	    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
	    $support    = array("country", "stateprov",  "city");
	    
	    if (filter_var($ip, FILTER_VALIDATE_IP) ) {
	        $ipdat = @json_decode(file_get_contents("http://api.db-ip.com/addrinfo?addr=" .$ip. "&api_key=5573a9ff4953426d653b82a2c90f1b541ea27f18"));
	              
	        $output = array(
	            "city"           => @$ipdat->city,
	            "state"          => @$ipdat->stateprov,
				"country"        => @$ipdat->country,
	        );
	                   
	            
	       
	    }
	    return $output;
	}

	static public function getUserIP(){
	    $client  = @$_SERVER['HTTP_CLIENT_IP'];
	    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	    $remote  = $_SERVER['REMOTE_ADDR'];
	
	    if(filter_var($client, FILTER_VALIDATE_IP))
	    {
	        $ip = $client;
	    }
	    elseif(filter_var($forward, FILTER_VALIDATE_IP))
	    {
	        $ip = $forward;
	    }
	    else
	    {
	        $ip = $remote;
	    }
	
	    return $ip;
	}

	static function get_client_ip() {
	    $ipaddress = '';
	    if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}
	
	static public function diffDays($startTS,$endTS){
		
		$timeDiff = abs($endTS - $startTS);
		$numberDays = $timeDiff/86400;
		$numberDays = intval($numberDays);
		
		return $numberDays;
	}
	
	static function convertToHoursMins($time) {
	    \settype($time, 'integer');
	    if ($time < 1) {
	        return;
	    }
	    $hours = floor($time / 60);
	    $minutes = ($time % 60);
	    $res['hour']=$hours;
	    $res['min'] = $minutes;
	    return $res;
    }
    
}
?>