<?php

namespace autoforum\models\common;

use \autoforum\application as app;
use \jet\db\dbc as db;  

class model {
    
    protected $db;
    
    protected $config;
    
    static protected $db_inst = null;
    
    protected $cache;
    
    public function __construct() {   
        $this->config = app::$config;
               
        if (self::$db_inst) {
            
        }
        else {
            if (!file_exists(app::$settings->config_path._.'db.cfg')) {
                throw new \jet\exception("File db.cfg does not exists");
            }
            $db_ini = parse_ini_file(app::$settings->config_path._.'db.cfg', true); 
        
            $connect = array(
                'host'        => $db_ini['db']['db_host'],
                'database'    => $db_ini['db']['db_name'],
                'user'        => $db_ini['db']['db_user'],
                'password'    => $db_ini['db']['db_password'],
            );      
            self::$db_inst = new db($connect, 'mysql', array('charset'=>'utf8'));     
        }                         
        
        $this->db = self::$db_inst;    
    }
    //--------------------------------------------------------------------------
    
    function __cache($id, $obj) {
        $this->cache[$id] = $obj;    
    }
    
    function __cached($id) {
        if (isset($this->cache[$id])) {
            return $this->cache[$id];
        }  
    }
    
    

}