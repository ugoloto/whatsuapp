<?php      
namespace autoforum\models\common;

use \autoforum\application as app;
use \jet\controller\http as HTTPController;   
use \autoforum\models\logic\jetsession as Session;
use \autoforum\models\logic\validator as Validator;
use \jet\language as lang;



class adminController extends HTTPController {

    function __before() {
        parent::__before();
        $this->session = new Session();
       // app::trace($_SESSION);
        $this->session->refresh();

        $this->validator = new Validator();
        //if (!$this->session->isLogged() ) \jet\redirect('/login/',true);
        
        $this->view = new \jet\twig();  
        $this->view->acs = $this->session->getData(); 
        $this->skin=app::$device; 
        $this->view->skin = $this->skin; 
        
        $this->host = app::$config->site['host'];
        
		$this->view->today = \date("d.m.Y");
        $this->view->datetime='d.m.Y'.' '.'H:i:s';
		$this->view->dpformat = app::dateFormatToDatePicker("d.m.Y");
		$this->view->momentformat = app::dateFormatToMoment("d.m.Y");
		$this->view->fcformat = app::dateFormatToFullCall("d.m.Y");

        if(isset($_COOKIE['lang']))
        $this->view->lan = $_COOKIE['lang'];

        if(!isset($_SESSION['iop']))$_SESSION['iop'] = 50;
        $this->session->setTZ("Europe/Kiev");	
		
	
    }
    
    function __default($args = false){
	     //if($_SESSION['account']['user_role']>1 && !$_SESSION['acl']['cfield']['acl_access']) die();
    }

	
   
    function logout(){

	    $this->session->logout();
	   // $this->session->logout();
	    \jet\redirect('/login',true);
    }

    function makeTags($search = null){
    	$indexes = array();
	    // Приводим строку к нижнему регистру, и убираем пробелы по краям
	    $string = trim(mb_strtolower($search));
	    // Заменяем символы на пробелы, т.к. в строке уже встречется символ "-"
	    //$string = preg_replace("/[^A-Za-z0-9\s]/u","",$string);
		  $string = stripslashes ($string);
 	    // Удаляем лишние пробелы, оставляем только один пробел между словами
	    $string = trim(preg_replace("/\s+/u", " ", $string));
	    
	    $tags = explode(" ",$string);
	    
	    foreach($tags as $k=>$tag){
		    
		    if(strlen($tag)<3){
			    $indexes[]=$k;
		    }
	    }
	    
	    if(count($indexes)>0){
		    foreach($indexes as $v){
			    
			    array_splice($tags, $v, 1);
		    }
		    
	    }
	    
	    
	    if(count($tags)>1)$tags[] = $string;
	    $srch['str'] = $string;
	    $srch['tags'] = $tags;
	    return $srch;
     }

	function resize($img, $w, $h, $newfilename) {
			    //Check if GD extension is loaded
			    if (!extension_loaded('gd') && !extension_loaded('gd2')) {
			        trigger_error("GD is not loaded", E_USER_WARNING);
			        return false;
			    }
			     
			    //Get Image size info
			    $imgInfo = getimagesize($img);
			    
			    switch ($imgInfo[2]) {
			    
			        case 1: $im = imagecreatefromgif($img); break;
			        case 2: $im = imagecreatefromjpeg($img);  break;
			        case 3: $im = imagecreatefrompng($img); break;
			        default:  trigger_error('Unsupported filetype!', E_USER_WARNING);  break;
			    }
			     
			    //If image dimension is smaller, do not resize
			    if ($imgInfo[0] <= $w && $imgInfo[1] <= $h) {
			        $nHeight = $imgInfo[1];
			        $nWidth = $imgInfo[0];
			    }
			    else{
			    // yeah, resize it, but keep it proportional
			        if ($w/$imgInfo[0] > $h/$imgInfo[1]) {
			            $nWidth = $imgInfo[0]*($h/$imgInfo[1]);
			            $nHeight = $h;            
			        }
			        else{
			            $nWidth = $w;
			            $nHeight = $imgInfo[1]*($w/$imgInfo[0]);
			        }
			    }
			     
			    $nWidth = round($nWidth);
			     
			    $nHeight = round($nHeight);
			     
			    $newImg = imagecreatetruecolor($nWidth, $nHeight);
			     
			    /* Check if this image is PNG or GIF, then set if Transparent*/  
			    
			    if(($imgInfo[2] == 1) OR ($imgInfo[2]==3)){
			        imagealphablending($newImg, false);
			        imagesavealpha($newImg,true);
			        $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
			        imagefilledrectangle($newImg, 0, 0, $nWidth, $nHeight, $transparent);
			    }
			     
			    imagecopyresampled($newImg, $im, 0, 0, 0, 0, $nWidth, $nHeight, $imgInfo[0], $imgInfo[1]);
			     
			    //Generate the file, and rename it to $newfilename
			    switch ($imgInfo[2]) {
			        case 1: imagegif($newImg,$newfilename); break;
			        case 2: imagejpeg($newImg,$newfilename);  break;
			        case 3: imagepng($newImg,$newfilename); break;
			        default:  trigger_error('Failed resize image!', E_USER_WARNING);  break;
			    }
			     
			    return $newfilename;
	 }	
    
	 

}
