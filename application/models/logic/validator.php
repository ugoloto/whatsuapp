<?php
namespace autoforum\models\logic;

use \autoforum\application as app;

class validator extends \autoforum\models\common\model {

	public function email($iemail=''){       
	    $email = \trim($iemail);                     
      	if (preg_match('/^[a-zA-Z0-9\._-]+@[a-zA-Z0-9\._-]+\.[a-zA-Z]{2,4}$/',$email)&&$email)  return true;
      	return false;
	}
	
	function ip($ip=''){
      	if (preg_match('/^([0-9]{1,3}\.){3}[0-9]{1,3}$/',$ip))  return true;
      	
      	return false;
	}

	function ssn($ssn=''){
	    if (preg_match('/^[0-9]{3}[- ][0-9]{2}[- ][0-9]{4}|[0-9]{9}$/',$ssn))  return true;
	    
      	return false;
	}
	function cc($cc){
	    if (preg_match('/^([0-9]{4}[- ]){3}[0-9]{4}|[0-9]{16}$/',$cc))  return true;
	    
      	return false;
	}
	
	
	/*function password($pass=''){
		
		if(preg_match("/^.*(?=.{5,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$/", $pass)) return true;
 		$errPass = '<p class="errText">Password must be at least 8 characters and must contain at least one lower case letter, one upper case letter and one digit</p>';
 		return false;
		
	}*/
	
	
	//^(?=.{8})(?=.*[A-Z])(?=.*[a-z])(?=.*\d.*\d.*\d)(?=.*[^a-zA-Z\d].*[^a-zA-Z\d].*[^a-zA-Z\d])[-+%#a-zA-Z\d]+$
	
	
	function password($pass=''){

		if (!preg_match_all('$\S*(?=\S{5,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*$', $pass))return false;
		return true;			
	}

}