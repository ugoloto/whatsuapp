<?php
namespace autoforum\models\logic;

use \autoforum\application as app;
use \autoforum\models\orm\users as OrmAccounts;
use \jet\libs\MDETECT as MOB; 
use \jet\db\qb as qb;

class jetsession extends \autoforum\models\common\model {
  
    function login($email, $password, $hashed = false) {
        $Accounts = new OrmAccounts();

       

        $account = $Accounts->access($email, $password, $hashed); 



        if (!$account) {
            throw new \jet\exception('account.wrong');
            return false;   
        }
        
        if ($account['user_status'] != 1 ) {
            throw new \jet\exception('account.blocked');
            return false;   
        }
        
        $_SESSION['account'] = $account;
        
        return $account;
    }
        
    function mobDetect(){
	    MOB::load();
	    $detect = new \Mobile_Detect();
	    if ($detect->isMobile()) return "desktop"; else return "desktop";
	    
    }
    
    function refresh() {
        if (isset($_SESSION['account']) && isset($_SESSION['account']['user_id']))  {
             $Accounts = new OrmAccounts();
             $account = $Accounts->getAccount($_SESSION['account']['user_id']);     
             
             if (!$account) {
                 $this->logout();
                 return false;
             }
             
             
             if ($account['user_status'] != 1) {
                $this->logout();    
                return false;
            } 
             
             $_SESSION['account'] = $account;
             return true;
             
        }
        else {
           //запретить действие если юзер вышел(поиск) $this->logout();
            return false;     
        }
    }

    function checkAccountID($userID = null) {
        if ($userID === null) return false;
        if (isset($_SESSION['account']['user_id']) && $_SESSION['account']['user_id'] == $userID) return true;
        return false;     
    }

    function getAccountID() {
        if (isset($_SESSION['account']['user_id'])) return $_SESSION['account']['user_id'];
        return false;     
    }

    function logout() {
       
       foreach ($_SESSION as $k=>$v){
	       
	       unset($_SESSION[$k]);
       }
         
       $_SESSION['account'] = null; 
        $this->disableAutoLogin() ;
    }

    function isLogged() {
        if (isset($_SESSION['account']) && isset($_SESSION['account']['user_id'])) return true; else return false;
    }

    function getData() {
        if (isset($_SESSION['account']))
            return $_SESSION['account'];
        else return null;
    }

    /************************************************
    
    	Autologin ( Remeber me)
    	
    *************************************************/
    
    function enableAutoLogin($email = null, $password = null) {
	
			\setcookie('zim_email',$email,time()+60*60*24*30,'/');
			\setcookie('zim_password', $password,time()+60*60*24*30,'/');
		
	}
	
	function tryAutoLogin() {
		if (!isset($_COOKIE['zim_password'])) {
			//throw new \jet\exception('no.autologin');
			return false;
		}
		else return true; //return $this->login($_COOKIE['ii_email'],$_COOKIE['ii_password'],true);
	}
	
	function disableAutoLogin() {
		setcookie('zim_email',null,time(),'/');
		setcookie('zim_password',null,time(),'/');
	}

	/**** 
	
	CLIENT SESSION
	
	**/
	
	function setTZ(){
		
		//app::trace($_SESSION['account']['co_tz']);
		
		 \date_default_timezone_set("Europe/Kiev");  
		 $time_offset = date('P');
		 if($time_offset=="+14:00")$time_offset="+13:00";
		 $this->db->q("SET `time_zone`='".$time_offset."'");
		 
	}

	function setTZUTC(){
		
		 \date_default_timezone_set('UTC');  
		  $this->db->q("SET `time_zone`='".date('P')."'");
		 
	}
    
	
}
