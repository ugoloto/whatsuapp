<?php
namespace autoforum\models\libs;

use \autoforum\application as app;
use \autoforum\models\logic\session as Session;
use \jet\libs\stripe as STRIPELIB; 
use \jet\libs\crypt as Crypt;
use \Defuse\Crypto\Crypto;
use \Defuse\Crypto\Exception as Ex;


class jetcrypt extends \autoforum\models\common\model {

	function __construct() {
		parent::__construct();	
		Crypt::load();
	}

	
	function doCrypt($message){

		$key = app::$config->crypto['key'];
		
		try {
		    $ciphertext = Crypto::encrypt($message, $key);
		} catch (Ex\CryptoTestFailedException $ex) {
		    die('Cannot safely perform encryption');
		} catch (Ex\CannotPerformOperationException $ex) {
		    die('Cannot safely perform encryption');
		}
		$ct = base64_encode($ciphertext);
		return $ct;
		
	} 

	function doDeCrypt($ciphertext){
		
		$key = app::$config->crypto['key'];
		$str = base64_decode($ciphertext);
		
		try {
		    $decrypted = Crypto::decrypt($str, $key);
		} catch (Ex\InvalidCiphertextException $ex) { // VERY IMPORTANT
		    // Either:
		    //   1. The ciphertext was modified by the attacker,
		    //   2. The key is wrong, or
		    //   3. $ciphertext is not a valid ciphertext or was corrupted.
		    // Assume the worst.
		    die('DANGER! DANGER! The ciphertext has been tampered with!');
		} catch (Ex\CryptoTestFailedException $ex) {
		    die('Cannot safely perform decryption');
		} catch (Ex\CannotPerformOperationException $ex) {
		    die('Cannot safely perform decryption');
		}
		
		return $decrypted;
		
	} 
	
	
	function getCryptKey(){
		try {
		    $k = Crypto::createNewRandomKey();
		    // WARNING: Do NOT encode $key with bin2hex() or base64_encode(),
		    // they may leak the key to the attacker through side channels.
		} catch (Ex\CryptoTestFailedException $ex) {
		    die('Cannot safely create a key');
		} catch (Ex\CannotPerformOperationException $ex) {
		    die('Cannot safely create a key');
		}

		$key = \base64_encode($k);		
		return $key;		
		
	}
	
	
}
