<?php
namespace autoforum\models\orm;

use \autoforum\application as app;
use \jet\db\qb as qb;

class posts extends \autoforum\models\common\model {
	
    function add($arr) {
        $this->db->q(qb::_table('posts')->insert($arr));
        return $this->db->getLastID();
       
    }

	function update($id=0,$data=array()){
		$w['post_id']=$id;

		$this->db->q(qb::_table('posts')->where($w)->update($data));
        return true;
	}

    function delete($id=0){
        
	    $cArr['post_del']=1;
	    $this->update($id,$cArr);    
    }


    function getList($where=array(),$page=1,$count=1, $search = array(),$order='post_date DESC') {

        if($page>0)$page--;
        $offset = \intval($page*$count);
        $where['post_del']=0;

        $select = '*';
        $collection = qb::_table('posts');
        $collection->leftjoin('users', 'users.user_id', 'post_user_id');
        $collection->leftjoin('category', 'category.cat_id', 'post_cat');
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }
    
    function getListCount($where=array(),$search = array()) {
        $select = '*';
         $where['post_del']=0;
        $collection = qb::_table('posts');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));    
    }

    function getByID($id=0) {
    	$where['post_id'] = $id;
		$where['post_del']=0;
        $select = '*';
        $collection = qb::_table('posts');
        $collection->leftjoin('users', 'users.user_id', 'post_user_id');
        $collection->leftjoin('category', 'category.cat_id', 'post_cat');
        return $this->db->q_($collection->where($where)->select($select));
    }

    
	
}
