<?php
namespace autoforum\models\orm;

use \autoforum\application as app;
use \jet\db\qb as qb;

class poster extends \autoforum\models\common\model {
	
    function add($arr) {
        $this->db->q(qb::_table('poster')->insert($arr));
        return $this->db->getLastID();
       
    }

	function update($id=0,$data=array()){
		$w['poster_id']=$id;

		$this->db->q(qb::_table('poster')->where($w)->update($data));
        return true;
	}

    function delete($id=0){
        
	    $cArr['poster_del']=1;
	    $this->update($id,$cArr);    
    }


    function getList($where=array(),$page=1,$count=1, $search = array(),$order='poster_date DESC') {
        
        if($page>0)$page--;
        $offset = (int)($page * $count);
        $where['poster_del']=0;
	
        $select = '*';
        $collection = qb::_table('poster');
        $collection->leftjoin('users', 'users.user_id', 'poster_user_id');
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }

    function getListCount($where=array(),$search = array()) {
        $select = '*';
         $where['poster_del']=0;
        $collection = qb::_table('poster');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));    
    }
	
	
    function getByID($id=0) {
    	$where['poster_id'] = $id;
		$where['poster_del']=0;
        $select = '*';
        $collection = qb::_table('poster');
        $collection->leftjoin('users', 'users.user_id', 'poster_user_id');
        return $this->db->q_($collection->where($where)->select($select));    
    }
	
	
}