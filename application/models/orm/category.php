<?php
namespace autoforum\models\orm;

use \autoforum\application as app;
use \jet\db\qb as qb;

class category extends \autoforum\models\common\model {

    function getList($where=array(), $search = array()) {


        $select = '*';
        $collection = qb::_table('category');
        return $this->db->q($collection->where($where)->Search($search)->select($select));
    }
}
?>