<?php
/**
 * Created by PhpStorm.
 * User: Julia
 * Date: 13.04.2017
 * Time: 15:26
 */

namespace autoforum\models\orm;

use \autoforum\application as app;
use \jet\db\qb as qb;


class postercom extends \autoforum\models\common\model {

    function add($arr) {
        $this->db->q(qb::_table('postercom')->insert($arr));
        return $this->db->getLastID();

    }

    function delete($id){
        $where['pc_id'] = $id;
        $collection = qb::_table('postercom');
        $this->db->q($collection->where($where)->delete());
        return true;
    }


    function getList($where=array(), $search = array(),$order='pc_id DESC') {

        $select = '*';
        $collection = qb::_table('postercom');
        $collection->leftjoin('users', 'users.user_id', 'pc_user_id');
        $collection->leftjoin('poster', 'poster.poster_id', 'pc_poster_id');
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->select($select));
    }



}
