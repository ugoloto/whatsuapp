<?php
namespace autoforum\models\orm;

use \autoforum\application as app;
use \jet\db\qb as qb;

class users extends \autoforum\models\common\model {
	
    function access($email, $password, $passwordHashed = false, $collection = array()) {

        $where = array(
            'user_email'     => $email,
            'user_password'  => ($passwordHashed)?$password:$this->hashPassword($password)
        );


        
        $account = $this->returnAccount($where);
        
        return $account;
    }
    
    function getAccount($userID, $collection = array()) {
        $where = array(
            'user_id' => $userID
        );
        return $this->returnAccount($where, $collection);    
    }    
    
    function createAccount($email, $password) {
 
	
        if ($this->findByEmail($email)) { 
            return false;
        }
        
 
        // Create user
        $user = array(
            'user_email'        => $email,
            'user_nick' => "u".uniqid(),
            'user_password'     => $this->hashPassword($password),
            'user_birth'		=> \date('Y-m-d'),
        );
        
        $this->db->q(qb::_table('users')->insert($user));

        
        return true;
    }

   function hashPassword($password) {
        return md5($password);
    }

    function findByEmail($email) {
        $where = array(
            'user_email'     => $email
        );
        return $this->returnAccount($where);
    }
	
	function update($userID=0,$usersArr=array()){
		$w['user_id']=$userID;
		$this->db->q(qb::_table('users')->where($w)->update($usersArr));
        return true;
	}
	
	
	function updateusr($usersArr=array()){
		$w['user_id']=$_SESSION['account']['user_id'];
		$this->db->q(qb::_table('users')->where($w)->update($usersArr));
        return true;
	}
	

    function getList($where=array(),$page=1,$count=1, $search = array(),$order='user_id') {
        $where['user_del']=0; 
        $where['user_company_id']=$_SESSION['account']['co_id'];
        if($page>0)$page--;
        $offset = \intval($page*$count);

        $select = '*,if (NOW() > company.co_date_exp, 1, 0) as expired, company.co_status as accountStatus';
        $collection = qb::_table('users');
        $collection->leftjoin('company', 'company.co_id', 'user_company_id');
        
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }

    function getListCount($where=array(),$search = array()) {
        $where['user_del']=0; 
        $where['user_company_id']=$_SESSION['account']['co_id'];
        $select = '*';
        $collection = qb::_table('users');
        $collection->leftjoin('company', 'company.co_id', 'user_company_id');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));    
    }

    function getListSearch($where=array(),$page=1,$count=1, $search = array(),$order="user_fname ASC") {
    	$where['user_del']=0; 
    	$where['user_company_id']=$_SESSION['account']['co_id'];
        if($page>0)$page--;
        $offset = \intval($page*$count);
		
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(user_tags LIKE '".$tag."')";
			$searchnew['user_tags'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $select = '*,'.$ss;
        $collection = qb::_table('users');
       // $collection->leftjoin('countries', 'countries.country_iso', 'customs.custom_country');
        
        return $this->db->q($collection->where($where)->Search($searchnew)->OrderBy($order)->GroupBy('user_id')->Limit($offset,$count)->select($select));    
    }

    function getListCountSearch($where=array(),$search = array()) {
    	$where['user_del']=0; 
    	$where['user_company_id']=$_SESSION['account']['co_id'];
		$add="";
		foreach($search as $k=>$tag){
			if($k>0)$add.=" + ";
			$add.= "(user_tags LIKE '".$tag."')";
			$searchnew['user_tags'][]=$tag;
		}
		$ss = "(".$add.") as hits";
		
        $collection = qb::_table('users');
        //$collection->leftjoin('customer_types', 'customer_types.ct_id', 'customs.custom_type');
        //$collection->leftjoin('countries', 'countries.country_iso', 'customs.custom_country');
        $count =  $this->db->q($collection->where($where)->Search($searchnew)->GroupBy('user_id')->count('*'));  
        return count($count);  
    }

    function getInfo($where=array()) {
    	$where['user_company_id']=$_SESSION['account']['co_id'];
        $select = '*';
        $collection = qb::_table('users');
        
        $collection->leftjoin('countries', 'countries.country_iso', 'users.user_country');
        $collection->leftjoin('states', 'states.state_code', 'users.user_state','AND states.state_country_iso = users.user_country');
        return $this->db->q_($collection->where($where)->select($select));    
    }

    function getByID($id=0) {
    	$where['user_id'] = $id;
    	//$where['user_company_id']=$_SESSION['account']['co_id'];
        $select = '*';
        $collection = qb::_table('users');
		$collection->leftjoin('countries', 'countries.country_iso', 'users.user_country');
		$collection->leftjoin('states', 'states.state_code', 'users.user_state','AND states.state_country_iso = users.user_country');
        return $this->db->q_($collection->where($where)->select($select));    
    }
    
    function delete($userID=0){
	    
	    $usersArr['user_del']=1;
	    $this->update($userID,$usersArr); 

    	$collection = qb::_table('crewlist');
	    $where['crewlist_user_id'] = $userID;
	    $where['crewlist_co_id'] = $_SESSION['account']['co_id'];
	    $this->db->q($collection->where($where)->delete()); 
	       
	    return true;
    }
 
    function getAllCount($w=array()){
        $select = '*';
        $collection = qb::_table('users');
        $w['user_del']=0;
        $w['user_company_id']=$_SESSION['account']['co_id'];
        return $this->db->q1($collection->where($w)->count($select));    
    }    
    
    function getAllsList($where=array(),$order='user_fname ASC'){
        $where['user_del']=0; 
        $where['user_company_id']=$_SESSION['account']['co_id'];
        $select = '*';
        $collection = qb::_table('users');
        return $this->db->q($collection->where($where)->OrderBy($order)->select($select));    
	    
    }
 
     function getAllList($where=array(),$search = array(),$order='user_fname ASC'){
        $where['user_company_id'] = $_SESSION['account']['co_id'];
        $where['user_del']=0; 
        $select = '*';
        $collection = qb::_table('users');
 
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->select($select));    
    }
 
     private function returnAccount($where=array(), $components = array()) {
         //app::trace($where);

        $select = '*, users.user_id as userID';
        
        $collection = qb::_table('users');

        

        $account = $this->db->q_($collection->where($where)->select($select));    

        return $account;
    }   
    
 }
?>