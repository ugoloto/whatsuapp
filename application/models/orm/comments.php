<?php
/**
 * Created by PhpStorm.
 * User: Julia
 * Date: 10.04.2017
 * Time: 14:11
 */

namespace autoforum\models\orm;

use \autoforum\application as app;
use \jet\db\qb as qb;

class comments extends \autoforum\models\common\model {

    function add($arr) {
        $this->db->q(qb::_table('comments')->insert($arr));
        return $this->db->getLastID();

    }

    function delete($id){
        $where['comments_id'] = $id;
        $collection = qb::_table('comments');
        $this->db->q($collection->where($where)->delete());
        return true;
    }


    function getList($where=array(), $search = array(),$order='comments_id DESC') {
        
        $select = '*';
        $collection = qb::_table('comments');
        $collection->leftjoin('users', 'users.user_id', 'comments_user_id');
        $collection->leftjoin('posts', 'posts.post_id', 'comments_post_id');
        return $this->db->q($collection->where($where)->Search($search)->OrderBy($order)->select($select));
    }

    

}