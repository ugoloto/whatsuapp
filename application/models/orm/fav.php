<?php
namespace autoforum\models\orm;

use \autoforum\application as app;
use \jet\db\qb as qb;

class fav extends \autoforum\models\common\model {
	
    function add($arr) {
        $this->db->q(qb::_table('fav_posts')->insert($arr));
        return $this->db->getLastID();
       
    }
	
	 function delete($id){  
	    	$where['fp_id'] = $id;
			$collection = qb::_table('fav_posts');
			$this->db->q($collection->where($where)->delete()); 
			return true;
    }
	
	
	function getList($where=array(),$page=1,$count=1, $search = array(),$order='fp_date DESC') {
        
        if($page>0)$page--;
	
		$z=$page*$count;
        $offset = intval($z);
        
        $select = '*';
        $collection = qb::_table('fav_posts');
        $collection->leftjoin('users', 'users.user_id', 'fp_user_id');
		$collection->leftjoin('posts', 'posts.post_id', 'fp_post_id');
        return $this->db->q($collection->Search($search)->OrderBy($order)->Limit($offset,$count)->select($select));    
    }

	function getByID($id=0) {
    	$where['fp_id'] = $id;
		$select = '*';
		$collection = qb::_table('fav_posts');
        return $this->db->q_($collection->where($where)->select($select));    
    }
	
	
	  function getListCount($where=array(),$search = array()) {
        $select = '*';    
        $collection = qb::_table('fav_posts');
        return $this->db->q1($collection->where($where)->Search($search)->count('*'));    
    }
	
	
}