<?php
namespace autoforum\models\orm;

use \autoforum\application as app;
use \jet\db\qb as qb;

class country extends \autoforum\models\common\model {
	

	function getTZList(){
		$collection = qb::_table('timezones');
		return $this->db->q($collection->select('*'));	
	}

	function addTZList($arr){
        $this->db->q(qb::_table('timezones')->insert($arr));
        
        $id = $this->db->getLastID();
        
        return $id;
	}

	function getTZ($where=array()){
		
		$collection = qb::_table('timezones');
		return $this->db->q_($collection->where($where)->select("*"));	
	}
	
	function getCurrencyList(){
		$collection = qb::_table('currencies');
		return $this->db->q($collection->select('*'));	
	}


	
	function getCountriesList(){
		$collection = qb::_table('countries');
		return $this->db->q($collection->select('*'));
	}

	
	
	
}

?>