<?php      
namespace autoforum\controllers\events;

use \autoforum\application as app;
use \autoforum\models\common\adminController as adminController;
use \autoforum\models\orm\users as OrmUsers;
use \autoforum\models\orm\postercom as OrmComments;
use \autoforum\models\orm\poster as OrmPosters;
use \autoforum\models\orm\posts as OrmPosts;
use \autoforum\models\libs\pagination as PAGLIB;
 

class main extends adminController {
    
    function __before() {

    	parent::__before();
        $this->view->setPath(app::$device.'/events'); 
		$this->poster = new  OrmPosters();
		$this->postercom = new  OrmComments();
		$this->posts = new  OrmPosts();
		$this->view->topEvents = "active";
		if(!isset($_SESSION['POSTER_STATUS']))$_SESSION['POSTER_STATUS']=0;
		$this->view->page_blog = "blog";
	}   
     
    function __default($args = false) { 


		$pages = new PAGLIB();
    	$where=array();
    	$search=array();
    	
		$this->view->popul = $this->getPopularP();
		$this->view->popular = $this->getPopular();

		if(isset($_SESSION['EVENTS_SEARCH']))$search['poster_text']=$_SESSION['EVENTS_SEARCH'];
		unset($_SESSION['EVENTS_SEARCH']);
		$totalRec = $this->poster->getListCount($where,$search);
		
    	$page = isset($_GET['p']) && $_GET['p']  ? \abs((int)$_GET['p']) :1;
    	
	    $itemsOnPage = isset($_GET['iop']) && $_GET['iop']  ? \abs((int)$_GET['iop']) : 5;
    	
	    //$pages = new \pagination();
	    $pagination = $pages->calculate_pages($totalRec,$itemsOnPage, $page);
		

		
		$records = $this->poster->getList($where,$pagination['current'],$itemsOnPage,$search);
		
	    $this->view->curl = $_SERVER['REQUEST_URI'];
	    $this->view->iop = $itemsOnPage;
	    $this->view->pagination = $pagination;
	    $this->view->totalRec = $totalRec;
		
		$this->view->records = $records;

        $this->view->setTemplate('index.tpl');  
        return $this->view;
    } 
	
    function view(){
		
		$id = $this->args[0];	 
	    $rec = $this->poster->getByID($id);

		$wherec['pc_poster_id']=$id;
		$this->view->postercom =  $this->postercom->getList($wherec, $search = array(),$order='pc_id DESC');
		
		$this->view->popul = $this->getPopularP();
		$this->view->popular = $this->getPopular();
		
		$upd=array();
		$upd['poster_view'] = $rec['poster_view']=$rec['poster_view']+1;
		$this->poster->update($id,$upd);
		
		$this->view->rec = $rec;
        $this->view->setTemplate('view.tpl');  
        return $this->view;
	    
	    
    }
	
	function getPopularP(){
		return $this->poster->getList(array(),1,4,array(),'poster_view DESC');
		
	}

	function getPopular(){
		return $this->posts->getList(array(),1,2,array(),'post_view DESC');

	}

	function addCom(){
		$post =app::strings_clear( $_POST['poster_id']);
		$text= app::strings_clear($_POST['text']);
		$add=array(

			"pc_user_id" => $_SESSION['account']['user_id'],
			"pc_poster_id" =>$post ,
			"pc_text" => $text,
		);

		$this->postercom->add($add);


		$res['id']=$_POST['poster_id'];
		$res['status'] = true;
		return $res;

	}
	function del(){

		$id = $_POST['id'];

		$this->postercom->delete($id);

		$res['status'] = true;
		return $res;
	}

	function changeSearch(){

		$_SESSION['EVENTS_SEARCH'] = trim($_POST['s']);

		$res['status'] = true;
		return $res;
	}

    function __format() {
        return array(
         
        );
    } 
    
}
