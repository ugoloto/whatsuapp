<?php      
namespace autoforum\controllers\login;

use \autoforum\application as app;
use \autoforum\models\common\adminController as adminController;
use \autoforum\models\orm\users as OrmUsers;
use \autoforum\models\orm\posts as OrmPosts;

class main extends adminController {
    
    function __before() {

    	parent::__before();
    	if ($this->session->isLogged() ) \jet\redirect('/',true);
    	
    	//app::trace($this->session->isLogged());
        $this->posts = new  OrmPosts();

        $this->view->setPath(app::$device.'/login');   
		
	}   
     
    function __default($args = false) {
        $this->view->popular = $this->getPopular();
        $this->view->setTemplate('index.tpl');  
        return $this->view;
    } 
 
 
    function doLogin($email = null,$pass = null,$hash = false){
	   
	   if($_SERVER['REQUEST_METHOD'] != 'POST'){
		   
		   return false;
	   }
	   
	    if(!$email && $_SERVER['REQUEST_METHOD'] == 'POST'){
	    	
	    	$email=$_POST['username'];
	    	$pass=$_POST['pass'];
	    	$hash = false;
	    }
	    
	    try{
	    	$account=$this->session->login($email,$pass,$hash);
	    
	    }catch (\Exception $e) {
			$e->getMessage();
           $res['status']=false;
           $res['msg']="Помилка";
		   $res['title']=$e->getMessage();;
		   return $res;
        }
        
 	    
         $res['status']=true;
         return $res;   

    }
    
    function getPopular(){
        return $this->posts->getList(array(),1,2,array(),'post_view DESC');

    }
    
    function logout(){

	    $this->session->logout();
	    \jet\redirect('/login',true);
    }
    
    
    
}
