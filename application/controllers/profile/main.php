<?php      
namespace autoforum\controllers\profile;

use \autoforum\application as app;
use \autoforum\models\common\adminController as adminController;
use \autoforum\models\orm\users as OrmUsers;
use \autoforum\models\orm\country as OrmCountry;
use \autoforum\models\orm\posts as OrmPosts;

	
	
class main extends adminController {
    
    private $monthsarr = array(
	    
	    array("m"=>1,"name"=>"Січень"),
	    array("m"=>2,"name"=>"Лютий"),
	    array("m"=>3,"name"=>"Березень"),
	    array("m"=>4,"name"=>"Квітень"),
	    array("m"=>5,"name"=>"Травень"),
	    array("m"=>6,"name"=>"Червень"),
	    array("m"=>7,"name"=>"Липень"),
	    array("m"=>8,"name"=>"Серпень"),
	    array("m"=>9,"name"=>"Вересень"),
	    array("m"=>10,"name"=>"Жовтень"),
	    array("m"=>11,"name"=>"Листопад"),
	    array("m"=>12,"name"=>"Грудень"),
	    
    );
    
    function __before() {

    	parent::__before();
        $this->view->setPath(app::$device.'/profile');
		$this->posts = new  OrmPosts();
		$this->users = new OrmUsers();
		$this->country = new OrmCountry();
		$this->view->smProfile = "active";
	}   
     
    function __default($args = false) {   
		
		//app::trace($_SESSION['account']);

		$this->view->popular = $this->getPopular();
		$this->view->clist = $this->country->getCountriesList(); 
		$this->view->months = $this->monthsarr;
        $this->view->setTemplate('index.tpl');  
        return $this->view;
    }

	function getPopular(){
		return $this->posts->getList(array(),1,2,array(),'post_view DESC');

	}

	function doUpdate(){
		
		$email = app::strings_clear($_POST['user_email']);
		$nick = app::strings_clear($_POST['user_nick']);
		$fname = app::strings_clear($_POST['user_fname']);
		$lname = app::strings_clear($_POST['user_lname']);
		$country = ($_POST['country']); 
		$day = isset($_POST['db_day']) ? (int)$_POST['db_day'] : 0;
		$month = isset($_POST['db_month']) ? (int)$_POST['db_month'] : 0;
		$year = isset($_POST['db_year']) ? (int)$_POST['db_year'] : 0;
		$date = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
		$gender = ($_POST['user_gender']);
		
		$img = app::strings_clear($_POST['img']);
		//app::trace($img);
		$state = app::strings_clear($_POST['user_state']);
		$city = app::strings_clear($_POST['user_city']);
		$street = app::strings_clear($_POST['user_street']);
		$zip = app::strings_clear($_POST['user_zip']);
		
		$id = $_POST['user_id'];
		$upd = array(
			'user_email' => $email,
			'user_nick' => $nick,
			'user_country' => $country,
			'user_birth' => $date,
			'user_gender' => $gender
		);
		
		if(strlen($img))$upd['user_pic'] = $img;
		
		if(strlen($fname))$upd['user_fname'] = $fname;
		if(strlen($lname))$upd['user_lname'] = $lname;
		if(strlen($state))$upd['user_state'] = $state;
		if(strlen($city))$upd['user_city'] = $city;
		if(strlen($street))$upd['user_street'] = $street;
		if(strlen($zip))$upd['user_zip'] = $zip;
		
		
		$upd['user_gender'] = $gender;
		
		$this->users->update($id,$upd);
		
		$res['status'] = true;
		return $res;
		
	}
	
        
    function logout(){

	    $this->session->logout();
	    \jet\redirect('/login',true);
    }
    
    
    
}
