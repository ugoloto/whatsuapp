<?php      
namespace autoforum\controllers\posts;

use \autoforum\application as app;
use \autoforum\models\common\adminController as adminController;
use \autoforum\models\orm\users as OrmUsers;
use \autoforum\models\orm\posts as OrmPosts;
use \autoforum\models\orm\category as OrmCat;
use \autoforum\models\libs\pagination as PAGLIB;

class main extends adminController {

    function __before() {

    	parent::__before();
        $this->view->setPath(app::$device.'/posts');   
		$this->users = new OrmUsers();
		$this->posts = new  OrmPosts();
		$this->category = new OrmCat();
		$this->view->smPosts = "active";
		if(!isset($_SESSION['POST_STATUS']))$_SESSION['POST_STATUS']=0;
	}   

    function __default($args = false) {   
		
    	$pages = new PAGLIB();
    	$where=array();
    	$search=array();
    	//if($_SESSION['user_role']='admin')$this -> posts->getAllList($where=array());
		$this->view->popular = $this->getPopular();


		if($_SESSION['account']['user_role']=="user"){
			$where['post_user_id'] = $_SESSION['account']['user_id'];
		}
		
		if(isset($_SESSION['POST_SEARCH']))$search['post_subj']=$_SESSION['POST_SEARCH'];
		unset($_SESSION['POST_SEARCH']);
		$totalRec = $this->posts->getListCount($where,$search);
		
    	$page = isset($_GET['p']) && $_GET['p']  ? \abs((int)$_GET['p']) :1;
    	
	    $itemsOnPage = isset($_GET['iop']) && $_GET['iop']  ? \abs((int)$_GET['iop']) : 5;
    	
	    //$pages = new \pagination();
	    $pagination = $pages->calculate_pages($totalRec,$itemsOnPage, $page);
		

		
		$records = $this->posts->getList($where,$pagination['current'],$itemsOnPage,$search);
		
	    $this->view->curl = $_SERVER['REQUEST_URI'];
	    $this->view->iop = $itemsOnPage;
	    $this->view->pagination = $pagination;
	    $this->view->totalRec = $totalRec;
		
		$this->view->records = $records;
   
        $this->view->setTemplate('index.tpl');  
        return $this->view;
    } 
    
  
    
    function add(){

		$this->view->popular = $this->getPopular();
		$this->view->clist = $this->category->getList();
		$this->view->setTemplate('add.tpl');
        return $this->view;
	    
	    
    }
    
    function edit(){
	    
	    $id = $this->args[0];
		$this->view->popular = $this->getPopular();
		$this->view->clist = $this->category->getList();
	    $this->view->post = $this->posts->getByID($id);
        $this->view->setTemplate('edit.tpl');  
        return $this->view;
	    
	    
	    
	    
    }
    
    function doAddPost(){
		
	   
	    $subj = app::strings_clear($_POST['subj']);
	    $post = app::strings_clear($_POST['post']);
	    
	    $add=array(
		    
		    "post_user_id" => $_SESSION['account']['user_id'],
		    "post_subj" => $subj,
		    "post_text" => $post,
			"post_pic" => $_POST['fname'],
			"post_cat" => $_POST['category'],
	    );
	    
	    $this->posts->add($add);
	   
	    
	    $res['status'] = true;
	    return $res;
    }
	
	 function doUpdatePost(){
	    
	    $subj = app::strings_clear($_POST['subj']);
	    $post = app::strings_clear($_POST['post']);
	    $img = app::strings_clear($_POST['fname']);
	    $edit=array(
		    
		  	"post_status" =>$_POST['status'],
		    "post_subj" => $subj,
		    "post_text" => $post,
			"post_cat" => $_POST['category'],
			
	    );
	    
		if(strlen($img))$edit['post_pic'] = $img;
	    $this->posts->update($_POST['post_id'],$edit);
	    
	    
	    $res['status'] = true;
	    return $res;
    }
    
    
    function del(){
	    
	    $id = $_POST['id'];
	    
	    $this->posts->delete($id);
	    
	    $res['status'] = true;
	    return $res;
    }

	function changeSearch(){

		$_SESSION['POST_SEARCH'] = trim($_POST['s']);

		$res['status'] = true;
		return $res;
	}

	function getPopular(){
		return $this->posts->getList(array(),1,2,array(),'post_view DESC');

	}
}