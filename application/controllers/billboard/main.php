<?php      
namespace autoforum\controllers\billboard;

use \autoforum\application as app;
use \autoforum\models\common\adminController as adminController;
 

class main extends adminController {
    
    function __before() {

    	parent::__before();
        $this->view->setPath(app::$device.'/billboard');   
		$this->view->topBB = "active";
	}   
     
    function __default($args = false) {   

        $this->view->setTemplate('index.tpl');  
        return $this->view;
    } 
    
    function __format() {
        return array(
         
        );
    } 
    
}
