<?
namespace autoforum\controllers\img;

use \autoforum\application as app;
use \autoforum\models\common\adminController as adminController;
use \autoforum\models\logic\jetsession as Session;
use \autoforum\models\orm\users as OrmUsers;

class main extends adminController {

    function __before() {

        $this->session = new Session();

        $this->session->refresh();
        if (!$this->session->isLogged() ) {
            \jet\redirect('/login/',true);
        }


        if (!file_exists($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles")) {
            @mkdir($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles", 0777, true);
        }

        if (!file_exists($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'])) {
            @mkdir($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'], 0777, true);
        }
    }

    function uploaduserpic(){
        //  	$lng_profile = app::$lang->profile;
        //  	$lng_common = app::$lang->common;


        $this->users = new OrmAccounts();

        if (!file_exists($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic")) {
            @mkdir($targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic", 0777, true);
        }


        $targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic" ;
        //app::trace($targetDir);

        $cleanupTargetDir = true; // Remove old files
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        // 5 minutes execution time
        @set_time_limit(5 * 60);

        // Uncomment this one to fake upload time
        // usleep(5000);

        // Get parameters
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

        // Clean the fileName for security reasons
        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        // Make sure the fileName is unique but only if chunking is disabled
        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

        // Create target dir
        if (!file_exists($targetDir))@mkdir($targetDir);

        // Remove old temp files
        if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                // Remove temp file if it is older than the max age and is not the current file
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                    @unlink($tmpfilePath);
                }
            }

            closedir($dir);
        } else
            die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');


        // Look for the content type header
        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];

        // Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
                // Open temp file
                $out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {
                    // Read binary input stream and append it to temp file
                    $in = fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    fclose($in);
                    fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
            // Open temp file
            $out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                fclose($in);
                fclose($out);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);
        }


        list($width, $height, $type) = getimagesize($filePath);



        if($width<401 || $height<401){
            $res['status'] = false;
            $res['title'] = app::$lang->common['operation_failed'];
            $res['msg']= app::$lang->profile['err_les200'];
            return $res;

        }else{
            $dest_x=400;
            $dest_y=400;

            $sourcefile = $filePath;
            $targetfile = $filePath;

            $arr = $this->resize($sourcefile, $dest_x, $dest_y, $targetfile);

            $width=$arr['width'];
            $height=$arr['height'];

        }

        $res['status'] = true;
        $res['cid'] = $_SESSION['account']['user_id'];
        $res['cleanFileName'] = $fileName;
        $res['width'] = $width+40;
        $res['type'] = $type;

        $res['imghost'] = app::$proto.app::$config->site['userhost'].'/userfiles/' ;

        return $res;

    }

    function createavatar(){
        $acc = new OrmAccounts();
        $targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR ."pic" ;

        $targ_w = $targ_h = 200;

        $src = $targetDir.DIRECTORY_SEPARATOR.$_POST['fname'];
        $nfname = uniqid().".jpg";
        $newfilename = $targetDir.DIRECTORY_SEPARATOR.$nfname;
        //app::trace($src);
        //$img_r = imagecreatefromjpeg($src);

        switch ($_POST['ftype'])
        {
            case 1: $img_r = imagecreatefromgif($src); break;
            case 2: $img_r = imagecreatefromjpeg($src);  break;
            case 3: $img_r = imagecreatefrompng($src); break;

        }


        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
        imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);

        switch ($_POST['ftype'])
        {
            case 1: imagegif($dst_r,$newfilename); break;
            case 2: imagejpeg($dst_r,$newfilename,100);  break;
            case 3: imagepng($dst_r,$newfilename,0); break;

        }


        //imagejpeg($dst_r,$newfilename ,$jpeg_quality);

        $upd['user_pic'] = $nfname;
        $acc->update($_SESSION['account']['user_id'],$upd);
        $res['cid'] = $_SESSION['account']['user_id'];
        $res['fname']= $nfname;

        $res['status'] = true;
        return $res;


    }

    function resize($img, $w, $h, $newfilename) {
        //Check if GD extension is loaded
        if (!extension_loaded('gd') && !extension_loaded('gd2')) {
            trigger_error("GD is not loaded", E_USER_WARNING);
            return false;
        }

        //Get Image size info
        $imgInfo = getimagesize($img);



        switch ($imgInfo[2]) {

            case 1: $im = imagecreatefromgif($img); break;
            case 2: $im = imagecreatefromjpeg($img);  break;
            case 3: $im = imagecreatefrompng($img); break;
            default:  trigger_error('Unsupported filetype!', E_USER_WARNING);  break;
        }

        //If image dimension is smaller, do not resize
        if ($imgInfo[0] <= $w && $imgInfo[1] <= $h) {
            $nHeight = $imgInfo[1];
            $nWidth = $imgInfo[0];
        }
        else{
            // yeah, resize it, but keep it proportional
            if ($w/$imgInfo[0] > $h/$imgInfo[1]) {
                $nWidth = $imgInfo[0]*($h/$imgInfo[1]);
                $nHeight = $h;
            }
            else{
                $nWidth = $w;
                $nHeight = $imgInfo[1]*($w/$imgInfo[0]);
            }
        }

        $nWidth = round($nWidth);

        $nHeight = round($nHeight);

        $newImg = imagecreatetruecolor($nWidth, $nHeight);

        /* Check if this image is PNG or GIF, then set if Transparent*/

        if(($imgInfo[2] == 1) OR ($imgInfo[2]==3)){
            imagealphablending($newImg, false);
            imagesavealpha($newImg,true);
            $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
            imagefilledrectangle($newImg, 0, 0, $nWidth, $nHeight, $transparent);
        }

        imagecopyresampled($newImg, $im, 0, 0, 0, 0, $nWidth, $nHeight, $imgInfo[0], $imgInfo[1]);

        //Generate the file, and rename it to $newfilename
        switch ($imgInfo[2]) {
            case 1: imagegif($newImg,$newfilename); break;
            case 2: imagejpeg($newImg,$newfilename);  break;
            case 3: imagepng($newImg,$newfilename); break;
            default:  trigger_error('Failed resize image!', E_USER_WARNING);  break;
        }

        $r['width'] =  $nWidth;
        $r['height'] = $nHeight;
        return $r;
    }
    
    function postimg(){
		

	
		$targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR  ."posts" ;	
			
		$cleanupTargetDir = true; // Remove old files
		$maxFileAge = 5 * 3600; // Temp file age in seconds

		@set_time_limit(5 * 60);

		// Uncomment this one to fake upload time
		// usleep(5000);
		
		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

		// Clean the fileName for security reasons
		$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);
		
		// Make sure the fileName is unique but only if chunking is disabled
		if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
			$ext = strrpos($fileName, '.');
			$fileName_a = substr($fileName, 0, $ext);
			$fileName_b = substr($fileName, $ext);
		
			$count = 1;
			while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
				$count++;
		
			$fileName = $fileName_a . '_' . $count . $fileName_b;
		}
		
		$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

		// Create target dir
		if($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'])@mkdir($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id']);
		if (!file_exists($targetDir))@mkdir($targetDir);
		
		
		
		// Remove old temp files	
		if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
			while (($file = readdir($dir)) !== false) {
				$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
		
				// Remove temp file if it is older than the max age and is not the current file
				if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
					@unlink($tmpfilePath);
				}
			}
		
			closedir($dir);
		} else
			die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
	

		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
			$contentType = $_SERVER["HTTP_CONTENT_TYPE"];
		
		if (isset($_SERVER["CONTENT_TYPE"]))
			$contentType = $_SERVER["CONTENT_TYPE"];
		
		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== false) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				// Open temp file
				$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
				if ($out) {
					// Read binary input stream and append it to temp file
					$in = fopen($_FILES['file']['tmp_name'], "rb");
		
					if ($in) {
						while ($buff = fread($in, 4096))
							fwrite($out, $buff);
					} else
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
					fclose($in);
					fclose($out);
					@unlink($_FILES['file']['tmp_name']);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
		} else {
			// Open temp file
			$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = fopen("php://input", "rb");
		
				if ($in) {
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
		
				fclose($in);
				fclose($out);
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}
		
		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) {
			// Strip the temp .part suffix off 
			rename("{$filePath}.part", $filePath);
		}
		
		/***
		  200px logo
		*/


		$tn_prefix="post_";
		
		$tnPath = $targetDir . DIRECTORY_SEPARATOR . $tn_prefix.$fileName;
		$tnFile= $tn_prefix.$fileName;
		$copy = copy($filePath, $tnPath);
		$sourcefile = $tnPath;
		$targetfile = $tnPath;
		$dest_x = 800;
		$dest_y = 800;
		$jpegqual =  app::$config->img['quality'];
		$this->resize($sourcefile, $dest_x, $dest_y, $targetfile);	
		
		$res['cid'] = $_SESSION['account']['user_id'];
		
		$res['cleanFileName'] = $fileName;
	//	$res['imghost'] = app::$proto.app::$config->site['userhost'] ;
		
		return $res;		
	}
	
	function posterimg(){
		

	
		$targetDir = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'].DIRECTORY_SEPARATOR  ."poster" ;	
			
		$cleanupTargetDir = true; // Remove old files
		$maxFileAge = 5 * 3600; // Temp file age in seconds

		@set_time_limit(5 * 60);

		// Uncomment this one to fake upload time
		// usleep(5000);
		
		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

		// Clean the fileName for security reasons
		$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);
		
		// Make sure the fileName is unique but only if chunking is disabled
		if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
			$ext = strrpos($fileName, '.');
			$fileName_a = substr($fileName, 0, $ext);
			$fileName_b = substr($fileName, $ext);
		
			$count = 1;
			while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
				$count++;
		
			$fileName = $fileName_a . '_' . $count . $fileName_b;
		}
		
		$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

		// Create target dir
		if($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id'])@mkdir($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."userfiles".DIRECTORY_SEPARATOR.$_SESSION['account']['user_id']);
		if (!file_exists($targetDir))@mkdir($targetDir);
		
		
		
		// Remove old temp files	
		if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
			while (($file = readdir($dir)) !== false) {
				$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
		
				// Remove temp file if it is older than the max age and is not the current file
				if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
					@unlink($tmpfilePath);
				}
			}
		
			closedir($dir);
		} else
			die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
	

		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
			$contentType = $_SERVER["HTTP_CONTENT_TYPE"];
		
		if (isset($_SERVER["CONTENT_TYPE"]))
			$contentType = $_SERVER["CONTENT_TYPE"];
		
		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== false) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				// Open temp file
				$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
				if ($out) {
					// Read binary input stream and append it to temp file
					$in = fopen($_FILES['file']['tmp_name'], "rb");
		
					if ($in) {
						while ($buff = fread($in, 4096))
							fwrite($out, $buff);
					} else
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
					fclose($in);
					fclose($out);
					@unlink($_FILES['file']['tmp_name']);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
		} else {
			// Open temp file
			$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = fopen("php://input", "rb");
		
				if ($in) {
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
		
				fclose($in);
				fclose($out);
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}
		
		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) {
			// Strip the temp .part suffix off 
			rename("{$filePath}.part", $filePath);
		}
		
		/***
		  200px logo
		*/


		$tn_prefix="poster_";
		
		$tnPath = $targetDir . DIRECTORY_SEPARATOR . $tn_prefix.$fileName;
		$tnFile= $tn_prefix.$fileName;
		$copy = copy($filePath, $tnPath);
		$sourcefile = $tnPath;
		$targetfile = $tnPath;
		$dest_x = 800;
		$dest_y = 800;
		$jpegqual =  app::$config->img['quality'];
		$this->resize($sourcefile, $dest_x, $dest_y, $targetfile);	
		
		$res['cid'] = $_SESSION['account']['user_id'];
		
		$res['cleanFileName'] = $fileName;
	//	$res['imghost'] = app::$proto.app::$config->site['userhost'] ;
		
		return $res;		
	}
    	
}