<?php      
namespace autoforum\controllers\pub;

use \autoforum\application as app;
use \autoforum\models\common\adminController as adminController;
use \autoforum\models\orm\posts as OrmPosts;
use \autoforum\models\orm\comments as OrmComments;
use \autoforum\models\orm\category as OrmCat;
use \autoforum\models\libs\pagination as PAGLIB;
 

class main extends adminController {
    
    function __before() {

    	parent::__before();
        $this->view->setPath(app::$device.'/pub');  
		$this->posts = new  OrmPosts();
		$this->comments = new  OrmComments();
		$this->category = new OrmCat();

		$this->view->topPub = "active";
		if(!isset($_SESSION['POST_STATUS']))$_SESSION['POST_STATUS']=0;

		$this->view->page_blog = "blog";
	}
     
    function __default($args = false) {   

        $pages = new PAGLIB();
    	$where=array();
    	$search=array();
    	//if($_SESSION['user_role']='admin')$this -> posts->getAllList($where=array());
		
		$this->view->popular = $this->getPopular();
		


		$this->view->category = $this->getCat();

		if(isset($_SESSION['PUB_SEARCH']))$search['post_subj']=$_SESSION['PUB_SEARCH'];
		//app::trace($_SESSION);
		unset($_SESSION['PUB_SEARCH']);
		$totalRec = $this->posts->getListCount($where,$search);

		$page = isset($_GET['p']) && $_GET['p']  ? \abs((int)$_GET['p']) :1;
    	
	    $itemsOnPage = isset($_GET['iop']) && $_GET['iop']  ? \abs((int)$_GET['iop']) : 10;
    	
	    //$pages = new \pagination();
	    $pagination = $pages->calculate_pages($totalRec,$itemsOnPage, $page);
		
		$where['post_status']=1;
		if(isset($_GET['cat']))$where['post_cat']=($_GET['cat']);

		$records = $this->posts->getList($where,$pagination['current'],$itemsOnPage,$search);
		//app::trace($records);
	    $this->view->curl = $_SERVER['REQUEST_URI'];
	    $this->view->iop = $itemsOnPage;
	    $this->view->pagination = $pagination;
	    $this->view->totalRec = $totalRec;
		if(isset($_GET['cat']))$this->view->pcat = $_GET['cat'];
		$this->view->records = $records;


		$this->view->setTemplate('index.tpl');  
        return $this->view;
		
		
		
    } 
    
    function view(){
		
		$id = $this->args[0];
		$rec = $this->posts->getByID($id);
		
		$this->view->popular = $this->getPopular();
		$this->view->category = $this->getCat();
		$wherec['comments_post_id']=$id;
		$this->view->comments =  $this->comments->getList($wherec, $search = array(),$order='comments_id DESC');



		//app::trace();
		$upd=array();
		$upd['post_view'] = $rec['post_view']=$rec['post_view']+1;
		$this->posts->update($id,$upd);
		
		$this->view->rec = $rec;


        $this->view->setTemplate('view.tpl');  
        return $this->view;
	    
	    
    }
	
	function getPopular(){
		return $this->posts->getList(array(),1,2,array(),'post_view DESC');
		
	}

	function getCat(){

		return	$this->category->getList($where=array(), $search = array());


	}

	function addCom(){
		$post =app::strings_clear( $_POST['post_id']);
		$text= app::strings_clear($_POST['text']);
		$add=array(

			"comments_user_id" => $_SESSION['account']['user_id'],
			"comments_post_id" =>$post ,
			"comments_text" => $text,
		);

		$this->comments->add($add);


        $res['id']=$_POST['post_id'];
		$res['status'] = true;
		return $res;

	}


	function del(){

		$id = $_POST['id'];
		$this->comments->delete($id);
		//app::trace($id);
		
		
		$res['status'] = true;
		return $res;
	}

	function changeSearch(){

		$_SESSION['PUB_SEARCH'] = trim($_POST['s']);
		$res['status'] = true;
		return $res;
	}


}
