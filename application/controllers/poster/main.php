<?php      
namespace autoforum\controllers\poster;

use \autoforum\application as app;
use \autoforum\models\common\adminController as adminController;
use \autoforum\models\orm\users as OrmUsers;
use \autoforum\models\orm\poster as OrmPosters;
use \autoforum\models\orm\posts as OrmPosts;
use \autoforum\models\libs\pagination as PAGLIB;


class main extends adminController {

    function __before() {

    	parent::__before();
        $this->view->setPath(app::$device.'/poster');   
		$this->users = new OrmUsers();
		$this->poster = new  OrmPosters();
		$this->posts = new  OrmPosts();
		$this->view->smPosters = "active";
		if(!isset($_SESSION['POSTER_STATUS']))$_SESSION['POSTER_STATUS']=0;
	}   

    function __default($args = false) {   
		
    	$pages = new PAGLIB();
    	$where=array();
    	$search=array();

		if(isset($_SESSION['POSTER_SEARCH']))$search['poster_text']=$_SESSION['POSTER_SEARCH'];
		unset($_SESSION['POSTER_SEARCH']);
		$this->view->popular = $this->getPopular();


		$totalRec = $this->poster->getListCount($where,$search);
		
    	$page = isset($_GET['p']) && $_GET['p']  ? \abs((int)$_GET['p']) :1;
    	
	    $itemsOnPage = isset($_GET['iop']) && $_GET['iop']  ? \abs((int)$_GET['iop']) : 5;
    	
	    //$pages = new \pagination();
	    $pagination = $pages->calculate_pages($totalRec,$itemsOnPage, $page);
		

		
		$records = $this->poster->getList($where,$pagination['current'],$itemsOnPage,$search);
		
	    $this->view->curl = $_SERVER['REQUEST_URI'];
	    $this->view->iop = $itemsOnPage;
	    $this->view->pagination = $pagination;
	    $this->view->totalRec = $totalRec;
		
		$this->view->records = $records;
   
        $this->view->setTemplate('index.tpl');  
        return $this->view;
    } 
    
  
    
    function add(){
		$this->view->popular = $this->getPopular();
		$this->view->setTemplate('add.tpl');
        return $this->view;
	    
	    
    }
    
    function edit(){
	    
	    $id = $this->args[0];
		$this->view->popular = $this->getPopular();
		$this->view->poster = $this->poster->getByID($id);
        $this->view->setTemplate('edit.tpl');  
        return $this->view;
	    
	    
	}    
	    
    
    
    function doAddPost(){
	    
	    $post = app::strings_clear($_POST['post']);
	   	 
			 
	    $add=array(
		    
		    "poster_user_id" => $_SESSION['account']['user_id'],
		    "poster_text" => $post,
		    "poster_date" => date('Y-m-d',strtotime($_POST['pdate'])),
			"poster_time" => $_POST['time'],
			"poster_pic" => $_POST['fname'],
	    );
	    
	    $this->poster->add($add);
	    
	    
	    $res['status'] = true;
	    return $res;
    }
	
	 function doUpdatePost(){
	    
	    $post = app::strings_clear($_POST['post']);
		$fname = app::strings_clear($_POST['fname']);
	    
	    $edit=array(
		    
		  	"poster_status" =>$_POST['status'],
		    "poster_text" => $post,
			"poster_date" => date('Y-m-d',strtotime($_POST['pdate'])),
			"poster_time" => $_POST['time'],
		
		   
	    );
	    
		
		if(strlen($fname))$edit['poster_pic'] = $fname;
		
	    $this->poster->update($_POST['poster_id'],$edit);
	    
	    
	    $res['status'] = true;
	    return $res;
    }
    
    
    function del(){
	    
	    $id = $_POST['id'];
	    
	    $this->poster->delete($id);
	    
	    $res['status'] = true;
	    return $res;
    }

	function changeSearch(){

		$_SESSION['POSTER_SEARCH'] = trim($_POST['s']);

		$res['status'] = true;
		return $res;
	}

	function getPopular(){
		return $this->posts->getList(array(),1,2,array(),'post_view DESC');

	}

}