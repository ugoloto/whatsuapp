<?php      
namespace autoforum\controllers\favorites;

use \autoforum\application as app;
use \autoforum\models\common\adminController as adminController;
use \autoforum\models\orm\fav as OrmFavPosts;
use \autoforum\models\orm\posts as OrmPosts;
use \autoforum\models\libs\pagination as PAGLIB;
 

class main extends adminController {
    
    function __before() {

    	parent::__before();
        $this->view->setPath(app::$device.'/favorites');  
		$this->fav = new  OrmFavPosts();
		$this->posts = new  OrmPosts();
		$this->view->smFavorites = "active";
		if(!isset($_SESSION['POST_STATUS']))$_SESSION['POST_STATUS']=0;
		$this->view->page_blog = "blog";
	}   
     
    function __default($args = false) {   

        $pages = new PAGLIB();
    	$where=array();
    	$search=array();
    	//if($_SESSION['user_role']='admin')$this -> posts->getAllList($where=array());
		//if(isset($_SESSION['FAV_SEARCH']))$search['posts.post_subj']=$_SESSION['FAV_SEARCH'];
		    	
		$totalRec = $this->fav->getListCount($where,$search);

		$this->view->popular = $this->getPopular();


		$page = isset($_GET['p']) && $_GET['p']  ? \abs((int)$_GET['p']) :1;
    	
	    $itemsOnPage = isset($_GET['iop']) && $_GET['iop']  ? \abs((int)$_GET['iop']) : 10;
    	
	    //$pages = new \pagination();
	    $pagination = $pages->calculate_pages($totalRec,$itemsOnPage, $page);
		
		//app::trace($pagination);
		
		$records = $this->fav->getList($where,$pagination['current'],$itemsOnPage,$search);
		
	    $this->view->curl = $_SERVER['REQUEST_URI'];
	    $this->view->iop = $itemsOnPage;
	    $this->view->pagination = $pagination;
	    $this->view->totalRec = $totalRec;
		
		$this->view->records = $records;
		
		$this->view->setTemplate('index.tpl');  
        return $this->view;
		
		
		
    } 
    
    function view(){
		
		$id = $this->args[0];
		$this->fav->getByID($id);
		$this->view->popular = $this->getPopular();

		//app::trace($rec);
		
        $this->view->setTemplate('view.tpl');  
        return $this->view;
	    
	    
    }
	
	function addFav(){
		
	    
	    $add=array(
		    
		    "fp_user_id" => $_SESSION['account']['user_id'],
		    "fp_post_id" => $_POST['id'],
		   
	    );
	    
	    $this->fav->add($add);
	    
	    
	    $res['status'] = true;
	    return $res;
		
	}
	
	 function del(){
	    
	    $id = $_POST['id'];
	    
	    $this->fav->delete($id);
	    
	    
	    return true;
    }

	function changeSearch(){

		$_SESSION['FAV_SEARCH'] = trim($_POST['s']);

		$res['status'] = true;
		return $res;
	}

	function getPopular(){
		return $this->posts->getList(array(),1,2,array(),'post_view DESC');

	}

    function __format() {
        return array(
         
        );
    } 
    
}
