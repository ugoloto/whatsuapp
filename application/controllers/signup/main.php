<?php      
namespace autoforum\controllers\signup;

use \autoforum\application as app;
use \autoforum\models\common\adminController as adminController;
use \autoforum\models\orm\users as OrmUsers;
use \autoforum\models\orm\posts as OrmPosts;


class main extends adminController {
    
    function __before() {

    	parent::__before();
		$this->posts = new  OrmPosts();
		$this->view->setPath(app::$device.'/signup');
		
	}   
     
    function __default($args = false) {

		$this->view->popular = $this->getPopular();
		$this->view->setTemplate('index.tpl');
        return $this->view;
    } 
    
    function doSignup(){

	 

        $email = \strtolower(\trim($_POST['username']));
        $pass = \trim($_POST['pass']);
		
		$users = new OrmUsers();
		
		$res = $users->createAccount($email, $pass);

		if($res){
			
			$r['status'] = true;
			return $r;
		}else{
			$r['status'] = false;
			$r['title'] = "Помилка";
			$r['msg'] = "Email вже існує";
			return $r;	
			
		}


		/* if(!$r['fail']){

                 $email=\strtolower(\trim($_POST['username']));
                 $pass=\trim($_POST['pass']);
                 $hash = false;

            try{
                 $account=$this->session->login($email,$pass,$hash);
             }catch (\Exception $e) {

                    $res['status']=false;
                    $msg=$e->getMessage();
                    $res['msg']=app::$lang->login[$msg];
                    $res['title'] = 'Login Failed';
                    return $res;
             }


         }    */

    }
	
	function getPopular(){
		return $this->posts->getList(array(),1,2,array(),'post_view DESC');

	}

}
