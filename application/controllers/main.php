<?php      
namespace autoforum\controllers;

use \autoforum\application as app;
use \autoforum\models\common\adminController as adminController;
use \autoforum\models\orm\posts as OrmPosts;

class main extends adminController {
    
    function __before() {

    	parent::__before();
        $this->view->setPath(app::$device.'/dashboard');   
		$this->view->topHome = "active";
        $this->posts = new  OrmPosts();

	}   
     
    function __default($args = false) {
        $this->view->popular = $this->getPopular();
        $this->view->setTemplate('index.tpl');  
        return $this->view;
    }

    function getPopular(){
        return $this->posts->getList(array(),1,2,array(),'post_view DESC');

    }
    
    function logout(){

	    $this->session->logout();
	    \jet\redirect('/login',true);
    }
    
    
    function __format() {
        return array(
         
        );
    } 
    
}
