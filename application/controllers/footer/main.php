<?php
namespace autoforum\controllers\footer;

use \autoforum\application as app;
use \autoforum\models\common\adminController as adminController;
use \autoforum\models\orm\subs as OrmSubs;

class main extends adminController {

    function __before() {

        parent::__before();
        $this->view->setPath(app::$device.'/common');
        $this->subs = new OrmSubs();
    }

    function __default($args = false) {


        //app::trace( $r);
        $this->view->setTemplate('footer.tpl');
        return $this->view;
    }

    function contact(){

        if((isset($_POST['qc-name'])&&$_POST['qc-name']!="")&&(isset($_POST['qc-email'])&&$_POST['qc-email']!="") &&(isset($_POST['qc-message'])&&$_POST['qc-message']!="")){
            $to = 'yuliya-240@mail.ru'; //Почта получателя
            //app::trace($to);
            $subject = 'Сообщение'; //Загаловок сообщения
            $name = trim($_POST['qc-name']); // поле имя
            $email = $_POST['qc-email']; // поле email
            $message = trim($_POST['qc-message']);// поле сообщения
            $headers = 'From: '. $email . "\r\n";
            $message = 'Имя: ' . $name .  'Email: ' . $email . $message;
            $subject = preg_replace("/(\r\n)|(\r)|(\n)/", "", $subject);
            $subject = preg_replace("/(\t)/", " ", $subject);
            $subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';
            /*$message = '
                <html>
                    <head>
                        <title>'.$subject.'</title>
                    </head>
                    <body>
                        <p>Имя: '.$_POST['qc-name'].'</p>
                        <p>email: '.$_POST['qc-email'].'</p>                        
                        <p>mess: '.$_POST['qc-message'].'</p>                        
                    </body>
                </html>'; //Текст нащего сообщения можно использовать HTML теги
            $headers  = "Content-type: text/html; charset=utf-8 \r\n"; //Кодировка письма
           $headers = "From: Отправитель <from@example.com>\r\n";*/


            mail($to, $subject, $message, $headers);
        }
       // app::trace();
        return true;
    }

    function doAdd(){
        $email = \strtolower(\trim($_POST['EMAIL']));


        $res=$this->subs->add($email);

        if($res){

            $r['status'] = true;
            return $r;
        }else{
            $r['status'] = false;
            $r['title'] = "Помилка";
            $r['msg'] = "Email вже існує";
            return $r;


        }
    }



    }