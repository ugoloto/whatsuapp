{% extends skin~"/common/root.tpl" %}
{% block content %}

<!--Breadcrumbs-->
<div class="container space-top">
    <ol class="breadcrumb">
        <li><a href="index.tpl">Головна</a></li>
        <li class="active">Афіша</li>
    </ol>
</div><!--Breadcrumbs Close-->

<div class="container double-padding-bottom">
    <div class="row">

        <!--Blog Posts-->
        <section class="col-md-8 grid-view">
            <!--Row-->
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    {% for r in records%}
                    <div class="post">
                        <div class="featured-img">
                            <a href="/events/view/{{r.poster_id}}"><img class="img-responsive"
                                                                        src="/userfiles/{{r.user_id}}/poster/{{r.poster_pic}}"
                                                                        alt="Post01"/></a>

                            <h3><a href="/events/view/{{r.poster_id}}">{{r.poster_text}}</a></h3>
                            <span class="devider"></span>
                            <div class="taxonomy">
                                <a href="#">{{r.user_nick}}</a></br>
                                Дата Події
                                <a href="#">{{r.poster_date|date("d.m.Y")}}</a>
                            </div>
                        </div>
                    </div>
                    {%endfor%}

                    <!--Pagination-->
                    {%if pagination.previous%} <a href="{{module}}?p={{pagination.previous}}" style="margin: 4px 0;"><i
                                class="fa fa-arrow-left"></i></a>{%endif%}
                    {{pagination.info}}
                    {%if pagination.next%}<a href="{{module}}?p={{pagination.next}}" style="margin: 6px 0 0 0;"><i
                                class="fa fa-arrow-right"></i></a>{%endif%}
                </div>
                    </div>
        </section><!--Blog Posts Close-->

        <!--Sidebar-->
        <div class="col-md-4" id="sidebar">
            <aside class="sidebar right">

                <!--Recent/Featured posts widget-->
                <div class="widget rposts">
                    <h3>Популярні події</h3>

                    {%for pop in popul%}
                    <div class="rpost">
                        <div class="rpost-img">
                            <img src="/userfiles/{{pop.user_id}}/poster/{{pop.poster_pic}}" alt="">
                        </div>
                        <div class="rpost-text">
                            <h4><a href="/pub/view/{{pop.poster_id}}">{{pop.poster_text}}</a></h4>
                            <p>Автор: {{pop.user_nick}}</p>
                            <p>Дата: {{pop.poster_date|date("d.m.Y")}}</p>
                            <p>Час: {{pop.poster_time}}</p>
                        </div>
                    </div>
                    {%endfor%}

                    <!-- Links Widget-->
                   {# <div class="widget links">
                        <h3>Категорії</h3>

                        <ul class="list-unstyled">
                            <li><a href="#">Home <span>13</span></a></li>
                            <li><a href="#">Shop <span>12</span></a></li>
                            <li><a href="#">Portfolio <span>4</span></a></li>
                        </ul>
                    </div>#}

            </aside>
        </div><!--Sidebar Close-->

    </div>
</div>

{%endblock%}
{%block js%}
<script src="/js/events.js"></script>
{%endblock%}