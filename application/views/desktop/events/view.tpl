{% extends skin~"/common/root.tpl" %}
{% block content %}

<!--Single Post-->
<section class="container space-top double-space-bottom">
    <div class="row">

        <!--Content-->
        <section class="col-md-8">
            <h2 class="post-title">Фото події</h2>
            <input type="hidden" name="poster_id" value="{{poster.poster_id}}">
            <div class="row lightGallery">
                <div class="col-md-6 col-sm-6" data-src="img/blog/slider/01.jpg" data-title="Image 01">
                    <div class="thumbnail-zoom">
                        <div class="overlay"></div>
                        <div class="figcap">
                            <i class="fa fa-search-plus"></i>
                            <p>Image Title</p>
                        </div>
                        <img src="/userfiles/{{rec.user_id}}/poster/{{rec.poster_pic}}"/>
                    </div>
                </div>
            </div>
            <p>{{rec.poster_text}}</p>

            <p></p>

            <!--Post Toolbox-->
            <div class="post-toolbox">

                <div class="buttons">
                    <a href="#"><i class="fa fa-plus"></i></a>
                    <a href="#" data-toggle="modal" data-target="#shareModal"><i class="fa fa-share"></i></a>
                    <a onclick="Share.facebook('URL','TITLE','DESC')"><i class="fa fa-facebook"></i></a>
                    <a onclick="Share.twitter('URL','TITLE')"><i class="fa fa-twitter"></i></a>

                </div>
            </div>

            <!--Post Meta-->
            <div class="post-meta">
                <div class="taxonomy">
                    <a href="#">Автор події: {{rec.user_nick}}</a>
                    <div class="date">Час: {{rec.poster_time}}</div>
                </div>
                <div class="date">Дата: {{rec.poster_date|date("d.m.Y")}}</div>
            </div>

            <!--Disqus Comments-->
            {%if acs.user_id%}
            <div class="double-space-top">
                <div class="page-heading center">
                    <h3>Залиште коментар</h3>
                    <p>Будь-ласка, переконайтеся що Ви увійшли у систему, щоб залишити коментар</p>
                </div>
                <form id = "doAddComments">
                    <input type="hidden" name="poster_id" value="{{rec.poster_id}}">
                    <div class="form-group">
                        <textarea class="form-control" name="text" cols="50" placeholder="Введіть коментар"></textarea>
                    </div>
                    <a href="#" class="btn btn-md btn-default2" type="reset">Скасувати</a>
                    <button class="btn btn-md btn-primary pull-right" id="doSave"  type="submit">Додати
                    </button>
                </form>

            </div>
            {%endif%}


            {%for r in postercom%}

            <input type="hidden"id="poster_id" value="{{r.poster_id}}">
            <div class=" panel static panel-default space-top">
                <div class=" panel-heading">
                    <div class="col-md-2">
                        <img class="img-responsive center-block" src="/userfiles/{{r.user_id}}/avatar/{{r.user_pic}}" style="margin-top: 10px;"/>

                    </div>
                    <a href="#">{{r.user_nick}}</a>
                    <div class="col-md-10 panel-body">
                        <p><h4>{{r.pc_text}}</h4></p>
                    </div>
                    <div class="right"><span class="text-muted">{{r.pc_date|date("d.m.Y")}}</span></div>

                </div>
                {%if acs.user_role=="admin"%}<a href="#" class="del" data-id="{{r.pc_id}}"><i class="fa fa-times"></i></a>{%endif%}
            </div>



            {%endfor%}


        </section><!--Content Close-->

        <!--Sidebar-->
        <div class="col-md-4 space-bottom" id="sidebar">
            <aside class="sidebar right">


                <!--Recent/Featured posts widget-->
                <div class="widget rposts">
                    <h3>Популярні</h3>
                    {%for pop in popul%}
                    <div class="rpost">
                        <div class="rpost-img">
                            <img src="/userfiles/{{pop.user_id}}/poster/{{pop.poster_pic}}" alt="">
                        </div>
                        <div class="rpost-text">
                            <h4><a href="/events/view/{{pop.poster_id}}">{{pop.poster_text}}</a></h4>
                            <p>Автор: {{pop.user_nick}}</p>
                            <p>Дата: {{pop.poster_date|date("d.m.Y")}}</p>
                            <p>Час: {{pop.poster_time}}</p>
                        </div>
                    </div>
                    {%endfor%}
                </div>

                <!-- Links Widget-->
                <div class="widget links">
                    <h3>Категорії</h3>

                    <ul class="list-unstyled">
                        <li><a href="#">Home <span>13</span></a></li>
                        <li><a href="#">Shop <span>12</span></a></li>
                        <li><a href="#">Portfolio <span>4</span></a></li>
                    </ul>
                </div>
            </aside>
        </div><!--Sidebar Close-->
    </div>
</section><!--Single Post Content Close-->


<!--Sidebar Toggle-->
<a href="#sidebar" class="sidebar-button"><i class="fa fa-outdent"></i></a>
{% endblock %}

{%block js%}

<script src="/js/postercom.js"></script>
<script src="/js/Share.js"></script>
{%endblock%}