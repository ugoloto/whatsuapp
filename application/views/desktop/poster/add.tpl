{% extends skin~"/common/root.tpl" %}
{%block css%}

<link href="/js/lib/datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="/js/lib/timepicker/css/jquery.timepicker.min.css" rel="stylesheet">
{%endblock%}


{% block content %}
<section class="page-block user-account double-space-bottom">
    <div class="container">
        <div class="row">

            {%include skin~"/profile/menu.tpl"%}


            <!--Content-->
            <div class="col-md-9 gray-bg">
                <div class="tab-content">


                    <!--Notifications-->

                    <div class="tab-pane fade in active" id="notify">
                        <h3 class="heading center">Подія</h3>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">

                                <form id="poster" enctype="multipart/form-data">


                                    <input type="text" id="calendar"
                                           {%if poster.poster_date|date("d.m.Y")==date %}selected{%endif%}
                                           value="{{date}}" name="pdate"/>
                                    <input type="text" id="timepicker" name="time"/>

                                    <div class="form-group">
                                        <label>Опис</label>
                                        <textarea class="form-control" name="post" cols="50"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Додати Фото</label>
                                        <input type="hidden" name="fname" id="img-file">
                                        <div class="form-group">
                                            <div id="containerpic">
                                                <img class="img_responsive" id="preview" src="/img/nopic.jpg"
                                                     style="display:none;">
                                                <button id="pickfiles" class="btn btn-primary">Upload Image</button>
                                            </div>
                                        </div>
                                    </div>

                                    <a href="/poster" class="btn btn-md btn-default2" type="reset">Скасувати</a>
                                    <button class="btn btn-md btn-primary pull-right" id="doSave" type="submit">Додати
                                    </button>
                                </form>

                            </div>


                        </div>


                    </div>


                </div>
            </div>
        </div>
    </div>
</section>
{%endblock%}

{%block js%}

<script src="/js/lib/datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/js/lib/datepicker/locales/bootstrap-datepicker.ru.min.js"></script>
<script src="/js/lib/timepicker/js/jquery.timepicker.min.js"></script>
<script src="/js/lib/plupload-2.1.9/plupload.full.min.js"></script>
<script src="/js/lib/bootbox.min.js"></script>
<script src="/js/lib/crope/cropper.js"></script>

<script src="/js/poster.js"></script>

{%endblock%}
