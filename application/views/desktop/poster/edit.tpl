{% extends skin~"/common/root.tpl" %}
{%block css%}
<link href="/js/lib/crope/cropper.css" rel="stylesheet">
<link href="/js/lib/datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="/js/lib/timepicker/css/jquery.timepicker.min.css" rel="stylesheet">
{%endblock%}


{% block content %}
<section class="page-block user-account double-space-bottom">
    <div class="container">
        <div class="row">

            {%include skin~"/profile/menu.tpl"%}


            <!--Content-->
            <div class="col-md-9 gray-bg">
                <div class="tab-content">


                    <!--Notifications-->

                    <div class="tab-pane fade in active" id="notify">
                        <h3 class="heading center">Подія</h3>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">


                                <form id="posterEdit" enctype="multipart/form-data">
                                    <input type="hidden" name="poster_id" value="{{poster.poster_id}}">
                                    <div class="form-group">
                                        <label for="af-first-name">Статус </label>
                                        <select class="control-form" name="status">
                                            <option value="0">Не Опубликована</option>
                                            <option {%if poster.poster_status==1%}selected{%endif%} value="1">
                                                Опубликована
                                            </option>
                                        </select>

                                    </div>

                                    <input type="hidden" name="fname" id="img-file">
                                    <div class="form-group">
                                        <div id="containerpic">
                                            <img src="/userfiles/{{acs.user_id}}/poster/{{poster.poster_pic}}">
                                            <img class="img_responsive" id="preview" src="/img/nopic.jpg"
                                                 style="display:none;">
                                            <button id="pickfiles" class="btn btn-primary">Редагувати зображення
                                            </button>

                                        </div>
                                    </div>

                                    <input type="text" id="calendar"
                                           {%if poster.poster_date|date("d.m.Y")==date %}selected{%endif%}
                                           value="{{poster.poster_date|date('d.m.Y')}}" name="pdate"/>
                                    <input type="text" id="timepicker" name="time" value="{{poster.poster_time}}"/>

                                    <div class="form-group">
                                        <label>Подія</label>
                                        <textarea class="form-control" name="post"
                                                  cols="50">{{poster.poster_text}}</textarea>
                                    </div>

                                    <a href="/poster" class="btn btn-md btn-default2" type="reset">Скасувати</a>
                                    <button class="btn btn-md btn-primary pull-right" id="doSave" type="submit">
                                        Обновити
                                    </button>
                                </form>

                            </div>


                        </div>


                    </div>


                </div>
            </div>
        </div>
    </div>
</section>
{%endblock%}

{%block js%}
<script src="/js/lib/datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/js/lib/datepicker/locales/bootstrap-datepicker.ru.min.js"></script>
<script src="/js/lib/timepicker/js/jquery.timepicker.min.js"></script>

<script src="/js/lib/plupload-2.1.9/plupload.full.min.js"></script>
<script src="/js/lib/bootbox.min.js"></script>
<script src="/js/lib/crope/cropper.js"></script>
<script src="/js/poster.js"></script>

{%endblock%}
