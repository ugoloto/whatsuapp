<!--Footer-->
<footer class="footer">

    <!-- Footer Subscribe -->
    <div class="footer-subscribe-widget light-version">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <p>Підписатися на наші новини. Не хвилюйтеся, ми не спамимо Вас!</p>
                </div>
                <div class="col-md-5">
                    <form class="validate" id="subs">
                        <div class="row">
                            <div class="col-sm-8">
                                <input type="email" class="form-control" name="EMAIL" id="subscr"
                                       placeholder="Email" required>
                            </div>

                          <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;"><input type="text"
                                                                                   name="b_168a366a98d3248fbc35c0b67_06b7eb4e02"
                                                                                   tabindex="-1" value=""></div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-transparent btn-md btn-primary">Підписатися
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="container space-top">
        <div class="row">

            <!--Featured Posts-->
            <div class="col-md-3 col-sm-3 col-xs-6">
                <h3>autoforum?</h3>
                <div class="featured-post">
                    <h5><a href="#">Про нас</a></h5>
                </div>
                <div class="featured-post">
                    <h5><a href="#">Контакти</a></h5>
                </div>

            </div>

            <!--Featured Posts-->
            <div class="col-md-3 col-sm-3 col-xs-6">
                <h4>Популярні публікації</h4>
                {%for pop in popular%}
                <div class="featured-post">
                    <h5><a href="/pub/view/{{pop.post_id}}">{{pop.post_subj}} </a></h5>
                    <div class="meta group">
                        <div class="right"><span
                                    class="text-muted">Дата публікації: {{pop.post_date|date("d.m.Y")}}</span></div>
                    </div>
                </div>
                {%endfor%}
            </div>

            <!-- Flickr wiget -->
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="flickr-wiget">
                    <h4>ФОТО</h4>


                </div>
            </div>

            <!--Contact Info-->
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="contacts">
                    <h4>Контакти</h4>
                    <h5>Адреса:</h5>
                    <p>105448 Casa Blanca BLVD New York</p>
                    <h5>Телефони:</h5>
                    <p>+9 876 543 00 21<br/>+0 123 400 56 78</p>
                    <h5>E-mail:</h5>
                    <p><a href="mailto:Yuliya-240@mail.ru">Yuliya-240@mail.ru</a></p>
                </div>
            </div>
        </div>
    </div>

    <!--Copyright-->
    <div class="copyright text-center">
        <div class="container">
            <p class="pull-left"><a href="/" target="_blank">autoforum</a> - Усі права захищені</p>

            <div class="social-bar">
                <a href="http://facebook.com" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://twitter.com" target="_blank"><i class="fa fa-twitter"></i></a>
                <a href="https://www.pinterest.com" target="_blank"><i class="fa fa-pinterest"></i></a>

                <a href="https://plus.google.com/collections/featured?hl=de" target="_blank"><i
                            class="fa fa-google-plus"></i></a>
            </div>
        </div>
    </div>
</footer><!--Footer Close

