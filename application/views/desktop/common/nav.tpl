<header class="header sticky">
    <!--Adding class "sticky" to header enables its pinning to top on scroll. Please note in a "boxed" mode header doesn't stick to the top.-->
    <div class="inner">
        <div class="container group">

            <!--Logo-->
            <a class="logo" href="index.html"><img src="/img/logo.png" alt="autoforum.eu"/></a>

            <!--Navigation Toggle-->
            <div class="left-off-canvas-toggle" id="nav-toggle">
                <span></span>
            </div>

            <!--Site Navigation-->
            <div class="navigation">
                <!--Menu-->
                <nav class="menu">
                    <ul>
                        <li class="{{topHome}}"><a href="/">Головна</a></li>

                        <li class="{{topPub}}"><a href="/pub">Публікації</a></li>

                        <li class="{{topEvents}}"><a href="/events">Афіша</a></li>

                      {#<li class="{{topBB}}"><a href="/billboard">Оголошення</a></li>#}
                    </ul>
                </nav>
                <!--Search-->
                {%if topHome or smProfile or smFavorites%}<div style="display: none"></div>{%else%}
                <div class="search"><i class="flaticon-search100"></i></div>{%endif%}
            </div>

            <!--Tools-->
            <div class="tools group">
                {%if acs.user_id%}
                <div class="user"><a href="/profile"><i class="fa fa-user"></i>Кабінет</a>

                    <div class="user-dropdown">
                        <a href="/profile">Профіль</a>
                        <a href="/logout">Вихід</a>
                    </div>
                </div>
                {%endif%}
                {# <div class="cart-btn">
                <a class="link" href="shopping-cart.html">
                <span>4</span>
                </a>
                <div class="cart-dropdown">
                <div class="empty-cart">
                <p>There are no items in your cart.</p>
                <a class="btn btn-center btn-primary" href="shop-grid-1-big-banner.html">Go to catalog</a>
                </div>
                <div class="wrap">
                <div class="container">
                <div class="row">
                <div class="col-md-8 col-sm-9">
                <div class="owl-carousel">
                <div class="item">
                <div class="delete"><i class="fa fa-angle-right"></i><i class="fa fa-angle-left"></i></div>
                <a href="shop-single-dr.html">
                <span class="name">Tourist backpack</span>
                <span class="price">175 $</span>
                <span class="overlay"></span>
                <img src="img/cart-dropdown/item01.jpg" alt="Item01"/>
                </a>
                <div class="qnt-count">
                <a class="incr-btn fa fa-angle-left inactive" data-action="decrease" href="#"></a>
                <input class="quantity" type="text" value="1">
                <a class="incr-btn fa fa-angle-right" data-action="increase" href="#"></a>
                </div>
                </div>
                <div class="item">
                <div class="delete"><i class="fa fa-angle-right"></i><i class="fa fa-angle-left"></i></div>
                <a href="shop-single-dl.html">
                <span class="name">Horsefeathers 752</span>
                <span class="price">129 $</span>
                <span class="overlay"></span>
                <img src="img/cart-dropdown/item02.jpg" alt="Item02"/>
                </a>
                <div class="qnt-count">
                <a class="incr-btn fa fa-angle-left inactive" data-action="decrease" href="#"></a>
                <input class="quantity" type="text" value="1">
                <a class="incr-btn fa fa-angle-right" data-action="increase" href="#"></a>
                </div>
                </div>
                <div class="item">
                <div class="delete"><i class="fa fa-angle-right"></i><i class="fa fa-angle-left"></i></div>
                <a href="shop-single-dr.html">
                <span class="name">Leather handbag</span>
                <span class="price">345 $</span>
                <span class="overlay"></span>
                <img src="img/cart-dropdown/item03.jpg" alt="Item03"/>
                </a>
                <div class="qnt-count">
                <a class="incr-btn fa fa-angle-left inactive" data-action="decrease" href="#"></a>
                <input class="quantity" type="text" value="1">
                <a class="incr-btn fa fa-angle-right" data-action="increase" href="#"></a>
                </div>
                </div>
                <div class="item">
                <div class="delete"><i class="fa fa-angle-right"></i><i class="fa fa-angle-left"></i></div>
                <a href="shop-single-dl.html">
                <span class="name">Ultra durable backpack</span>
                <span class="price">220 $</span>
                <span class="overlay"></span>
                <img src="img/cart-dropdown/item04.jpg" alt="Item04"/>
                </a>
                <div class="qnt-count">
                <a class="incr-btn fa fa-angle-left inactive" data-action="decrease" href="#"></a>
                <input class="quantity" type="text" value="1">
                <a class="incr-btn fa fa-angle-right" data-action="increase" href="#"></a>
                </div>
                </div>
                </div>
                </div>
                <div class="col-md-3 col-md-offset-1 col-sm-3">
                <table>
                <tr>
                <td>Subtotal</td>
                <td class="text-right">999 $</td>
                </tr>
                <tr>
                <td>Delivery</td>
                <td class="text-right">111 $</td>
                </tr>
                </table>
                <table class="total">
                <tr>
                <td>Total</td>
                <td class="text-right">1110 $</td>
                </tr>
                </table>
                <div class="links">
                <a href="#">Need a Special Preparation?</a><br />
                <a href="#" id="promo-code-link">Gift or Promo Code?</a>
                <form class="promo-code group">
                <label for="promo-code"></label>
                <input class="form-control" type="text" name="promo-code" id="promo-code" placeholder="Code">
                <input class="btn disabled" type="submit" value="Apply">
                </form>
                </div>
                <a class="btn btn-center btn-primary" href="checkout.html">Checkout</a>
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>#}
                {#<a class="pull-right" href="/login">Login</a>
                <a class="signup pull-right" href="/signup">Реєстрація</a>#}
            </div><!--Tools Close-->

        </div>
    </div>
{%if topHome or smProfile or smFavorites %} <div style="display: none"></div>
    <!--Quick Search-->
    {%else%}
    <form class="quick-search" autocomplete="off">
        <div class="overlay"></div>
        <input class="search-field" name="s" type="text" placeholder="Пошук">
        <span>Натисніть enter для пошуку</span>
    </form>

{%endif%}
</header><!--Header Close-->