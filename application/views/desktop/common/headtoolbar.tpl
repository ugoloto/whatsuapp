<!--Header Toolbar-->
<div class="header-toolbar">
    <div class="container">
        <div class="cont-info">
            <div><i class="fa fa-envelope"></i><a href="mailto:Yuliya-240@mail.ru" target="_blank">Yuliya-240@mail.ru</a>
            </div>
        </div>
        <div class="social-bar">
            <a href="http://facebook.com" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="https://twitter.com" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="https://www.pinterest.com" target="_blank"><i class="fa fa-pinterest"></i></a>

            <a href="https://plus.google.com/collections/featured?hl=de" target="_blank"><i
                        class="fa fa-google-plus"></i></a>
        </div>

        <ul class="tools">
        <li>
            <select class="form-control" name="lang" style="border-color: #ffffff;">

                <option class="lang" value="ua">Укр</option>
                <option class="lang" {%if lan=="ru"%}selected{%endif%} value="ru">Рус</option>
                <option class="lang" {%if lan=="de"%}selected{%endif%} value="de">Deu</option>

            </select>
        </li>
            <li><a href="#">&nbsp;|&nbsp;</a></li>
            {%if acs.user_id%}
            <li><a href="/logout" style="color: red;">Вихід</a></li>
            {%else%}
            <li><a href="/signup" style="color: red;">Реєстрація</a></li>
            <li><a href="/login">Логін</a></li>
            {%endif%}
        </ul>
    </div>
</div><!--Header Toolbar Close-->
          
