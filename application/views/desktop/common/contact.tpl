<div class="sticky-btns">
    <form class="quick-contact" method="post" name="quick-contact">
        <h3>Зворотній зв'язок</h3>
        <div class="form-group">
            <label for="qc-name">Ім'я</label>
            <input class="form-control" type="text" name="qc-name" id="qc-name" placeholder="Введіть свое ім'я"
                   required>
        </div>
        <div class="form-group">
            <label for="qc-email">Email</label>
            <input class="form-control" type="email" name="qc-email" id="qc-email" placeholder="Введіть email" required>
        </div>
        <div class="form-group">
            <label for="qc-message">Повідомлення</label>
            <textarea class="form-control" name="qc-message" id="qc-message" placeholder="Напишіть Ваше повідомлення"
                      required></textarea>
        </div>
        <button class="btn btn-primary btn-block" id="doSendContact" type="submit" >Надіслати</button>
    </form>
    <span id="qcf-btn"><i class="fa fa-envelope"></i></span>
    <span id="scrollTop-btn"><i class="fa fa-chevron-up"></i></span>
</div><!--Sticky Buttons Close-->
