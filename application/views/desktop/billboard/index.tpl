{% extends skin~"/common/root.tpl" %}
{% block content %}

<div class="page search-results">


    <!--Toolbox-->
    <div class="toolbox top-line sorting">
        <div class="container">
            <div class="heading">
                <h1>Оголошення</h1>
            </div>
            <ul class="sort">
                <li>Order by</li>
                <li class="active"><a href="#">Date</a></li>
                <li><a href="#">Relevance</a></li>
            </ul>
        </div>
    </div><!--Toolbox Close-->

    <!--Search Results-->
    <section class="list-view double-space-top space-bottom">
        <div class="container">
            <div class="row">

                <!--Column Left-->
                <div class="col-lg-6">
                    <div class="row post">
                        <div class="col-lg-5 col-md-4 col-sm-4">
                            <a class="featured-img" href="#"><img src="img/blog/posts/post01.jpg" alt="Post01"/></a>
                        </div>
                        <div class="col-lg-7 col-md-8 col-sm-8">
                            <h3><a href="#">Welcome to our website!</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, quae sit architecto unde
                                possimus voluptatem velit.</p>
                            <span class="devider"></span>
                            <div class="meta group">
                                <div class="left">
                                    <a href="#">John Doe</a>
                                    <span class="text-muted">in</span>
                                    <a href="#">Editor Picks</a>
                                </div>
                                <div class="right"><span class="text-muted">5 min read</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="row post">
                        <div class="col-lg-5 col-md-4 col-sm-4">
                            <a class="featured-img" href="#"><img src="img/blog/posts/post02.jpg" alt="Post02"/></a>
                        </div>
                        <div class="col-lg-7 col-md-8 col-sm-8">
                            <h3><a href="#">LAUNCHING NEW ROCKET</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, quae sit architecto unde
                                possimus voluptatem velit.</p>
                            <span class="devider"></span>
                            <div class="meta group">
                                <div class="left">
                                    <a href="#">James Roe</a>
                                    <span class="text-muted">in</span>
                                    <a href="#">Most Popular</a>
                                </div>
                                <div class="right"><span class="text-muted">10 min read</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="row post">
                        <div class="col-lg-5 col-md-4 col-sm-4">
                            <a class="featured-img" href="#"><img src="img/blog/posts/post03.jpg" alt="Post03"/></a>
                        </div>
                        <div class="col-lg-7 col-md-8 col-sm-8">
                            <h3><a href="#">Found new constellation</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, quae sit architecto unde
                                possimus voluptatem velit.</p>
                            <span class="devider"></span>
                            <div class="meta group">
                                <div class="left">
                                    <a href="#">Dexter Dirk</a>
                                    <span class="text-muted">in</span>
                                    <a href="#">Our Galaxy</a>
                                </div>
                                <div class="right"><span class="text-muted">15 min read</span></div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Column Right-->
                <div class="col-lg-6">
                    <div class="row post">
                        <div class="col-lg-5 col-md-4 col-sm-4">
                            <a class="featured-img" href="#"><img src="img/blog/posts/post04.jpg" alt="Post04"/></a>
                        </div>
                        <div class="col-lg-7 col-md-8 col-sm-8">
                            <h3><a href="#">Review of Nokia Lumia 1520</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, quae sit architecto unde
                                possimus voluptatem velit.</p>
                            <span class="devider"></span>
                            <div class="meta group">
                                <div class="left">
                                    <a href="#">Will Smith</a>
                                    <span class="text-muted">in</span>
                                    <a href="#">Tech Reviews</a>
                                </div>
                                <div class="right"><span class="text-muted">8 min read</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="row post">
                        <div class="col-lg-5 col-md-4 col-sm-4">
                            <a class="featured-img" href="#"><img src="img/blog/posts/post05.jpg" alt="Post05"/></a>
                        </div>
                        <div class="col-lg-7 col-md-8 col-sm-8">
                            <h3><a href="#">Summer Vacation Tips</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, quae sit architecto unde
                                possimus voluptatem velit.</p>
                            <span class="devider"></span>
                            <div class="meta group">
                                <div class="left">
                                    <a href="#">Susy Davis</a>
                                    <span class="text-muted">in</span>
                                    <a href="#">Vacations</a>
                                </div>
                                <div class="right"><span class="text-muted">20 min read</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="row post">
                        <div class="col-lg-5 col-md-4 col-sm-4">
                            <a class="featured-img" href="#"><img src="img/blog/posts/post06.jpg" alt="Post06"/></a>
                        </div>
                        <div class="col-lg-7 col-md-8 col-sm-8">
                            <h3><a href="#">Choosing Photocamera</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, quae sit architecto unde
                                possimus voluptatem velit.</p>
                            <span class="devider"></span>
                            <div class="meta group">
                                <div class="left">
                                    <a href="#">Will Smith</a>
                                    <span class="text-muted">in</span>
                                    <a href="#">Tech Reviews</a>
                                </div>
                                <div class="right"><span class="text-muted">16 min read</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--Pagination-->
            <ul class="pagination">
                <li class="page-count">Page 1 of 20:</li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>...</li>
                <li><a href="#">20</a></li>
                <li class="next"><a href="#">Next &raquo;</a></li>
            </ul>
        </div>
    </section><!--Search Results Close-->


</div>
{%endblock%}