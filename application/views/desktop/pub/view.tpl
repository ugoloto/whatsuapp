{% extends skin~"/common/root.tpl" %}
{%block content%}
<section class="container space-top double-space-bottom">
    <div class="row">

        <!--Content-->
        <section class="col-md-8">

            <h2 class="post-title">Фото події</h2>
            <input type="hidden" name="post_id" value="{{post.post_id}}">

            <!--Slider-->


            <img src="/userfiles/{{rec.user_id}}/posts/{{rec.post_pic}}"/>
            <!-- slide caption -->


            <h3>{{rec.post_subj}}</h3>

            <p>{{rec.post_text}}</p>

            <!--Post Toolbox-->
            <div class="post-toolbox ">
                <a class="btn btn-md btn-outlined btn-primary" data-id="{{rec.post_id}}" id="doFav">Додати в обране<i
                            class="fa fa-heart-o right"></i></a>
                <div class="buttons">
                    <a href="#"><i class="fa fa-plus"></i></a>
                    <a href="#" data-toggle="modal" data-target="#shareModal"><i class="fa fa-share"></i></a>
                    <a onclick="Share.facebook('URL','TITLE','DESC')"><i class="fa fa-facebook"></i></a>
                    <a onclick="Share.twitter('URL','TITLE')"><i class="fa fa-twitter"></i></a>
                </div>
            </div>

            <!--Post Meta-->
            <div class="post-meta">
                <div class="taxonomy">
                    <a href="#">{{rec.user_nick}}</a>
                </div>
                <div class="date">{{rec.post_date|date("d.m.Y")}}</div>
            </div>

            <!--Disqus Comments-->
            {%if acs.user_id%}
            <div class="double-space-top">
                <div class="page-heading center">
                    <h3>Залиште коментар</h3>
                    <p>Будь-ласка, переконайтеся що Ви увійшли у систему, щоб залишити коментар</p>
                </div>
                <form id = "doAddComments">
                    <input type="hidden" name="post_id" value="{{rec.post_id}}">
                    <div class="form-group">
                    <textarea class="form-control" name="text" cols="50" placeholder="Введіть коментар"></textarea>
                    </div>
                    <a href="#" class="btn btn-md btn-default2" type="reset">Скасувати</a>
                    <button class="btn btn-md btn-primary pull-right" id="doSave"  type="submit">Додати
                    </button>
                </form>

            </div>
            {%endif%}


            {%for r in comments%}

            <input type="hidden" id="post_id"  name="post_id" value="{{r.post_id}}">
            <div class=" panel static panel-default space-top">
                <div class=" panel-heading">
                    <div class="col-md-2">
               {# <span class="flaticon-account4" style="font-size: 5.7em"></span>#}
           <img class="img-responsive center-block" src="/userfiles/{{r.user_id}}/avatar/{{r.user_pic}}" style="margin-top: 10px;"/>

                    </div>
                    <a href="#">{{r.user_nick}}</a>
                    <div class="col-md-10 panel-body">
                           <p><h4>{{r.comments_text}}</h4></p>
                        </div>
                        <div class="right"><span class="text-muted">{{r.comments_date|date("d.m.Y")}}</span></div>

                </div>
                {%if acs.user_role=="admin"%}

                <a href="#" class="D" data-id="{{r.comments_id}}"><i class="fa fa-times"></i></a>
                {%endif%}
            </div>



            {%endfor%}


        </section><!--Content Close-->

        <!--Sidebar-->
        <div class="col-md-4 space-bottom" id="sidebar">
            <aside class="sidebar right">

                <!--Search Widget-->

                <!--Recent/Featured posts widget-->
                <div class="widget rposts">
                    <h3>Популярні</h3>
                    {%for pop in popular%}
                    <div class="rpost">
                        <div class="rpost-img">
                            <img src="/userfiles/{{pop.user_id}}/posts/{{pop.post_pic}}" alt="">
                        </div>
                        <div class="rpost-text">
                            <h4><a href="/pub/view/{{pop.post_id}}">{{pop.post_subj}}</a></h4>
                            <p>Автор публікації:{{pop.user_nick}}</p>
                            <p>Дата публікації: {{pop.post_date|date("d.m.Y")}}</p>
                        </div>
                    </div>
                    {%endfor%}
                </div>


                <!-- Links Widget-->
                <div class="widget links">
                    <h3>Категорії</h3>

                    <ul class="list-unstyled">
                        {%for c in category%}
                        <li>
                            <a name="cat" id="cat" href="/pub?cat={{c.cat_id}}">{{c.cat_name}}</a>
                        </li>
                        {%endfor%}
                    </ul>
                </div>
            </aside>
        </div><!--Sidebar Close-->
    </div>
</section><!--Single Post Content Close-->
{%endblock%}

{%block js%}
<script src="/js/comments.js"></script>
<script src="/js/favorites.js"></script>
<script src="/js/Share.js"></script>
<script src="/js/pub.js"></script>
{%endblock%}