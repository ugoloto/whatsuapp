{% extends skin~"/common/root.tpl" %}

{%block content%}
<!--Breadcrumbs-->
<div class="container space-top">
    <ol class="breadcrumb">
        <li><a href="index.tpl">Головна</a></li>
        <li class="active">Публікації</li>
        {#<li class="active">Blog List Sidebar Right</li>#}
    </ol>
</div><!--Breadcrumbs Close-->

<div class="container double-padding-bottom">
    <div class="row">
        <section class="col-md-8 list-view">

            {%for r in records%}
            <div class="row post">
                <div class="col-md-4 col-sm-4">
                    <input type="hidden" name="cat" id="cat" value="{{r.cat_id}}">
                    <a class="featured-img" href="/pub/view/{{r.post_id}}">
                        <img class="img-responsive" src="/userfiles/{{r.user_id}}/posts/{{r.post_pic}}" alt="Post01"/></a>
                </div>
                <div class="col-md-8 col-sm-8">
                    <h3><a href="/pub/view/{{r.post_id}}">{{r.post_subj}}</a></h3>
                    {#<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, quae sit architecto unde possimus voluptatem velit sed illum.</p>#}
                    <span class="devider"></span>
                    <div class="meta group">
                        <div class="left">
                            <a href="#">{{r.user_nick}}</a>
                        </div>
                        <div class="right"><span class="text-muted">{{r.post_date|date("d.m.Y")}}</span></div>
                    </div>
                </div>
            </div>

            {%endfor%}








            <ul class="pagination">

                {%if pagination.previous%} <a href="{{module}}?p={{pagination.previous}}{%if pcat%}&cat={{pcat}}{%endif%}" style="margin: 4px 0;"><i
                            class="fa fa-arrow-left"></i></a>{%endif%}
                {{pagination.info}}
                {%if pagination.next%}<a href="{{module}}?p={{pagination.next}}{%if pcat%}&cat={{pcat}}{%endif%}" style="margin: 6px 0 0 0;"><i
                            class="fa fa-arrow-right"></i></a>{%endif%}
            </ul>




        </section>

        <!--Sidebar-->
        <div class="col-md-4" id="sidebar">
            <aside class="sidebar right">

                <!--Recent/Featured posts widget-->
                <div class="widget rposts">
                    <h3>Популярні</h3>
                    {%for pop in popular%}
                    <div class="rpost">
                        <div class="rpost-img">
                            <img src="/userfiles/{{pop.user_id}}/posts/{{pop.post_pic}}" alt="">
                        </div>
                        <div class="rpost-text">
                            <h4><a href="/pub/view/{{pop.post_id}}">{{pop.post_subj}}</a></h4>
                            <p>Автор публікації:{{pop.user_nick}}</p>
                            <p>Дата публікації: {{pop.post_date|date("d.m.Y")}}</p>
                        </div>
                    </div>
                    {%endfor%}
                </div>


                <!-- Links Widget-->
                <div class="widget links">
                    <h3>Категорії</h3>

                    <ul class="list-unstyled">
                        {%for c in category%}
                        <li>
                          <a name="cat" id="cat" href="/pub?cat={{c.cat_id}}">{{c.cat_name}}</a>
                        </li>
                        {%endfor%}
                    </ul>
                </div>
            </aside>
        </div><!--Sidebar Close-->

    </div>
</div>

{%endblock%}

{%block js%}
<script src="/js/pub.js"></script>
{%endblock%}