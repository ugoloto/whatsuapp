{% extends skin~"/common/root.tpl" %}
{%block css%}
<style>
    .error {
        color: red;
    }
</style>
{%endblock%}

{% block content%}
<section class="specialty-page">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-12">
                <div class="page-heading">
                    <h2>РЕЄСТРАЦІЯ</h2>
                    {#<h3>ddd</h3>
                    <p>For a full list of our services including support and consultancy for start-up businesses please see<br />"Our Services" section of the website.</p>#}
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <form class="signup-form space-bottom" id="regForm">
                            <div class="form-group">
                                <label class="sr-only" for="username"></label>
                                <input type="email" class="form-control input-lg" name="username" id="username"
                                       placeholder="E-mail" required>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="password"></label>
                                <input type="password" class="form-control input-lg" name="pass" id="pass"
                                       placeholder="Пароль" required>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="password2"></label>
                                <input type="password" class="form-control input-lg" name="cpass" id="cpass"
                                       placeholder="Підтвердіть пароль" required>
                            </div>
                            <button class="btn btn-md btn-primary btn-center" type="submit">Зареєструватись</button>
                        </form>
                    </div>
                    <div class="col-md-6 col-sm-6 social-sugnup">
                        <a class="twitter" href="#">Twitter </a>
                        <a class="facebook" href="#">Facebook </a>
                        <a class="pinterest" href="#">Google Plus</a>
                    </div>
                </div>
                <p class="helper-text">Реєструючись ви згодні з нашими <a href="#"> умовами та правилами </a>
                    користування сайтом</p>
                <p>Вже зареєстровані? <a href="/login">Увійти</a></p>
            </div>
        </div>
    </div>
</section>
{% endblock%}

{%block js%}
<script src="/js/signup.js"></script>
{%endblock%}
