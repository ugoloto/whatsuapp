{% extends skin~"/common/root.tpl" %}
{%block css%}
<link href="/js/lib/crope/cropper.css" rel="stylesheet">
{%endblock%}


{% block content %}
<section class="page-block user-account double-space-bottom">
    <div class="container">
        <div class="row">

            {%include skin~"/profile/menu.tpl"%}


            <div class="col-md-10">
                <div class="tab-content">

                    <div class="tab-pane fade in active" id="notify">

                        <h3 class="heading center">Обрані публікації</h3>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 gray-bg">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Дата</th>
                                        <th>Тема</th>
                                        <th width="5%"></th>
                                    </tr>
                                    </thead>
                                    {%for r in records%}
                                    <tr>
                                        <td>{{r.fp_date|date("d.m.Y")}}</td>
                                        <td><a href="/pub/view/{{r.fp_post_id}}"><h3>{{r.post_subj}}</h3></td>
                                        <td>
                                            <a href="#" class="del" data-id="{{r.fp_id}}"><i
                                                        class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                    {%endfor%}
                                </table>
                            </div>


                        </div>

                        {%if pagination.previous%} <a href="{{module}}?p={{pagination.previous}}"
                                                      style="margin: 4px 0;"><i
                                    class="fa fa-arrow-left"></i></a>{%endif%}
                        {{pagination.info}}
                        {%if pagination.next%}<a href="{{module}}?p={{pagination.next}}" style="margin: 6px 0 0 0;"><i
                                    class="fa fa-arrow-right"></i></a>{%endif%}

                    </div>


                </div>
            </div>
        </div>


    </div>
</section>

{%endblock%}

{%block js%}


<script src="/js/lib/plupload-2.1.9/plupload.full.min.js"></script>
<script src="/js/lib/bootbox.min.js"></script>
<script src="/js/lib/crope/cropper.js"></script>
<script src="/js/favorites.js"></script>

{%endblock%}
