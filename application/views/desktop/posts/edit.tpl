{% extends skin~"/common/root.tpl" %}
{%block css%}
<link href="/js/lib/crope/cropper.css" rel="stylesheet">
{%endblock%}


{% block content %}
<section class="page-block user-account double-space-bottom">
    <div class="container">
        <div class="row">

            {%include skin~"/profile/menu.tpl"%}


            <!--Content-->
            <div class="col-md-9 gray-bg">
                <div class="tab-content">


                    <!--Notifications-->

                    <div class="tab-pane fade in active" id="notify">
                        <h3 class="heading center">Публікації</h3>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">


                                <form id="postsEdit" enctype="multipart/form-data">
                                    <input type="hidden" name="post_id" value="{{post.post_id}}">
                                    <div class="form-group">
                                        <label for="af-first-name">Статус </label>
                                        <select class="control-form" name="status">
                                            <option value="0">Не Опубликован</option>
                                            <option {%if post.post_status==1%}selected{%endif%} value="1">Опубликован
                                            </option>
                                        </select>

                                    </div>

                                    <input type="hidden" name="fname" id="img-file">
                                    <div class="form-group">
                                        <div id="containerpic">
                                            <img src="/userfiles/{{acs.user_id}}/posts/{{post.post_pic}}">
                                            <img class="img_responsive" id="preview" src="/img/nopic.jpg"
                                                 style="display:none;">
                                            <button id="pickfiles" class="btn btn-primary">Редагувати зображення
                                            </button>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="af-country">Категорія</label>
                                        <div class="select-style">
                                            <select name="category" id="category">
                                                {%for c in clist%}
                                                <option {%if c.cat_id==post.post_cat%}selected{%endif%} value="{{c.cat_id}}">{{c.cat_name}}</option>
                                                {%endfor%}
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="af-first-name">Коротко </label>
                                        <textarea class="form-control" name="subj"
                                                  cols="5">{{post.post_subj}}</textarea>

                                    </div>
                                    <div class="form-group">
                                        <label>Пост</label>
                                        <textarea class="form-control" name="post"
                                                  cols="50">{{post.post_text}}</textarea>
                                    </div>

                                    <a href="/posts" class="btn btn-md btn-default2" type="reset">Скасувати</a>
                                    <button class="btn btn-md btn-primary pull-right" id="doSave" type="submit">
                                        Обновити
                                    </button>
                                </form>

                            </div>


                        </div>


                    </div>


                </div>
            </div>
        </div>
    </div>
</section>
{%endblock%}

{%block js%}


<script src="/js/lib/plupload-2.1.9/plupload.full.min.js"></script>
<script src="/js/lib/bootbox.min.js"></script>
<script src="/js/lib/crope/cropper.js"></script>
<script src="/js/post.js"></script>

{%endblock%}
