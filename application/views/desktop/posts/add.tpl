{% extends skin~"/common/root.tpl" %}
{%block css%}

{%endblock%}


{% block content %}
<section class="page-block user-account double-space-bottom">
    <div class="container">
        <div class="row">

            {%include skin~"/profile/menu.tpl"%}


            <!--Content-->
            <div class="col-md-9 gray-bg">
                <div class="tab-content">


                    <!--Notifications-->

                    <div class="tab-pane fade in active" id="notify">
                        <h3 class="heading center">Публікації</h3>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">


                                <form id="posts" enctype="multipart/form-data">
                                    <input type="hidden" name="fname" id="img-file">
                                    <div class="form-group">
                                        <div id="containerpic">
                                            <img class="img_responsive" id="preview" src="/img/nopic.jpg"
                                                 style="display:none;">
                                            <button id="pickfiles" class="btn btn-primary">Завантажити фото</button>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="af-country">Категорія</label>
                                        <div class="select-style">
                                            <select name="category" id="category">
                                                {%for c in clist%}
                                                <option  value="{{c.cat_id}}">{{c.cat_name}}</option>
                                                {%endfor%}
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="af-first-name">Коротко </label>
                                        <textarea class="form-control" name="subj" cols="5"></textarea>

                                    </div>
                                    <div class="form-group">
                                        <label>Пост</label>
                                        <textarea class="form-control" name="post" cols="50"></textarea>
                                    </div>

                                    <a href="/posts" class="btn btn-md btn-default2" type="reset">Скасувати</a>
                                    <button class="btn btn-md btn-primary pull-right" id="doSave" type="submit">Додати
                                    </button>
                                </form>

                            </div>


                        </div>


                    </div>


                </div>
            </div>
        </div>
    </div>
</section>
{%endblock%}

{%block js%}


<script src="/js/lib/plupload-2.1.9/plupload.full.min.js"></script>
<script src="/js/lib/bootbox.min.js"></script>

<script src="/js/lib/jquery-form.js"></script>
<script src="/js/post.js"></script>


{%endblock%}
