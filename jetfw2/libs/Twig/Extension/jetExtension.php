<?php
namespace jet\libs\Twig\Extension;

class jetExtension extends \Twig_Extension_Core
{
  public function getFilters() {
    return array_merge(
      parent::getFilters(),
      array(       
        'youtube_video_id' => new \Twig_Filter_Method($this, 'youtube_video_id'),
        'preview_path' => new \Twig_Filter_Method($this, 'preview_path'),
        'truncate' => new \Twig_Filter_Method($this, 'truncate'),
        'wordwraps' => new \Twig_Filter_Method($this, 'wordwraps'),
        'inArray' => new \Twig_Filter_Method($this, 'inArray'),
        'rusd' => new \Twig_Filter_Method($this, 'rusd'),
        'mailto' => new \Twig_Filter_Method($this,'mailto'),
        'addsl' => new \Twig_Filter_Method($this,'addsl'),
        'dquotos' => new \Twig_Filter_Method($this,'dquotos')    ,
        'socialType' => new \Twig_Filter_Method($this,'socialType')    ,                   
        'videoTimeShortcuts' => new \Twig_Filter_Method($this,'videoTimeShortcuts'),
        'size' => new \Twig_Filter_Method($this,'size'),
        'pr' => new \Twig_Filter_Method($this,'pr'),
        'json' => new \Twig_Filter_Method($this,'json'),                  
      //  'number_format' => new \Twig_Filter_Method($this,'number_format'),     
        'asterify' => new \Twig_Filter_Method($this, 'asterify'),
        'creditCardEscape' => new \Twig_Filter_Method($this, 'creditCardEscape'),
        'trims' => new \Twig_Filter_Method($this, 'trims'),
        'clean_num' => new \Twig_Filter_Method($this, 'clean_num')
      ) 
    );
  }   
  public function trims($string = "", $sym = '\s') {
      return preg_replace("/[".$sym."]*?/","",$string);
  }
  public function asterify($string = "") {
      return str_repeat('*',strlen($string));
  }
  
  public function creditCardEscape($string = "") {
      return "XXXX-XXXX-XXXX-".substr($string, -4); 
  }
  public function socialType($type) {
      switch(strtolower($type)) {
          case 'twitter':
            return "Twitter";
          case 'facebook':
            return "Facebook";
          case 'vkontakte':
            return "ВКонтакте";
          case 'icq':
            return "ICQ";
          case 'skype':
            return "Skype";
          case 'livejournal':
            return "LiveJournal";
          default:
            return "Неизвестно";
      }
  } 
  public function videoTimeShortcuts($cont,$url = '') {
    $m = array();  
    preg_match_all('/(([\d]{1,2}):([\d]{2}))|(([\d]{1,2})мин[\.\s]*?([\d]{2})сек[\.]{0,1})/i',$cont,$m, PREG_SET_ORDER);
    if (count($m) == 0) return $cont;
    $t = preg_replace('/(([\d]{1,2}):([\d]{2}))|(([\d]{1,2})мин[\.\s]*?([\d]{2})сек[\.]{0,1})/i',"%s",$cont);   
                       
    $args = array();
    foreach ($m as $i) {
        if ($i[1]) {
            $secs = intval($i[2],10)*60 + intval($i[3],10);     
        }
        elseif ($i[4]) {
            $secs = intval($i[5],10)*60 + intval($i[6],10);     
        }     
        array_push($args,'<a href="'.$url.'#time/'.$secs.'" class="video-time" rel="nofollow">'.$i[0].'</a>');       
    }        
    $t = vsprintf($t,$args); 
    return $t;
  } 
  public function json($value)  {
    if (empty($value)) return "null";
    return \jet\json::encode($value, true);
  }
  public function addsl($value)  {
    return addslashes($value);
  } 
  public function dquotos($value)  {
    return str_ireplace('"',"'",$value);
  } 

  public function isonline($value,$class = 'user_online',$text = '') {
      if ((strtotime('now') - strtotime($value)) < 60*10) return '<span class="'. $class .'">'.$text.'</span>';                         
      else return '';
  } 
  public function youtube_video_id($string)
  {
    $m = array();
    preg_match("/v=(.*)(&|$)/",$string,$m);      
    if (count($m)>1)
        return $m[1];
    else return $string;
  }   
  function inArray($string,$array = array())
  {    
    return in_array($string,$array);
  }    
  function truncate($string, $length = 80, $etc = '...', $break_words = false, $middle = false)
  {
        if ($length == 0)
            return '';

        if (strlen($string) > $length) {
            $length -= min($length, strlen($etc));
            if (!$break_words && !$middle) {
                $string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $length+1));
            }
            if(!$middle) {
                return substr($string, 0, $length) . $etc;
            } else {
                return substr($string, 0, $length/2) . $etc . substr($string, -$length/2);
            }
        } else {
            return $string;
        }
  } 
  
  function wordwraps($string, $length = 80){
	  
        if ($length == 0)
            return '';

	   return  wordwrap ( $string, $length, "<br>",true);
	  
  }
  
  
  function rusd($date) {
    if ( substr_count($date , '---') > 0 ) return str_replace('---', '', $date);

    $treplace = array (
        "Январь" => "января",
        "Февраль" => "февраля",
        "Март" => "марта",
        "Апрель" => "апреля",
        "Май" => "мая",
        "Июнь" => "июня",
        "Июль" => "июля",
        "Август" => "августа",
        "Сентябрь" => "сентября",
        "Октябрь" => "октября",
        "Ноябрь" => "ноября",
        "Декабрь" => "декабря",
        
        "January" => "января",
        "February" => "февраля",
        "March" => "марта",
        "April" => "апреля",
        "May" => "мая",
        "June" => "июня",
        "July" => "июля",
        "August" => "августа",
        "September" => "сентября",
        "October" => "октября",
        "November" => "ноября",
        "December" => "декабря",    
        
        "Jan" => "янв.",
        "Feb" => "фев.",
        "Mar" => "марта",
        "Apr" => "апр.",
        "May" => "мая",
        "Jun" => "июня",
        "Jul" => "июля",
        "Aug" => "авг.",
        "Sep" => "сен.",
        "Oct" => "окт.",
        "Nov" => "нояб.",
        "Dec" => "дек.",
        
        "Sunday" => "воскресенье",
        "Monday" => "понедельник",
        "Tuesday" => "вторник",
        "Wednesday" => "среда",
        "Thursday" => "четверг",
        "Friday" => "пятница",
        "Saturday" => "суббота",
        
        "Sun" => "Вс.",
        "Mon" => "Пн.",
        "Tue" => "Вт.",
        "Wed" => "Ср.",
        "Thu" => "Чт.",
        "Fri" => "Пт.",
        "Sat" => "Сб.",
        
        "th" => "",
        "st" => "",
        "nd" => "",
        "rd" => ""   
    );
    return strtr($date, $treplace);
  }   
  public function mailto($address, $encode) {
    $extra = '';
    $params['address'] = $address;
    $params['encode'] = $encode;
    if (empty($params['address'])) {
        
        return;
    } else {
        $address = $params['address'];
    }

    $text = $address;

    // netscape and mozilla do not decode %40 (@) in BCC field (bug?)
    // so, don't encode it.
    $search = array('%40', '%2C');
    $replace  = array('@', ',');
    $mail_parms = array();
    foreach ($params as $var=>$value) {
        switch ($var) {
            case 'cc':
            case 'bcc':
            case 'followupto':
                if (!empty($value))
                    $mail_parms[] = $var.'='.str_replace($search,$replace,rawurlencode($value));
                break;
                
            case 'subject':
            case 'newsgroups':
                $mail_parms[] = $var.'='.rawurlencode($value);
                break;

            case 'extra':
            case 'text':
                $$var = $value;

            default:
        }
    }

    $mail_parm_vals = '';
    for ($i=0; $i<count($mail_parms); $i++) {
        $mail_parm_vals .= (0==$i) ? '?' : '&';
        $mail_parm_vals .= $mail_parms[$i];
    }
    $address .= $mail_parm_vals;

    $encode = (empty($params['encode'])) ? 'none' : $params['encode'];
    if (!in_array($encode,array('javascript','javascript_charcode','hex','none')) ) {
        
        return;
    }

    if ($encode == 'javascript' ) {
        $string = 'document.write(\'<a href="mailto:'.$address.'" '.$extra.'>'.$text.'</a>\');';

        $js_encode = '';
        for ($x=0; $x < strlen($string); $x++) {
            $js_encode .= '%' . bin2hex($string[$x]);
        }

        return '<script type="text/javascript">eval(unescape(\''.$js_encode.'\'))</script>';

    } elseif ($encode == 'javascript_charcode' ) {
        $string = '<a href="mailto:'.$address.'" '.$extra.'>'.$text.'</a>';

        for($x = 0, $y = strlen($string); $x < $y; $x++ ) {
            $ord[] = ord($string[$x]);   
        }

        $_ret = "<script type=\"text/javascript\" language=\"javascript\">\n";
        $_ret .= "<!--\n";
        $_ret .= "{document.write(String.fromCharCode(";
        $_ret .= implode(',',$ord);
        $_ret .= "))";
        $_ret .= "}\n";
        $_ret .= "//-->\n";
        $_ret .= "</script>\n";
        
        return $_ret;
        
        
    } elseif ($encode == 'hex') {

        preg_match('!^(.*)(\?.*)$!',$address,$match);
        if(!empty($match[2])) {
            
            return;
        }
        $address_encode = '';
        for ($x=0; $x < strlen($address); $x++) {
            if(preg_match('!\w!',$address[$x])) {
                $address_encode .= '%' . bin2hex($address[$x]);
            } else {
                $address_encode .= $address[$x];
            }
        }
        $text_encode = '';
        for ($x=0; $x < strlen($text); $x++) {
            $text_encode .= '&#x' . bin2hex($text[$x]).';';
        }

        $mailto = "&#109;&#97;&#105;&#108;&#116;&#111;&#58;";
        return '<a href="'.$mailto.$address_encode.'" '.$extra.'>'.$text_encode.'</a>';

    } else {
        // no encoding
        return '<a href="mailto:'.$address.'" '.$extra.'>'.$text.'</a>';

    }
  } 
  public function pr($array) {
    $a = print_r($array, true);
    $b = '<pre>' . $a . '</pre>';
    return $b;
  } 
  public function size($array) {
    return sizeof($array);
  }
  /*public function number_format($number, $length = 0) {
    return '<nobr>' . number_format($number, $length, '.', ',') . '</nobr>';
  }*/
  public function clean_num( $num="" ){
  	$pos = strpos($num, '.');
	if($pos === false) { // it is integer number
			return $num;
	}else{ // it is decimal number
			return rtrim(rtrim($num, '0'), '.');
	}
  }

  
  
}
