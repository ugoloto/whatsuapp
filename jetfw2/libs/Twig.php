<?php    
namespace jet\libs;

class Twig {
    
    public static function load() {
        require_once 'Twig/Autoloader.php';
        \Twig_Autoloader::register();    
    }
    
}
