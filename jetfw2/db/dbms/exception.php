<?php
namespace jet\db\dbms;

class exception extends \jet\exception {
    
    public function __construct($message = "Unknown", $dbms, $level = E_USER_ERROR) {  
        $details['dbms_error'] = $message;        
        $details['last_query'] = $dbms->getLastQuery();   
        $class = get_class($dbms);
        $class_parts = explode('\\',$class);
        array_shift($class_parts);   
        $class = implode(".",$class_parts);
        $class  = strtoupper($class);    
        $trace = $this->getTrace();
        $r = array();
        $i = array_shift($trace);
        if ($i) $r = $i;
        $i = array_shift($trace);
        if ($i) $r = $i;
        $i = array_shift($trace);
        if ($i) $r = $i;
        $details['line'] = $r['line'];
        $details['file'] = $r['file'];
        $i = array_shift($trace);
        if ($i) $r = $i;
        $details['class']           =   $r['class'];
        $details['class_function']  =   $r['function'];
        $details['class_line']      =   $r['line'];
        $details['class_file']      =   $r['file'];   
        $details['last_result']     =   $dbms->getLastResult();         
          
        parent::__construct($class." in ".$details['class']."->".$details['class_function']."()", $level, $details);
    }
    
}