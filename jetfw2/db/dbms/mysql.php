<?php
namespace jet\db\dbms;

class mysql extends \jet\db\dbms {
    protected $link;        
    protected $autocommit_state = null;
    
    protected $debug;
    
    public function __construct($connect, $debug = false)
    {
        $this->debug = $debug;   
        if (!isset($connect['port'])) $port = '';
        else $port = ':'.$connect['port'];   
         
        $this->link = \mysqli_connect($connect['host'].$port, $connect['user'], $connect['password'],$connect['database']);    
        if (!$this->link)
        {
            throw new exception(mysqli_error($this->link), $this); 
        }   
        
    }      
    
    public function query($query = NULL, $type = null)
    {
       if ($this->debug) {
         \jet\log::write($query, 'mysql');
       }   
        if ($mResult = \mysqli_query($this->link,$query ))
        {
            
            switch($type) {
                case 'one':
                    $line = \mysqli_fetch_array($mResult, MYSQLI_ASSOC);
                    if ($line) {
                        foreach ($line as $key=>$value)
                        {
                            $result = $value;
                            \mysqli_free_result($mResult);
                            return $value;
                        }
                    }
                    else return NULL;
                break; 
                case 'row':
                    $result=array();
                    $line = \mysqli_fetch_array($mResult, MYSQLI_ASSOC);
                    if (!empty($line))
                    {
                      foreach ($line as $key=>$value)
                      {
                          $result[$key]=$value;
                      }
                      \mysqli_free_result($mResult);
                      return $result;
                    }
                    return NULL;
                break; 
                default:
                    if ($mResult == 'TRUE') return true;
                    $result = array();
                    while ($line = \mysqli_fetch_array($mResult, MYSQLI_ASSOC))
                    {
                        foreach ($line as $key=>$value)
                        {
                            $l[$key]=$value;
                        }
                        array_push($result,$l);
                    }
                    \mysqli_free_result($mResult);
                    return $result;
                break;
            }

        }
        else throw new exception(mysqli_error($this->link), $this);
    }            
    public function startTransaction() {
        $this->autocommit_state = $this->autocommit();
        $this->autocommit(false);
        $this->query("START TRANSACTION");    
    }
    public function commit() {
        $this->query("COMMIT");
        if ($this->autocommit_state !== null) {
            $this->autocommit($this->autocommit_state);
        }
    }
    
    public function rollback() {
        $this->query("ROLLBACK");
        if ($this->autocommit_state !== null) {
            $this->autocommit($this->autocommit_state);
        }
    }
    
    public function autocommit($enable = -1) {
        if ($enable === -1) {
            return (intval($this->query('SELECT @@AUTOCOMMIT','one')))?true:false;
        }
        else {
            $this->query("set AUTOCOMMIT = ".(($enable)?"1":"0"));
        }
    }
    
    public function __destruct()
    {                     
        \mysqli_close($this->link);   
    }     
           
    public function setCharset($names = 'utf8') {
        if (!\mysqli_query($this->link,'SET NAMES '.$names)) throw new exception(mysqli_error($this->link), $this);
    } 
    
    public function getCharset($names = 'utf8') {
        return 'TODO';
    }
    
    public function getAffectedRows() {
        return mysqli_affected_rows();
    }
    
    public function getLastID() {
        return mysqli_insert_id($this->link);
    }       
    
    public function getListTables($prefix = null){
      $ret = array();
      $r = mysqli_query($this->link,"SHOW TABLES");
      if (!$r) throw new exception(mysqli_error($this->link), $this);
      if (mysqli_num_rows($r)>0) {
        while($row = mysqli_fetch_array($r, mysqli_NUM)) {
          if ($prefix === null || $prefix == substr($row[0], 0, strlen($prefix))) 
            $ret[] = $row[0];
        }
      }
      return $ret;
    }  
    
    public function getLink(){
	    
	    return $this->link;
	    
    }

}
