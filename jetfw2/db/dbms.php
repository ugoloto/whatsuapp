<?php
namespace jet\db;

abstract class dbms {
    
    private $last_query = "";
    
    private $last_result = "";
    
    abstract public function __construct($connect, $debug = false);
    abstract public function getAffectedRows();   
    abstract public function getLastID();
    abstract public function query($query = NULL, $type = NULL);
    abstract public function startTransaction();
    abstract public function rollback();
    abstract public function commit();
    abstract public function getCharset();    
    abstract public function setCharset($carset);    
    abstract public function getListTables($prefix = null);    
    
    public final function setTrace($last_query, $last_result) {
        $this->last_query = $last_query;    
        $this->last_result = $last_result;
    }    
    public final function getLastQuery() {
        return $this->last_query;
    }
    public final function getLastResult() {
        return $this->last_result;
    }
}
