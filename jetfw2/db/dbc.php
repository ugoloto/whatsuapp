<?php
namespace jet\db;

class dbc {
    private $dbms;
    
    private $dbms_name;   
    
    private $charset;  
    
    public function __construct($connect, $dbms_name = "mysql", $settings = array(), $debug = false) {
        $this->dbms_name = $dbms_name;
        $dbms_class = "jet\\db\\dbms\\".$this->dbms_name;     
        $this->dbms = new $dbms_class($connect, $debug);
        if (isset($settings['charset'])) {
            $this->setCharset($settings['charset']);
        }
    }         
    public function __destruct() {
  
    }
    public function getDBMS() {
        return $this->dbms_name;
    }
    
    public function getAffectedRows() {
        return $this->dbms->getAffectedRows();
    }
    
    public function getCharset(){
        return $this->charset;
    }
    
    public function setCharset($set){
        $this->dbms->setCharset($set);
        $this->charset = $set;
    }  
    
    public function getLastID(){
        return $this->dbms->getLastID();
    }  
    
    public function listTables($prefix = null){
      return $this->dbms->getListTables($prefix);
    }
    
    public function getLastQuery() {
        return $this->dbms->getLastQuery();
    }
    
    public function q($query) {
        return $this->query($query);                     
    }
    public function q1($query) {
        return $this->queryOne($query);                     
    }
    public function q_($query) {
        return $this->queryRow($query);                     
    }
                                 
    public function query($query = NULL, $type = null){  
        $this->dbms->setTrace($query, null);          
        $result = $this->dbms->query($query, $type);     
        $this->dbms->setTrace($query, $result);
        return $result;
    }
    
    public function queryOne($query = NULL){
        return $this->query($query, 'one'); 
    }
    
    public function queryRow($query = NULL){
        return $this->query($query, 'row');                                  
    }
    
    public function startTransaction() {
        return $this->dbms->startTransaction();                                  
    }
    
    public function rollback() {
        return $this->dbms->rollback();                                  
    }
    
    public function commit() {
        return $this->dbms->commit();                                  
    }
    
    public function autocommit($enable = -1) {
        return $this->dbms->autocommit($enable);                                  
    }

}