<?php
namespace jet;
/********************************
****[   Developed by Jet    ]****
*********************************
    class: Validator
  version: v1.1.0.0
  website: http://sojet.ru
********************************/ 
/*
definition example:
    $form=new \jet\validator($_POST);

access to field:
    $form->form_field_name;
            or
    $form->fields['form_field_name'];
    $form->form_field_name->value - value of this field.
    $form->form_field_name - string in this field

validation example:
    $form->form_field_name->email();
                or
    $form->fields['form_field_name']->email();
                or
    $form->form_field_name->validate(array('email'=>true,'range'=>'3,9','required'=>true,'>='=>3)).

validator methods:
    ->isValid() - returns true if all fields are valid;
    ->getErrors() - returns a array of errors occured;
    ->echoErrors() - prints a array of errors (<br> separated).
    ->printErrors() - returns a string of errors (<br> separated).

field methods:
    ->length(value, [=,>,<,>=,<=,!=]);     - length of string [=,>,<,>=,<=,!=] of value   ('=' by default);
    ->boolean(value, [=,>,<,>=,<=,!=]); - numeric value of field [=,>,<,>=,<=,!=] of value  ('=' by default);
    ->range(value1,value2);             - length of string is between value1 and value2;
    ->login();                            - latinic symbols,0-9,-._;
    ->email();                             - xx@xx.xx;
    ->required();                         - field is set or not string of spaces ("   ");
    ->number();                         - field is number;
    ->confirm(fieldname);                 - check if value of field is same as [fieldname] value;
    ->price();                             - Price (number with point and two digits after point) (example 131.32 or 121.00 or 121)
    ->zip();
    ->phone();
    ->isbn();
    ->ssn();
    ->cc();

    (All of them returns false if validation failed and true if validation succeed)

*/
class validator {
    public $fields;
    public $errors;
    function __get($nm)
    {
        if (!isset($this->fields[$nm])) $this->fields[$nm] = new validatorField($nm, null, $this);
        return $this->fields[$nm];
    }     
    function __set($nm, $val)
    {
        if (!isset($this->fields[$nm])) $this->fields[$nm] = new validatorField($nm, $val, $this);
        else $this->fields[$nm] = $val;
    }    
    function __construct($data)
    {
        $this->fields=array();
        $this->errors=array();
        foreach ($data as $key=>$value)
        {
            $this->fields[$key] = new validatorField($key,stripslashes($value),$this);
        }    
    }      
    function isValid()
    {
        if (count($this->errors)>0) return false;
        else return true;
    }     
    function printErrors()
    {
        $r="";
        foreach ($this->errors as $value)
        {
            $r.= $value."<BR>";
        };
        return $r;
    }
    function echoErrors()
    {
        foreach ($this->errors as $value)
        {
            echo $value."<BR>";
        };
    }    
    function getErrors()
    {
        return $this->errors;
    }
}

class validatorField {
    public $name;
    public $value;
    public $base;
    function __construct($name, $value, $base)
    {
        $this->name  = $name;
        $this->value = $value;
        $this->base  = $base;
    }
    function __toString() {
        return $this->value;
    }
    function iset() {
        return !empty($this->value);
    }
    function validate($v = array())
    {
        $valid = true;
        foreach ($v as $key => $value)
        {
            switch ($key) {
                case 'required':
                    if ($value == true) $valid = $this->required();
                    break;
                case 'number':
                    if ($value == true) $valid = $this->number();
                    break;
                case 'range':
                    $r = explode(',',$value);
                    $valid = $this->range(floatval($r[0]),floatval($r[1]));
                    break;
                case 'confirm':
                    $valid = $this->confirm($field);
                case 'email':
                    $valid=$this->email();
                    break;
                case 'ip':
                    $valid=$this->ip();
                    break;
                case 'url':
                    $valid=$this->url();
                    break;
                case 'date':
                    $valid=$this->date();
                    break;
                case 'zip':
                    $valid=$this->zip();
                    break;
                case 'isbn':
                    $valid=$this->isbn();
                    break;
                case 'ssn':
                    $valid=$this->ssn();
                    break;
                case 'cc':
                    $valid=$this->cc();
                    break;
                case 'hexColor':
                    $valid=$this->hexColor();
                    break;
                case 'login':
                    $valid=$this->login();
                    break;
                case 'phone':
                    $valid=$this->phone();
                    break;
                case 'captcha':
                    $valid=$this->captcha();
                    break;
                default:
                    $valid=$this->length($value,$key);
                    break;
            }
        }
        return $valid;
    }
    function captcha()
    {
        if ($_SESSION['captcha'] == $this->value) return true;
        array_push($this->base->errors,"Incorrect code");
          return false;
    }
    function required($error = null)
    {
        if ($error === null) $error = "Field $this->name is required";
        $r=str_replace(" ","",$this->value);
          if (isset($this->value)&&$r != "")  return true;
          array_push($this->base->errors,$error);
          return false;
    }                            
    function email($error = "Incorrect email")
    {                            
          if (preg_match('/^[a-zA-Z0-9\._-]+@[^\.]{1}[a-zA-Z0-9\._-]+\.[a-zA-Z]{2,4}$/',$this->value)&&$this->value)  return true;
        array_push($this->base->errors,$error);
          return false;
    }
    function ip()
    {
          if (preg_match('/^([0-9]{1,3}\.){3}[0-9]{1,3}$/',$this->value))  return true;
          array_push($this->base->errors,"Incorrect ip-address");
          return false;
    }
    function price()
    {
          if (preg_match('/^[0-9]+(\.[0-9]{1,2})?$/',$this->value))  return true;
          array_push($this->base->errors,"Incorrect ip-address");
          return false;
    }
    function url()
    {
        if (preg_match('/^[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,4}$/',$this->value))  return true;
        array_push($this->base->errors,"Incorrect URL");
          return false;
    }
    function date()
    {
        if (preg_match('/^([0-9][0-2]|[0-9])\/([0-2][0-9]|3[01]|[0-9])\/[0-9]{4}|([0-9][0-2]|[0-9])-([0-2][0-9]|3[01]|[0-9])-[0-9]{4}$/',$this->value))  return true;
        array_push($this->base->errors,"Incorrect date format");
          return false;
    }
    function zip()
    {
        if (preg_match('/^[0-9]{5}(-[0-9]{4})?$/',$this->value))  return true;
        array_push($this->base->errors,"Incorrect zip-code");
          return false;
    }
    function hexColor()
    {
        if (preg_match('/^#?([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?$/',$this->value))  return true;
        array_push($this->base->errors,"Incorrect hex format of color");
          return false;
    }
    function ssn()
    {
        if (preg_match('/^[0-9]{3}[- ][0-9]{2}[- ][0-9]{4}|[0-9]{9}$/',$this->value))  return true;
        array_push($this->base->errors,"Incorrect SSN");
          return false;
    }
    function cc()
    {
        if (preg_match('/^([0-9]{4}[- ]){3}[0-9]{4}|[0-9]{16}$/',$this->value))  return true;
        array_push($this->base->errors,"Incorrect CC");
          return false;
    }
    function isbn()
    {
        if (preg_match('/^[0-9]{9}[[0-9]|X|x]$/',$this->value))  return true;
        array_push($this->base->errors,"Incorrect ISBN");
          return false;
    }
    function phone()
    {
        if (preg_match('/^((\([0-9]{3}\) ?)|([0-9]{3}-))?[0-9]{3}-[0-9]{4}$/',$this->value))  return true;
        array_push($this->base->errors,"Incorrect format of phone number");
          return false;
    }
    function number()
    {
        if (is_numeric($this->value)) return true;
        array_push($this->base->errors,"Field $this->name must be numeric");
          return false;
    }
    function length($val,$eq="=")
    {
        switch ($eq) {
            case "=":
                if (strlen($this->value) == $val) return true;
                array_push($this->base->errors,"Length of field $this->name must be equal to $val");
                break;
            case ">":
                if (strlen($this->value) > $val) return true;
                array_push($this->base->errors,"Length of field $this->name must be greater than $val");
                break;
            case "<":
                if (strlen($this->value) < $val) return true;
                array_push($this->base->errors,"Length of field $this->name must be lesser than $val");
                break;
            case ">=":
                if (strlen($this->value) >= $val) return true;
                array_push($this->base->errors,"Length of field $this->name must be greater or equal than $val");
                break;
            case "<=":
                if (strlen($this->value) <= $val) return true;
                array_push($this->base->errors,"Length of field $this->name must be lesser or equal than $val");
                break;
            case "!=":
                if (strlen($this->value) !=  $val) return true;
                array_push($this->base->errors,"Length of field $this->name must be not equal to $val");
                break;
        }
          return false;
    }
    function boolean($val,$eq = "=")
    {
        if (!is_numeric($this->value)) return false;
        switch ($eq) {
            case "=":
                if ($this->value == $val) return true;
                array_push($this->base->errors,"Value of field $this->name must be equal to $val");
                break;
            case ">":
                if ($this->value > $val) return true;
                array_push($this->base->errors,"Value of field $this->name must be greater than $val");
                break;
            case "<":
                if ($this->value < $val) return true;
                array_push($this->base->errors,"Value of field $this->name must be lesser than $val");
                break;
            case ">=":
                if ($this->value >= $val) return true;
                array_push($this->base->errors,"Value of field $this->name must be greater or equal than $val");
                break;
            case "<=":
                if ($this->value <= $val) return true;
                array_push($this->base->errors,"Value of field $this->name must be lesser or equal than $val");
                break;
            case "!=":
                if ($this->value != $val) return true;
                array_push($this->base->errors,"Value of field $this->name must be not equal to $val");
                break;
        }
          return false;
    }
    function range($val1,$val2)
    {
        if ((strlen($this->value)>=$val1)&&(strlen($this->value)<=$val2)) return true;
        array_push($this->base->errors,"Length of field $this->name must be between $val1 and $val2");
          return false;
    }
    function login()
    {
        if (preg_match( '/^[-\.\_0-9a-zA-Z]+$/',$this->value)) return true;
        array_push($this->base->errors,"Login can include latinic symbols, numbers, and this symbols:_.- only!");
          return false;
    }
    function confirm($field,$error = "Confirm field is incorrect")
    {
        if ($this->value == $this->base->fields[$field]->value) return true;
        array_push($this->base->errors,$error);
          return false;
    }
    function pr()
    {
        echo $this->value;
    }
}
