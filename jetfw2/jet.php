<?php            
namespace jet;   

ini_set('display_errors', 1);

/* Автолоад */
function autoload($class) { 
    $dir = dirname(__FILE__);    
    if (substr($class,0,3) != 'jet') 
        return false;  
                     
    if (!class_exists($class, false)) {
        $file = $dir . str_replace('\\', DIRECTORY_SEPARATOR, substr($class, 3)) . '.php';

             
        if (file_exists($file)) 
            require($file);
        else 
            return false;
    }
    
    return class_exists($class, false); 
}     

/* Зарегистрируем автолоад */
spl_autoload_register('\jet\autoload');     

/* Шорткат для слеша */          
define('_', DIRECTORY_SEPARATOR);  


 
/* ======================= */
/* Фундаментальные классы и функии */
/* ======================= */

/* Тип - Обьект */
class obj implements \Iterator{
    protected $array = array();  
    protected $readonly = false;
    public function __construct($array = array(), $extend = false, $readonly = false) {
        
        if (\is_array($array))
            $this->array = $array; 
            
        if (is_obj($array)) 
            $this->array = $array->getArray();     
             
        if (is_iterable($extend)) {  
            $this->extendWith($extend);   
        }
        
        $this->readonly = $readonly;
    } 
    function toArray() {
        return clone $this->array;
    }  
    function getArray() {
        return $this->array;        
    } 
    function get($i) {
        return $this->array[$i];    
    }
    function set($i, $v) {
        #if ($this->readonly) throw new Exception('Core.Obj.Readonly', array('instance_of' => get_class($this), 'attribute'=>$i, 'value'=>$v, 'action'=>'set'));  
        if ($this->readonly) throw new Exception('Core.Obj.Readonly');
        else $this->array[$i] = $v;               
    }
    function __get($i) {
        return $this->get($i);
    }
    function __set($i, $v) {  
        $this->set($i, $v);        
    }
    function __isset($i) {
        return isset($this->array[$i]);
    }
    function __unset($i) {
        #if ($this->readonly) throw new Exception('Core.Obj.Readonly', array('instance_of' => get_class($this), 'attribute'=>$i, 'action'=>'unset'));  
        if ($this->readonly) throw new Exception('Core.Obj.Readonly');  
        else unset($this->array[$i]);
    }
    function __toString() {
        return \print_r($this->array, true);
    }
    public function rewind()
    {
        \reset($this->array);
    }     
    public function current()
    {
        return \current($this->array);
    }    
    public function key() 
    {
        return \key($this->array);
    }  
    public function next() 
    {
        return \next($this->array);
    }    
    public function valid()
    {
        $key = \key($this->array);
        return ($key !== NULL && $key !== FALSE);
    }    
    function extendWith($mixed) {   
        $array = extend($this->array, $mixed);
        
        if (\is_array($array))
            $this->array = $array; 
            
        if (is_obj($array)) 
            $this->array = $array->getArray(); 
                
        return $this;
    }
    function toJSON() {
        return json::encode($this->array);
    }
}

function is_obj($mixed) {
    return ($mixed instanceof  obj);    
}

function is_iterable($mixed) {
    return (\is_array($mixed) || is_obj($mixed));    

}  

/* То же самое что jQuery.extend */
function extend($array, $mixed, $include_empty = true) { 
 
    if (!is_iterable($mixed)) 
        return $mixed;   
               
    if (!is_iterable($array) && is_iterable($mixed)) 
        $array = array();   
        
    foreach ($mixed as $key => $value) {  
         if (!$include_empty && empty($value)) continue;
         if (is_iterable($value)) {    
             if (!isset($array[$key]))       
                $array[$key] = array();                 
             if (!is_iterable($array[$key])) 
                $array[$key] = array();   
             $array[$key] = extend($array[$key], $value);
         }
         else 
            $array[$key] = $value;  
     } 
     
     return $array;
}


function filter($array, $mixed, $include_empty = false, $recursive = true) {
    
    if (is_iterable($array) && !is_iterable($mixed)) 
        return $array;   
               
    if (!is_iterable($array) && is_iterable($mixed)) 
        return $array;
        
    foreach ($array as $key => $value) {  
         if (!$include_empty && empty($mixed[$key])) continue;
         
         if (isset($mixed[$key]) && is_iterable($mixed[$key])) {                         
             if ($recursive)
                $array[$key] = filter($array[$key], $mixed[$key],$include_empty,$recursive);
             else 
                $array[$key] = $mixed[$key];
         }
         else if (isset($mixed[$key])) 
            $array[$key] = $mixed[$key];  
     } 
     
     return $array;    
}

function mapping($array, $mapping, $back = false) {
    $n = array();
    foreach($mapping as $key => $value) {
        if (!$back) {
            if (isset($array[$key])) {
                $n[$value] = $array[$key];    
            }
            else {
                $n[$value] = "";    
            }
        }
        else {
            if (isset($array[$value])) {
                $n[$key] = $array[$value];    
            }
            else {
                $n[$key] = "";        
            }
        }
    }  
    return $n;     
}

/* post object */
function post($array = array()) {
    return httpPost($array);
}
function httpPost($array = array()) {
    return new obj($array, $_POST, true);
}
function httpGet($array = array()) {
    return new obj($array, $_GET, true);
}
/* Redirect */
function redirect($url = '/', $history = false) {
    if ($history) {           
        $_SESSION['jet_redirect_history'] = $_SERVER['REQUEST_URI'];
    }
    else if (isset($_SESSION['jet_redirect_history'])) {
        unset($_SESSION['jet_redirect_history']);
    }
    header("Location: ".$url);  
    die();   
}

  function clientGet($url, $data = array()) {
		if($data)$url = $url.'?'.\http_build_query($data, '', '&');
		//self::redirect($url, false);
		$this->redirect($url, false);
}

function redirect_back($default = '/') {
    if (isset($_SESSION['jet_redirect_history'])) {
        header("Location: ".$_SESSION['jet_redirect_history']);
        session_unregister('jet_redirect_history');
    }    
    else {
        header("Location: ".$default);       
    }
    die();   
}

function redirect_get_history() {
    if (isset($_SESSION['jet_redirect_history'])) {
        return $_SESSION['jet_redirect_history'];
    }
    else {
        return null;
    }
}

/* Валидация по перечню допустимых значений */
function validate($value, $allowed = array(), $default = false) {
    if (!isset($value)) return $default;
    if (in_array($value,$allowed)) return $value;
    else return  $default;      
}


function getClientIp()
{
     if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
     {
       $ip=$_SERVER['HTTP_CLIENT_IP'];
     }
     elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
     {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
     }
     else
     {
       $ip=$_SERVER['REMOTE_ADDR'];
     }
     return $ip;
}