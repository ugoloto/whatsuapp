<?php    
namespace jet\controller; 
use \jet\application as app;

/* Класс CLI контроллера */
class cli extends \jet\controller{
    protected $view;

    public function __construct()
    {
        parent::__construct();

        if (php_sapi_name() != 'cli') {
            header("HTTP/1.0 404 Not Found");
            die();

        }

        public
        function __before()
        {

        }

        public
        function __default()
        {

        }

        public
        function __error($e)
        {
            die($e);
        }

        public
        function __after()
        {

        }

    }
