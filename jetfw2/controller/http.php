<?php    
namespace jet\controller; 
use \jet\application as app;
use \jet\view\twig as View;   

/* Класс HTTP контроллера */
class http extends \jet\controller {    
    protected $view;
    
    public function __construct() {          
        parent::__construct();

        if (app::$settings->content_type)  \header(app::$settings->content_type);    
        
        if (app::$settings->use_session) {
            
            if (app::$settings->session_name) 
                \session_name(app::$settings->session_name);  
                          
            if (app::$settings->session_id) {
                \session_id(app::$settings->session_id);                           
            }          
            
            \session_set_cookie_params(time()+60*60*24*30);
            
            \session_start();  
        }   
             
    }
    
    public function __before() {
	    if(isset($_SERVER['HTTPS']))$_SESSION['http']='https://'; else $_SESSION['http']='http://';
    
 
        
    }
    
    public function __default() {
        //$this->view->message = "Hello world!";
    }  



    
    public function __error($e) {        
        if (isset($e->is_jet)) {
            $e->setFormat($this->__getFormat());            
        } else {
            $e = new \jet\exception($e);
            $e->setFormat($this->__getFormat());              
        }
        die($e);
    }
    
    public function __http404() {
        
    }
    
    public function __after() {
       
    }    

}
