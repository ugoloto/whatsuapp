<?php    
namespace jet; 

/* Класс неопределённого контроллера */
class controller {
    public $name;
    public $method;
    public $args;
    public $ajax = false;
    public $format;
    
    protected $__404 = false;
    
    public function __construct() {
        $this->ajax = (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');  
    }        
    
    public function __before() {

    }
    
    public function __after() {

    }  
    
    public function __desctruct() {
        
    }
    
    public function __default() {
        
    }    
    
    public function __setup($name, $method = '__default', $args = array()) {
        $this->name = $name;
        $this->method = $method;   
        $this->args = $args;
        $this->format = $this->__getFormat();             
    }
    
    public function __callmethod($method = "__default", $args = array()) {    
         return call_user_func_array(array($this,$method),$args);      
    } 
    
    public function __error($e) {
        if (isset($e->is_jet)) {
            $e->setFormat($this->__getFormat());                
        }   
        else {
            $e = \jet\exception($e);
            $e->setFormat($this->__getFormat());              
        }
        die($e);
    }
    
    protected function __getFormat() {
        $types = $this->__format();      
        if (isset($types[$this->method])) {     
            return $types[$this->method];    
        }   
        else return 'html';
    }
        
    protected function __format() {
        return array();
    }
}