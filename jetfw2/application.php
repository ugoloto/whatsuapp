<?php    
namespace jet; 

/* Класс приложения */
abstract class application {
    
    static public $controller = null;
    
    static public $model = null;
    
    static public $view = null;  

    static private $instance = null;
    
    static public $name = null;      
          
    static public $input = null;  
    
    static public $route = null; 
    
    static public $settings = array();   
    
    protected $error = null;
   
    public static function create($settings = array()) {   
        if (self::$instance === null) {        
            $class = \get_called_class();
            self::$instance = new $class($settings);     
        }                    
        return self::$instance;    
    }
    
    public static function getInstance() {       
        return self::$instance;    
    }
   
    protected function __construct($settings = array()){
        /* Настройки приложения */          
        \chdir($_SERVER['DOCUMENT_ROOT']);
        
        $settings = new obj($settings, $this->settings());   
        
        $settings->path = \realpath($settings->path); 
                   
        self::$settings = new obj(array(   
           'charset'        =>  'utf8',
           'content_type'   =>  'Content-Type: text/html; charset=UTF-8',
           'session_name'   =>  'jet',
           'session_id'     =>  false,
           'debug'          =>  false,
           'template_engine'=>  'twig',
           'locale'         =>  'en_US',
           'use_session'    =>  true,
           'cache_on'       =>  true,     
           'cache_path'     =>  false,
           'root_path'      =>  $_SERVER['DOCUMENT_ROOT'],     
           'config_path'    =>  $_SERVER['DOCUMENT_ROOT']._.'config',  
           'path'           =>  $_SERVER['DOCUMENT_ROOT']._.'application'       
        ), $settings); 
                                 
        if (self::$settings->debug) 
            \jet\log::$recording_mode = true;
            
        if (!self::$settings->cache_path) 
            self::$settings->cache_path = self::$settings->path._.'cache';
                                                                         
        \chdir(self::$settings->path);      
        
        $class = \get_class($this);
        
        $classpath = \explode('\\',$class);
        
        self::$name = \reset($classpath);  
        
        \spl_autoload_register(array($this, 'autoload'));   
        
        if (self::$settings->debug) {
            \error_reporting(E_ALL);
            \ini_set('error_reporting', E_ALL);
            \ini_set('display_errors', 'On');
        }  
        
        \set_error_handler(array('\jet\application','error_handler'), E_ALL);
        \set_exception_handler(array('\jet\application','exception_handler'));  
        \register_shutdown_function(array('\jet\application','terminate_handler'));
    }

    protected function __clone()    {
        
    }  
    
    protected function settings() {
        return array();    
    }
    
    /* Автолоад */
    private function autoload($class) {
        $dir = self::$settings->path;
        
        if (\substr($class,0,strlen(self::$name)) != self::$name) 
            return false;  
            
        if (!\class_exists($class, false)) {
            $file = $dir . \str_replace('\\', _, \substr($class, \strlen(self::$name))) . '.php';
                       
            if (\file_exists($file)) 
                require_once($file);
            else 
                return false;
        }
        
        return \class_exists($class, false); 
    }
    
    /* Роутинг */
    public function routing($route) {
        return $route;        
    }
    
    protected function beforeController() {
        
    }
    
    protected function afterController() {
        
    }
    
    protected function handleException($exception) {                  
        $this->error = $exception;
       
        if (self::$controller)  { 
            self::$controller->__error($exception);  
        }
        else {  
        
        
            print($exception);   
        }      
        $this->finish(); 
              
        return true;    
    }
    
    public static final function terminate_handler() {   
        $error = error_get_last();  
        if ($error) {           
            $exception = new \jet\exception($error);
            if (method_exists(self::$instance,'handleException')) {
                self::$instance->handleException($exception);  
            }
            else {
                print($exception);
            }
        }      
        return false;                                     
    }
    
    public static final function exception_handler($exception) {     
        if (!isset($exception->is_jet)) {
            $exception = new \jet\exception($exception);    
        }
        if (method_exists(self::$instance,'handleException')) {            
            self::$instance->handleException($exception);  
        }
        else {
            print($exception);
        }                                        
    }
    
    public static function error_handler($errno, $errstr, $errfile, $errline){
        if (!(error_reporting() & $errno)) return;  
        $exception = new \jet\exception($errstr, $errno, array('line'=>$errline, 'file'=>$errfile));
        if (method_exists(self::$instance,'handleException')) {
            self::$instance->handleException($exception);  
        }
        else {
            print($exception);
        }
        return false;
    }
    
    /* Запуск приложения */
    public final function run($input = null) {   
     
        self::$input = $input;
        
        $sections = explode("?", self::$input);
        
        self::$route = urldecode($sections[0]);
        
        // Роутинг                                                
        $path = $this->routing(self::$route);  
        
        // Подбор контроллера                                 
        
        $controller_path = \trim(\str_ireplace('/','\\', $path), " \\");  
                       
        $class = self::$name."\\controllers\\".$controller_path;  
        
        $look_for = $class;
        
        $class_path = \explode("\\", $class);
        
        $args = array();      
                               
        while (!\class_exists($class) && \count($class_path)>0) {   
            if (\class_exists($class."\\main")) {
                $class = $class."\\main";
                array_push($class_path,"main");
                break;
            }
            
            \array_unshift($args, \array_pop($class_path));
            
            $class = \join("\\", $class_path);  
            
       } 
        
        if (!\class_exists($class)) {
            throw new exception('NoController', E_USER_ERROR, array('looked_for' => $look_for, 'input' => $input ));            
        }         
            
        $controller_name = end($class_path); // Имя контроллера без неймспейса  
                
        // Создание объекта контролелра            
        self::$controller = new $class();        
        
        // Подбор метода контроллера                  
        $method = '__default';
    
        if (isset($args[0])) {   
            if (\method_exists(self::$controller, $args[0])) {
                $refl = new \ReflectionMethod(self::$controller, $args[0]);
                if ($refl->isPublic()) { 
                    $method = \array_shift($args);    
                }
            }                      
        }        
        $this->beforeController($controller_name, $method, $args);
        
        self::$controller->__setup($controller_name, $method, $args);     
        
        self::$controller->__before();
   
        // Вызов выбранного метода выбранного контроллера  
        
        $result = self::$controller->__callmethod(self::$controller->method, self::$controller->args); 
        
        $this->view($result);

        $this->finish();
    }
    
    public function view($view_object = null) {
        
        if ($view_object === null) return true;
        
        $view = $view_object;    

        if (gettype($view_object) == 'array') {
            $view = json::encode($view_object);
        }
        else if (gettype($view_object) == 'object') {
            $view_class = "\\jet\\view\\".get_class($view_object);  
            if (class_exists($view_class)) {
                $view = new $view_class($view_object); 
            }       
        }
        $render = (string)$view;          
        print($render);
        
        return true;
    }

    public final function finish() {
        if (self::$controller)  {        
            self::$controller->__after();
            $this->afterController(self::$controller->name, self::$controller->method, self::$controller->args);
        }
        else {
            $this->afterController(null,null,null);    
        }          
    }                   
    
    /* Получить текущие настройки */
    public function getSettings() {   
        return self::$settings;   
    }    
    
    public function bindSession($id = false) {
       
    }    
    
} 