var uploader = new plupload.Uploader({
	runtimes : 'html5,flash,html4',
	browse_button : 'pickfiles', // you can pass an id...
	container: document.getElementById('containerpic'), // ... or DOM Element itself
	url : '/img/uploaduserpic',
	unique_names:true,
	multi_selection:false,
	//max_file_count: 1,

	flash_swf_url : '/js/lib/plupload-2.1.9/Moxie.swf',
	silverlight_xap_url : '/js/lib/plupload-2.1.9/Moxie.xap',
	
	filters : {
		max_file_size : '50mb',
		mime_types: [
			{title : "Image files", extensions : "jpg,gif,png,jpeg"},
			
		]
	},

	init: {
		PostInit: function() {},

		FilesAdded: function(up, files) {
			while (up.files.length > 1) {
	        	up.removeFile(up.files[0]);
	        }		
	
			up.refresh(); // Reposition Flash/Silverlight
			uploader.start();
		},

		UploadProgress: function(up, file) {
		},

		Error: function(up, err) {
		
	
			up.refresh(); // Reposition Flash/Silverlight
			
	
		},
		
		FileUploaded: function(up, file, info) {
			$("#progress1").hide();
			var obj = JSON.parse(info.response);

			$("#preview").prop("src",'/userfiles/'+obj.cid+'/avatar/'+obj.cleanFileName).show();
			$("#img-file").val(obj.cleanFileName);
		}
	}
	
});					


$(document).ready(function(){
	uploader.init();
	$("#fprofile").validate({
		  rules: {
		    
		    user_email: {
		      required: true,
		      email: true
		    },
			user_nick: {
				required: true
			},
			cpass: {
				equalTo: "#pass"
    		}		    
		  },
		  messages: {
		    
		    username: {
		      required: "Поле Email обов'язково має бути заповнено",
		      email: "Ваша email адреса має бути в форматі name@domain.com"
		    },
		    
		    pass: {
			  required: "Нік обов'язково",		    
			}
		    
		},
		
		submitHandler: function() { 	
			//showProcess();
			 
			$.ajax({
				type:'POST', 
				url: '/profile/doUpdate', 
				data:$('#fprofile').serialize(), 
				dataType: 'json',
				success: function(r) {
					
					if(r.status){
						window.location.href="/profile";
					}else{
						swal({   title: r.title,   text: r.msg,   type: "error", confirmButtonColor: "#DD6B55",  confirmButtonText: "Ok" });
						
						
					}
				}
			
			});
			
		}
			
	});

	
});



	