$(document).ready(function(){

	$("#regForm").validate({
		  rules: {
		    
		    username: {
		      required: true,
		      email: true
		    },
			pass: {
				required: true
			},
			cpass: {
				equalTo: "#pass"
    		}		    
		  },
		  messages: {
		    
		    username: {
		      required: "Поле Email обов'язково має бути заповнено",
		      email: "Ваша email адреса має бути в форматі name@domain.com"
		    },
		    
		    pass: {
			  required: "Пароль обов'язково",		    
			}
		    
		},
		
		submitHandler: function() { 	
			//showProcess();
			 
			$.ajax({
				type:'POST', 
				url: '/signup/doSignup', 
				data:$('#regForm').serialize(), 
				dataType: 'json',
				success: function(r) {
					
					if(r.status){
						window.location.href=r.url;
					}else{
						$("#username").val('');
						$("#pass").val('');
						$("#cpass").val('');
						//endProcess();
						swal({   title: r.title,   text: r.msg,   type: "error", confirmButtonColor: "#DD6B55",  confirmButtonText: "Ok" });
						
						
					}
				}
			
			});
			
		}
			
	});



});