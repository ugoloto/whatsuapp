$(document).ready(function(){
	
	
	$("#flogin").validate({
		  rules: {
		    
		    username: {
		      required: true,
		      email: true
		    },
			password: {
				required: true
			}
		  },
		  messages: {
		    
		    username: {
		      required: "Поле Email обов'язково має бути заповнено",
		      email: "Ваша email адреса має бути в форматі name@domain.com"
		    },
		    
		    password: {
			  required: "Пароль обов'язково",		    
			}
			
		},
		
		submitHandler: function() {
			//showProcess();	
			 
			$.ajax({
				type:'POST', 
				url: '/login/doLogin', 
				data:$('#flogin').serialize(), 
				dataType: 'json',
				success: function(r) {
					if(r.status){
						window.location.href="/profile";
					}else{
						//endProcess();
						swal({ title: r.title,   text: r.msg,   type: "error", confirmButtonColor: "#87b87f",  confirmButtonText: "Ok" });
						
					}
				}
			
			});
			
		}
			
	});	
	
});