/**
 * Created by Julia on 18.04.2017.
 */
$(document).ready(function(){
       $("#subs").validate({

           messages: {

               subscr: {
                   required: "Поле Email обов'язково має бути заповнено",
                   email: "Email адреса має бути в форматі name@domain.com"
               }
           },
        submitHandler: function() {

            $.ajax({
                type:'POST',
                url: '/footer/doAdd',
                data:$('#subs').serialize(),
                dataType: 'json',
                success: function(r) {

                    if(r.status){
                       swal('Ви підписалися на новини');
                    }else{
                       $("#subscr").val('');
                        swal({   title: r.title,   text: r.msg,   type: "error", confirmButtonColor: "#DD6B55",  confirmButtonText: "Ok" });


                    }
                }

            });

        }

    });
});
