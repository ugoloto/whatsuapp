var uploader = new plupload.Uploader({
	runtimes : 'html5,flash,html4',
	browse_button : 'pickfiles', // you can pass an id...
	container: document.getElementById('containerpic'), // ... or DOM Element itself
	url : '/img/postimg',
	unique_names:true,
	multi_selection:false,
	//max_file_count: 1,

	flash_swf_url : '/js/lib/plupload-2.1.9/Moxie.swf',
	silverlight_xap_url : '/js/lib/plupload-2.1.9/Moxie.xap',
	
	filters : {
		max_file_size : '50mb',
		mime_types: [
			{title : "Image files", extensions : "jpg,gif,png,jpeg"},
			
		]
	},

	init: {
		PostInit: function() {},

		FilesAdded: function(up, files) {
			while (up.files.length > 1) {
	        	up.removeFile(up.files[0]);
	        }		
	
			up.refresh(); // Reposition Flash/Silverlight
			uploader.start();
		},

		UploadProgress: function(up, file) {
		},

		Error: function(up, err) {
		
	
			up.refresh(); // Reposition Flash/Silverlight
			
	
		},
		
		FileUploaded: function(up, file, info) {
			$("#progress1").hide();
			var obj = JSON.parse(info.response);

			$("#preview").prop("src",'/userfiles/'+obj.cid+'/posts/'+obj.cleanFileName).show();
			$("#img-file").val(obj.cleanFileName);
		}
	}
	
});					
	
	

$(document).ready(function(){
	uploader.init();
	$("#posts").validate({

		
		submitHandler: function() { 	
			//showProcess();
			 
			$.ajax({
				type:'POST', 
				url: '/posts/doAddPost', 
				data:$('#posts').serialize(), 
				dataType: 'json',
				success: function(r) {
					
					if(r.status){
						window.location.href="/posts";
					}else{
						swal({   title: r.title,   text: r.msg,   type: "error", confirmButtonColor: "#DD6B55",  confirmButtonText: "Ok" });
						
						
					}
				}
			
			});
			
		}
			
	});
	
	$("#postsEdit").validate({

		
		submitHandler: function() { 	
			//showProcess();
			 
			$.ajax({
				type:'POST', 
				url: '/posts/doUpdatePost', 
				data:$('#postsEdit').serialize(), 
				dataType: 'json',
				success: function(r) {
					
					if(r.status){
						window.location.href="/posts";
					}else{
						swal({   title: r.title,   text: r.msg,   type: "error", confirmButtonColor: "#DD6B55",  confirmButtonText: "Ok" });
						
						
					}
				}
			
			});
			
		}
			
	});


});


$(document).on("click",".del",function(){
	
	var id=$(this).data("id");
	$.post("/posts/del",{id:id},function(){
		window.location.href="/posts";
	},"json");
	
	return false;
});

$(document).on("submit",".quick-search",function(){

	var pd = $(".quick-search").serialize();

	$.post("/posts/changeSearch",pd,function(r){

		window.location.href="/posts";

	},"json")

	return false;
});
$(document).on("submit",".mobile-search",function(){

	var pd = $(".mobile-search").serialize();

	$.post("/posts/changeSearch",pd,function(r){

		window.location.href="/posts";

	},"json")

	return false;
});