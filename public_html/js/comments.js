/**
 * Created by Julia on 10.04.2017.
 */
$(document).ready(function(){

    $("#doAddComments").validate({


        submitHandler: function() {
            //showProcess();

            $.ajax({
                type:'POST',
                url: '/pub/addCom',
                data:$('#doAddComments').serialize(),
                dataType: 'json',
                success: function(r) {

                    if(r.status){

                        swal("Коментар додано")

                       window.location.href="/pub/view/"+r.id;
                    }else{
                        swal({   title: r.title,   text: r.msg,   type: "error", confirmButtonColor: "#DD6B55",  confirmButtonText: "Ok" });


                    }


                }

            });

        }

    });
    $(document).on("click",".D",function(){
        var pubid = $("#post_id").val()
        var id=$(this).data("id");
        $.post("/pub/del",{id:id},function(){
           swal("Видалено");
           window.location.href="/pub/view/"+pubid;
        },"json");

        return false;
    });
});

