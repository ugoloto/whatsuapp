/**
 * Created by Julia on 13.04.2017.
 */
$(document).ready(function(){

    $("#doAddComments").validate({


        submitHandler: function() {
            //showProcess();

            $.ajax({
                type:'POST',
                url: '/events/addCom',
                data:$('#doAddComments').serialize(),
                dataType: 'json',
                success: function(r) {

                    if(r.status){

                        swal("Коментар додано");
                        window.location.href="/events/view/"+r.id;
                    }else{
                        swal({   title: r.title,   text: r.msg,   type: "error", confirmButtonColor: "#DD6B55",  confirmButtonText: "Ok" });


                    }


                }

            });

        }

    });

});
$(document).on("click",".del",function(){
    var evid = $("#poster_id").val()
    var id=$(this).data("id");
    $.post("/events/del",{id:id},function(){
        swal("Видалено");
        window.location.href="/events/view/"+evid;
    },"json");

    return false;
});

